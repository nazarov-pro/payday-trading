## PayDay Trading Application ##

### Requirements ###
- Java 1.8
- PostgreSql 12
- Docker 1.6.0 or higher (for testing)

### Installation ###

#### 1. Installation on docker env ####
For building and running the image in the local environment (with **docker-compose**):
```shell script
docker-compose -f docker-compose.yaml up --force-recreate -d
```

For building and running the image in the local environment (with jib plugin):
```shell script
# deploying postgres to the docker container
docker-compose -f configs/postgres.compose.yml up -d

# building app
export BUILD_NUMBER=1 # will be payday-trade:1.0.1
# can take more time for integration tests (for skipping add at the end of the command `-x test`)
./gradlew clean build
./gradlew jibDockerBuild # or docker build -t payday-trade:1.0.1
docker run -it --rm  -p 8080:8080 -e SPRING_PROFILES_ACTIVE=dev -e SPRING_DATASOURCE_URL="jdbc:postgresql://192.168.0.2:5432/payday?user=postgres&password=secured2019" payday-trade:1.0.1
```

For pushing the image to the remote docker registry
```shell script
export BUILD_NUMBER=1 # will be payday-trade:1.0.1
export DOCKER_REGISTRY_URL=""
export DOCKER_REGISTRY_USER=""
export DOCKER_REGISTRY_PASSWORD=""

# can take more time for integration tests (for skipping add at the end of the command `-x test`)
./gradlew clean build
./gradlew jib -DsendCredentialsOverHttp=true -Djib.httpTimeout=0
docker run -it --rm  -p 8080:8080 -e SPRING_PROFILES_ACTIVE=dev -e SPRING_DATASOURCE_URL="jdbc:postgresql://192.168.0.2:5432/payday?user=postgres&password=secured2019" payday-trade:1.0.2
```

#### 2. Install on k8s env ####
- Step 1. Push the image to the private registry
    ```shell script
    export BUILD_NUMBER=1 # will be payday-trade:1.0.1
    export DOCKER_REGISTRY_URL="" #without http/https and end with slash '/' example: 198.168.0.2:30005/ 
    export DOCKER_REGISTRY_USER=""
    export DOCKER_REGISTRY_PASSWORD=""

    # can take more time for integration tests (for skipping add at the end of the command `-x test`)
    ./gradlew clean build
    ./gradlew jib -DsendCredentialsOverHttp=true -Djib.httpTimeout=0
    ```
- Step 2. Edit config file (add private registry's url and image pull secret name to ``configs/k8s/deployment.dev.yaml`` and ``configs/k8s/deployment.prod.yaml``)
- Step 3. Deploy resources to the k8s cluster 
    Development env:
    ```shell script
    kubectl apply -f configs/k8s/postgres.dev.yaml
  
    kubectl apply -f configs/k8s/deployment.dev.yaml
    ```
    Production env:
    ```shell script
    kubectl apply -f configs/k8s/postgres.prod.yaml
  
    kubectl apply -f configs/k8s/deployment.prod.yaml
    ```
    In all deployments node port is enabled.

PS: Postman collection is also available with steps ``configs/Payday Trading.postman_collection.json``