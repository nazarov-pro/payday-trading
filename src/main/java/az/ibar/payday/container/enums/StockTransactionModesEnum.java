package az.ibar.payday.container.enums;

import az.ibar.payday.utils.Constants;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum StockTransactionModesEnum {
    SELL(Constants.TRANSACTION_SELL_VALUE),
    UNKNOWN(Constants.TRANSACTION_UNKNOWN_VALUE),
    BUY(Constants.TRANSACTION_BUY_VALUE);

    private final Integer value;

    public static StockTransactionModesEnum from(Integer value) {
        for(StockTransactionModesEnum mode : values()) {
            if(mode.getValue().equals(value)) {
                return mode;
            }
        }
        return UNKNOWN;
    }
}
