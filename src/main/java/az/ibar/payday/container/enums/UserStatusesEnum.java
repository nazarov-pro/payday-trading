package az.ibar.payday.container.enums;

public enum UserStatusesEnum {
    DEACTIVATED,
    ACTIVE,
    SUSPENDED,
    DELETED
}
