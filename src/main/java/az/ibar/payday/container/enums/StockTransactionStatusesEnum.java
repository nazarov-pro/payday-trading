package az.ibar.payday.container.enums;

public enum StockTransactionStatusesEnum {
    ACTIVE,
    COMPLETED,
    DEACTIVATED
}
