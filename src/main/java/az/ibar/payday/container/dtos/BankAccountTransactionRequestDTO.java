package az.ibar.payday.container.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class BankAccountTransactionRequestDTO {
    private BigDecimal amount;
    private String version;
}
