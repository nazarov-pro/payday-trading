package az.ibar.payday.container.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class StockResponseDTO implements Serializable {
    private String symbol;
    private BigDecimal price;

}
