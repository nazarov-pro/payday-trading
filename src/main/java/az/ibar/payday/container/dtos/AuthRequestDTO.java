package az.ibar.payday.container.dtos;

import az.ibar.payday.utils.Constants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthRequestDTO {
    private String usernameOrEmail;
    @Pattern(regexp = Constants.VALID_PASSWORD_REGEXP,
            message = Constants.VALID_PASSWORD_MESSAGE)
    private String password;
}
