package az.ibar.payday.container.dtos;

import lombok.Data;

@Data
public class UserActivationRequestDTO {
    private Long id;
    private String email;
    private String activationCode;

}
