package az.ibar.payday.container.dtos;

import lombok.Data;

import java.util.List;

@Data
public class FinnHubRealtimeTradingDTO {
    private String type;
    private List<FinnHubRealtimeTradingDataDTO> data;
}
