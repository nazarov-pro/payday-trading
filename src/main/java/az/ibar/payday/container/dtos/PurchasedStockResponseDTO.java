package az.ibar.payday.container.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PurchasedStockResponseDTO {
    private Long id;
    private String symbol;
    private BigDecimal symbolUnit;
    private String version;
    private Long createdTimestamp;
    private Long updatedTimestamp;
}
