package az.ibar.payday.container.dtos;

import az.ibar.payday.container.enums.StockTransactionModesEnum;
import az.ibar.payday.container.enums.StockTransactionStatusesEnum;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class StockOrderResponseDTO {
    private Long id;
    private StockTransactionModesEnum mode;
    private BigDecimal targetAmount;
    private String symbol;
    private BigDecimal symbolUnit;
    private BigDecimal startAmount;
    private BigDecimal finalAmount;
    private StockTransactionStatusesEnum status;
    private Long createdTimestamp;
    private Long updatedTimestamp;
    private String version;
    private Long bankAccountId;

}
