package az.ibar.payday.container.dtos;

import az.ibar.payday.utils.Constants;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDTO {
    private Long id;
    private String username;
    @Pattern(regexp = Constants.VALID_PASSWORD_REGEXP,
            message = Constants.VALID_PASSWORD_MESSAGE)
    private String password;
    @Email(message = Constants.VALID_EMAIL_MESSAGE)
    private String email;

}
