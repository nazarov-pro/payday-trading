package az.ibar.payday.container.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class StockOrderRequestDTO {
    private Long id;
    @JsonIgnore
    private Integer mode;
    @NotNull
    private BigDecimal targetAmount;
    @NotNull
    private String symbol;
    @NotNull
    private BigDecimal symbolUnit;
    @NotNull
    private Long bankAccountId;
    @JsonIgnore
    private Long userId;

}
