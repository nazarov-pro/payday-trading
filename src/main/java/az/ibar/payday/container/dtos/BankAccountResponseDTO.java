package az.ibar.payday.container.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BankAccountResponseDTO {
    private Long id;
    private BigDecimal balance;
    private String version;

}
