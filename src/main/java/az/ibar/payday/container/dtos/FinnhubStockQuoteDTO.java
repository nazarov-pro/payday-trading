package az.ibar.payday.container.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class FinnhubStockQuoteDTO implements Serializable {
    private String symbol;
    @JsonAlias("o")
    private BigDecimal openPriceOfTheDay;
    @JsonAlias("h")
    private BigDecimal highPriceOfTheDay;
    @JsonAlias("l")
    private BigDecimal lowPriceOfTheDay;
    @JsonAlias("c")
    private BigDecimal currentPrice;
    @JsonAlias("pc")
    private BigDecimal previousClosePrice;
    @JsonAlias("t")
    private Long timestamp;
}
