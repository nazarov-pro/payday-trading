package az.ibar.payday.container.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class FinnHubRealtimeTradingDataDTO {
    @JsonAlias("s")
    private String symbol;
    @JsonAlias("p")
    private BigDecimal lastPrice;
    @JsonAlias("t")
    private Long timestamp;
    @JsonAlias("v")
    private BigDecimal volume;

}
