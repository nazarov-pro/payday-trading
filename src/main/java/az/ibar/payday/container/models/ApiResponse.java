package az.ibar.payday.container.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ApiResponse<M> extends ApiMessage {
    private M item;

}
