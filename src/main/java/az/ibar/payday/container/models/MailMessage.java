package az.ibar.payday.container.models;

import lombok.Data;

@Data
public class MailMessage {
    private String recipient;
    private String subject;
    private String content;

}
