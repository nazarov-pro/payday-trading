package az.ibar.payday.container.models;

import lombok.Data;

@Data
public class ApiMessage {
    private boolean success;
    private ApiMetadata metadata;
    private ApiError error;
}
