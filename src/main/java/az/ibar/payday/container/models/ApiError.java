package az.ibar.payday.container.models;

import lombok.Data;

@Data
public class ApiError {
    private String message;
    private String reason;
    private String code;

}
