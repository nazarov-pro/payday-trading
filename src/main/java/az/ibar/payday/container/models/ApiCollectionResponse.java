package az.ibar.payday.container.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = true)
public class ApiCollectionResponse<M> extends ApiMessage {
    private Collection<M> items;

}
