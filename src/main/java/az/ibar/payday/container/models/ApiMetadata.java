package az.ibar.payday.container.models;

import lombok.Data;

@Data
public class ApiMetadata {
    private String responseId;
    private String requestId;
    private Long timestamp;
}
