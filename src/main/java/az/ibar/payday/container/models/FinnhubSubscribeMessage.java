package az.ibar.payday.container.models;

import lombok.Data;

@Data
public class FinnhubSubscribeMessage {
    private String type = "subscribe";
    private String symbol;

}
