package az.ibar.payday.container.exceptions;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("Item not found.");
    }

    public NotFoundException(String message) {
        super(message);
    }
}
