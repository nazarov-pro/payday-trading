package az.ibar.payday.container.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(
        name = "p_purchased_stocks",
        indexes = {
                @Index(name = "p_ps_symbol_idx", columnList = "symbol"),
                @Index(name = "p_ps_version_idx", columnList = "version")
        }
)
@Data
public class PurchasedStock {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "p_purchased_stock_id_seq"
    )
    @SequenceGenerator(
            name = "p_purchased_stock_id_seq",
            sequenceName = "p_purchased_stock_id_seq"
    )
    @Column(name = "id")
    private Long id;
    @Column(name = "symbol")
    private String symbol;
    @Column(name = "symbol_unit")
    private BigDecimal symbolUnit;
    @Column(name = "created_timestamp", updatable = false)
    private Long createdTimestamp;
    @Column(name = "updated_timestamp", insertable = false)
    private Long updatedTimeStamp;
    @Column(name = "version")
    private String version;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
