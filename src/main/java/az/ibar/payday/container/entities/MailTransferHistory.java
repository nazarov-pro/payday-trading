package az.ibar.payday.container.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(
        name = "p_mail_history",
        indexes = {
                @Index(name = "p_mth_recipient_idx", columnList = "recipient"),
                @Index(name = "p_mth_sent_idx", columnList = "sent")
        }
)
public class MailTransferHistory {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "p_mail_history_id_seq_generator"
    )
    @SequenceGenerator(
            name = "p_mail_history_id_seq_generator",
            sequenceName = "p_mail_history_id_seq",
            allocationSize = 1
    )
    @Column(name = "id")
    private Long id;
    @Column(name = "recipient")
    private String recipient;
    @Column(name = "content", columnDefinition = "TEXT")
    private String content;
    @Column(name = "sent")
    private boolean sent;
    @Column(name = "last_update_timestamp")
    private Long lastUpdateTimestamp;

}
