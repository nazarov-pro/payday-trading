package az.ibar.payday.container.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(
        name = "p_stock_settings",
        indexes = {
                @Index(name = "p_st_last_update_tmp_idx", columnList = "last_update_timestamp"),
                @Index(name = "p_st_feed_provider_idx", columnList = "feed_provider"),
                @Index(name = "p_st_locked_idx", columnList = "locked"),
        }
)
@Data
public class StockSettings {
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "last_price")
    private BigDecimal lastPrice;
    @Column(name = "feed_provider")
    private String feedProvider;
    @Column(name = "locked")
    private Boolean locked;
    @Column(name = "last_update_timestamp")
    private Long lastUpdateTimestamp;

}
