package az.ibar.payday.container.entities;

import az.ibar.payday.container.enums.UserStatusesEnum;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(
        name = "p_users",
        indexes = {
                @Index(name = "p_ps_username_idx", columnList = "username"),
                @Index(name = "p_ps_email_idx", columnList = "email"),
                @Index(name = "p_ps_status_idx", columnList = "status")
        }
)
public class User {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "p_user_id_seq"
    )
    @SequenceGenerator(
            name = "p_user_id_seq",
            sequenceName = "p_user_id_seq"
    )
    @Column(name = "id")
    private Long id;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;
    @Column(name = "activation_token")
    private String activationToken;
    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    private UserStatusesEnum status;

}
