package az.ibar.payday.container.entities;

import az.ibar.payday.container.enums.StockTransactionStatusesEnum;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(
        name = "p_stock_transactions",
        indexes = {
                @Index(name = "p_st_symbol_idx", columnList = "symbol"),
                @Index(name = "p_st_status_idx", columnList = "status"),
                @Index(name = "p_st_target_amount_idx", columnList = "target_amount"),
                @Index(name = "p_st_mode_idx", columnList = "mode"),
                @Index(name = "p_st_version_idx", columnList = "version")
        }
)
@Data
public class StockTransaction {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "p_stock_transaction_id_seq"
    )
    @SequenceGenerator(
            name = "p_stock_transaction_id_seq",
            sequenceName = "p_stock_transaction_id_seq"
    )
    @Column(name = "id")
    private Long id;
    @Column(name = "mode")
    private Integer mode;//buy or sell
    @Column(name = "target_amount")
    private BigDecimal targetAmount;
    @Column(name = "symbol_unit")
    private BigDecimal symbolUnit;
    @Column(name = "symbol")
    private String symbol;
    @Column(name = "start_amount")
    private BigDecimal startAmount;
    @Column(name = "final_amount")
    private BigDecimal finalAmount;
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status")
    private StockTransactionStatusesEnum status;
    @Column(name = "updated_timestamp", insertable = false)
    private Long updatedTimestamp;
    @Column(name = "created_timestamp", updatable = false)
    private Long createdTimestamp;
    @Column(name = "version")
    private String version;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bank_account_id")
    private BankAccount bankAccount;

}
