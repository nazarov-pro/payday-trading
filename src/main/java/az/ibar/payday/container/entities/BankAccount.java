package az.ibar.payday.container.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(
        name = "p_bank_accounts",
        indexes = {
                @Index(name = "p_ba_version_idx", columnList = "version")
        }
)
public class BankAccount {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "p_bank_account_id_seq"
    )
    @SequenceGenerator(
            name = "p_bank_account_id_seq",
            sequenceName = "p_bank_account_id_seq"
    )
    @Column(name = "id")
    private Long id;
    @Column(name = "balance")
    private BigDecimal balance;
    @Column(name = "version")
    private String version;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
