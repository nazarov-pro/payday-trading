package az.ibar.payday.resources;

import az.ibar.payday.container.BaseResource;
import az.ibar.payday.container.dtos.AuthRequestDTO;
import az.ibar.payday.container.dtos.AuthResponseDTO;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.services.UserService;
import az.ibar.payday.utils.JwtUtils;
import az.ibar.payday.utils.generators.ApiResponseGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/auth")
public class AuthenticationResource implements BaseResource {
    private final JwtUtils jwtUtils;
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;
    private final ApiResponseGenerator apiResponseGenerator;

    @RequestMapping(method = RequestMethod.POST)
    public Mono<ResponseEntity<ApiResponse<AuthResponseDTO>>> auth(@RequestBody @Valid AuthRequestDTO authRequestDto) {
        return Mono.just(userService.findByUsernameOrEmail(authRequestDto.getUsernameOrEmail())).map(
                userDetails -> {
                    if (passwordEncoder.encode(authRequestDto.getPassword()).equals(userDetails.getPassword())) {
                        return ResponseEntity.ok(apiResponseGenerator.generate(
                                new AuthResponseDTO(jwtUtils.generateToken(userDetails)))
                        );
                    } else {
                        throw new IllegalArgumentException("Login or password is incorrect.");
                    }
                }
        );
    }


}
