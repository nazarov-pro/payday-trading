package az.ibar.payday.resources;

import az.ibar.payday.container.BaseResource;
import az.ibar.payday.container.dtos.UserActivationRequestDTO;
import az.ibar.payday.container.dtos.UserRequestDTO;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.services.UserService;
import az.ibar.payday.utils.generators.ApiResponseGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserResource implements BaseResource {
    private final UserService userService;
    private final ApiResponseGenerator apiResponseGenerator;
    private final PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Mono<ResponseEntity<ApiResponse<String>>> register(@RequestBody @Valid UserRequestDTO userRequestDto) {
        userRequestDto.setPassword(passwordEncoder.encode(userRequestDto.getPassword()));
        userService.registerUser(userRequestDto);
        return Mono.just(ResponseEntity.ok(apiResponseGenerator.generate("OK")));
    }

    @RequestMapping(value = "/activate", method = RequestMethod.GET)
    public Mono<ResponseEntity<ApiResponse<String>>> register(
            @RequestParam("id") Long id,
            @RequestParam("email") String email,
            @RequestParam("activationToken") String activationToken
    ) {
        UserActivationRequestDTO userActivationRequestDto = new UserActivationRequestDTO();
        userActivationRequestDto.setEmail(email);
        userActivationRequestDto.setId(id);
        userActivationRequestDto.setActivationCode(activationToken);
        userService.activateUser(userActivationRequestDto);
        return Mono.just(ResponseEntity.ok(apiResponseGenerator.generate("OK")));
    }

}
