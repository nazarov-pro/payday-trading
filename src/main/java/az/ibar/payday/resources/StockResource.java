package az.ibar.payday.resources;

import az.ibar.payday.container.BaseResource;
import az.ibar.payday.container.dtos.PurchasedStockResponseDTO;
import az.ibar.payday.container.dtos.StockOrderRequestDTO;
import az.ibar.payday.container.dtos.StockOrderResponseDTO;
import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.container.enums.StockTransactionModesEnum;
import az.ibar.payday.container.models.ApiCollectionResponse;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.services.PurchasedStockService;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.services.StockTransactionService;
import az.ibar.payday.utils.generators.ApiResponseGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/stocks")
public class StockResource implements BaseResource {
    private final PurchasedStockService purchasedStockService;
    private final StockTransactionService stockTransactionService;
    private final StockSettingsService stockSettingsService;
    private final ApiResponseGenerator apiResponseGenerator;

    @PreAuthorize("isAuthenticated()")
    @GetMapping
    public Mono<ApiCollectionResponse<PurchasedStockResponseDTO>> purchasedStocks(
            Authentication authentication
    ) {
        AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
        List<PurchasedStockResponseDTO> purchasedStocks = purchasedStockService.findAllByUserId(userDetails.getId());
        return Mono.just(apiResponseGenerator.generateCollection(purchasedStocks));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/orders")
    public Mono<ApiCollectionResponse<StockOrderResponseDTO>> stockOrders(
            Authentication authentication
    ) {
        AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
        List<StockOrderResponseDTO> purchasedStocks = stockTransactionService.findAllByUserId(userDetails.getId());
        return Mono.just(apiResponseGenerator.generateCollection(purchasedStocks));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/orders/{id}")
    public Mono<ApiResponse<StockOrderResponseDTO>> stockOrders(
            @PathVariable("id") Long id,
            Authentication authentication
    ) {
        AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
        StockOrderResponseDTO purchasedStock = stockTransactionService.findOneByUserId(id, userDetails.getId());
        return Mono.just(apiResponseGenerator.generate(purchasedStock));
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/subscribe/{id}")
    public Mono<ApiResponse<String>> subscribeStock(
            @PathVariable("id") String id
    ) {
        stockSettingsService.create(id);
        return Mono.just(apiResponseGenerator.generate("OK"));
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/unsubscribe/{id}")
    public Mono<ApiResponse<String>> unSubscribeStock(
            @PathVariable("id") String id
    ) {
        stockSettingsService.delete(id);
        return Mono.just(apiResponseGenerator.generate("OK"));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/market")
    public Mono<ApiCollectionResponse<StockResponseDTO>> stocks(
            @RequestParam(value = "symbols", defaultValue = "", required = false) String symbols
    ) {
        List<StockResponseDTO> stockResponses = stockTransactionService.fetchAllStocksBySymbols(symbols);
        return Mono.just(apiResponseGenerator.generateCollection(stockResponses));
    }


    @PreAuthorize("isAuthenticated()")
    @PostMapping("/sell")
    public Mono<ApiResponse<StockOrderResponseDTO>> sell(
            @Valid @RequestBody StockOrderRequestDTO stockOrderRequestDto,
            Authentication authentication
    ) {
        AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
        stockOrderRequestDto.setMode(StockTransactionModesEnum.SELL.getValue());
        stockOrderRequestDto.setUserId(userDetails.getId());
        StockOrderResponseDTO orderResponseDto = stockTransactionService.sell(stockOrderRequestDto);
        return Mono.just(apiResponseGenerator.generate(orderResponseDto));
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/buy")
    public Mono<ApiResponse<StockOrderResponseDTO>> buy(
            @Valid @RequestBody StockOrderRequestDTO stockOrderRequestDto,
            Authentication authentication
    ) {
        AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
        stockOrderRequestDto.setMode(StockTransactionModesEnum.BUY.getValue());
        stockOrderRequestDto.setUserId(userDetails.getId());
        StockOrderResponseDTO orderResponseDto = stockTransactionService.buy(stockOrderRequestDto);
        return Mono.just(apiResponseGenerator.generate(orderResponseDto));
    }


}
