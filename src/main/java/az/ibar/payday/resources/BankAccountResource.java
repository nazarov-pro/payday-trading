package az.ibar.payday.resources;

import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.dtos.BankAccountTransactionRequestDTO;
import az.ibar.payday.container.models.ApiCollectionResponse;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.services.BankAccountService;
import az.ibar.payday.utils.generators.ApiResponseGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/bank-accounts")
public class BankAccountResource {
    private final BankAccountService bankAccountService;
    private final ApiResponseGenerator apiResponseGenerator;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET)
    public Mono<ResponseEntity<ApiCollectionResponse<BankAccountResponseDTO>>> list(Authentication authentication) {
        AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
        List<BankAccountResponseDTO> bankAccounts = bankAccountService.list(userDetails.getId());
        return Mono.just(ResponseEntity.ok(apiResponseGenerator.generateCollection(bankAccounts)));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Mono<ResponseEntity<ApiResponse<BankAccountResponseDTO>>> create(Authentication authentication) {
        AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
        BankAccountResponseDTO bankAccountResponseDto = bankAccountService.create(userDetails.getId());
        return Mono.just(ResponseEntity.ok(apiResponseGenerator.generate(bankAccountResponseDto)));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}/top-up", method = RequestMethod.POST)
    public Mono<ResponseEntity<ApiResponse<BankAccountResponseDTO>>> balanceTopUp(
            @PathVariable("id") Long id,
            @Valid @RequestBody BankAccountTransactionRequestDTO bankAccountTransactionRequestDto,
            Authentication authentication
    ) {
        AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
        boolean usersBankAccount = Optional.ofNullable(
                bankAccountService.checkUserIdAndBankAccountId(userDetails.getId(), id)
        ).orElse(false);
        if(!usersBankAccount) {
            throw new IllegalArgumentException("This bank account does not belong to the user.");
        }
        BankAccountResponseDTO bankAccountResponseDto = bankAccountService.topUp(
                id, bankAccountTransactionRequestDto.getAmount(), bankAccountTransactionRequestDto.getVersion()
        );
        return Mono.just(ResponseEntity.ok(apiResponseGenerator.generate(bankAccountResponseDto)));
    }

}
