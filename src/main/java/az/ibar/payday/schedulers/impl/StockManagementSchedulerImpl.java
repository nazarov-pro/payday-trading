package az.ibar.payday.schedulers.impl;

import az.ibar.payday.container.entities.StockSettings;
import az.ibar.payday.schedulers.StockManagementScheduler;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.services.trading.RealTimeTradingService;
import az.ibar.payday.utils.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class StockManagementSchedulerImpl implements StockManagementScheduler {
    @Qualifier(Constants.BEAN_REALTIME_FINN_HUB_PROXY_NAME)
    private final RealTimeTradingService realTimeTradingService;
    private final StockSettingsService stockSettingsService;
    private final Integer EXPIRATION_SECONDS = 120;
    private final Set<String> symbolSet = new HashSet<>();

    @PostConstruct
    @Override
    public void init() {
        stockSettingsService.unlockAll(realTimeTradingService.name());
        List<StockSettings> stockSettings = stockSettingsService.updateAndGet(
                realTimeTradingService.name(), EXPIRATION_SECONDS
        );
        stockSettings.forEach(stockSetting -> subscribe(stockSetting.getId()));
    }

    @Scheduled(fixedDelay = 60_000, initialDelay = 10_000)//every minute
    @Override
    public void refreshStocks() {
        stockSettingsService.updateAllSubscribersLastTmp(realTimeTradingService.name());
        List<StockSettings> stockSettings = stockSettingsService.updateAndGet(
                realTimeTradingService.name(), EXPIRATION_SECONDS
        );
        stockSettings.forEach(stockSetting -> subscribe(stockSetting.getId()));

        Set<String> currentSymbolsInDbLevel = stockSettingsService.findAllByFeedProviderId(
                realTimeTradingService.name()).stream()
                .map(StockSettings::getId).collect(Collectors.toSet());

        currentSymbolsInDbLevel.stream().filter(symbol -> !symbolSet.contains(symbol))
                .forEach(this::subscribe);
        symbolSet.stream().filter(
                symbol -> !currentSymbolsInDbLevel.contains(symbol)
        ).collect(Collectors.toList()).forEach(this::unSubscribe);
    }

    private void subscribe(String id) {
        realTimeTradingService.subscribe(id);
        symbolSet.add(id);
    }

    private void unSubscribe(String id) {
        realTimeTradingService.unsubscribe(id);
        symbolSet.remove(id);
    }
}
