package az.ibar.payday.schedulers;

public interface StockManagementScheduler {
    void init();
    void refreshStocks();
}
