package az.ibar.payday.configs;

import az.ibar.payday.container.exceptions.NotFoundException;
import az.ibar.payday.container.models.ApiError;
import az.ibar.payday.container.models.ApiMessage;
import az.ibar.payday.utils.generators.ApiResponseGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

@Configuration
@RequiredArgsConstructor
@Slf4j
@Order(-2)
public class GlobalExceptionHandler implements ErrorWebExceptionHandler {
    private final ApiResponseGenerator apiResponseGenerator;
    @Qualifier("json")
    private final ObjectMapper objectMapper;

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        log.error("Global security error, ", ex);
        DataBufferFactory bufferFactory = exchange.getResponse().bufferFactory();
        ApiMessage apiMessage = apiResponseGenerator.generateMessage(apiResponseGenerator.generateMetadata());
        String message = ex.getMessage().length() > 32 ? ex.getMessage().substring(0, 32).concat("...") :
                ex.getMessage();
        ApiError apiError = new ApiError();
        apiError.setMessage(message);
        apiError.setReason(message);
        apiMessage.setError(apiError);
        apiMessage.setSuccess(false);

        if (ex instanceof NotFoundException) {
            apiError.setCode("NOT_FOUND");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_FOUND);
        } else {
            apiError.setCode("UNKNOWN");
            exchange.getResponse().setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        DataBuffer dataBuffer;
        try {
            dataBuffer = bufferFactory.wrap(objectMapper.writeValueAsBytes(apiMessage));
            exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        } catch (JsonProcessingException e) {
            log.error("Something terrible occurred, ", e);
            dataBuffer = bufferFactory.wrap("SSerialization problem occurred.".getBytes(StandardCharsets.UTF_8));
            exchange.getResponse().getHeaders().setContentType(MediaType.TEXT_PLAIN);
        }
        return exchange.getResponse().writeWith(Mono.just(dataBuffer));
    }
}
