package az.ibar.payday.configs;

import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.services.UserService;
import az.ibar.payday.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthenticationManagerService implements ReactiveAuthenticationManager {
    private final JwtUtils jwtUtils;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        String authToken = authentication.getCredentials().toString();
        try {
            if (!jwtUtils.validateToken(authToken)) {
                return Mono.empty();
            }
            Claims claims = jwtUtils.getAllClaimsFromToken(authToken);
            List<String> rolesMap = claims.get("role", List.class);
            AuthUserDetails authUserDetails = new AuthUserDetails();
            authUserDetails.setId(claims.get("userId", Long.class));
            authUserDetails.setUsername(claims.getSubject());
            if (!rolesMap.isEmpty()) {
                authUserDetails.setRole(rolesMap.get(0));
            }

            List<GrantedAuthority> authorities = new ArrayList<>();
            for (String rolemap : rolesMap) {
                authorities.add(new SimpleGrantedAuthority(rolemap));
            }
            return Mono.just(new UsernamePasswordAuthenticationToken(authUserDetails, null, authorities));
        } catch (Exception e) {
            return Mono.empty();
        }
    }
}
