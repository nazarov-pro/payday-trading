package az.ibar.payday.configs;

import az.ibar.payday.container.entities.StockSettings;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.utils.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class InitialDataConfiguration implements ApplicationRunner {
    private final StockSettingsService stockSettingsService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Constants.SAMPLE_STOCKS.forEach(
                symbol -> {
                    if(!stockSettingsService.exist(symbol)) {
                        StockSettings stockSettings = new StockSettings();
                        stockSettings.setLocked(false);
                        stockSettings.setId(symbol);
                        stockSettingsService.save(stockSettings);
                    }
                }
        );
    }
}
