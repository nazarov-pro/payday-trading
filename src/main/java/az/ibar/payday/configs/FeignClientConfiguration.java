package az.ibar.payday.configs;

import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@Configuration
@EnableFeignClients(
        basePackages = "az.ibar.payday.services"
)
public class FeignClientConfiguration {
    @Bean
    public Decoder feignDecoder() {
        return new SpringDecoder(messageConverters());
    }

    @Bean
    public Encoder feignEncoder() {
        return new SpringEncoder(messageConverters());
    }

    private ObjectFactory<HttpMessageConverters> messageConverters() {
        ObjectFactory<HttpMessageConverters> messageConverters = () -> {
            HttpMessageConverters converters = new HttpMessageConverters(
                    new MappingJackson2HttpMessageConverter()
            );
            return converters;
        };
        return messageConverters;
    }

}
