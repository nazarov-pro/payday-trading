package az.ibar.payday.services.impl;

import az.ibar.payday.container.entities.StockSettings;
import az.ibar.payday.repos.StockSettingsRepository;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.utils.TimeUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class StockSettingsServiceImpl implements StockSettingsService {
    private final StockSettingsRepository stockSettingsRepository;
    private final TimeUtils timeUtils;

    @Override
    public List<StockSettings> findAll() {
        log.info("Fetch all stocks");
        return stockSettingsRepository.findAll();
    }

    @Override
    public List<StockSettings> findAllByFeedProviderId(String feedProviderId) {
        log.info("Fetch all stocks by feed provider id ({}).", feedProviderId);
        return stockSettingsRepository.findALlByFeedProviderId(feedProviderId, true);
    }

    @Transactional
    @Override
    public List<StockSettings> updateAndGet(String feedProviderName, Integer expirationSeconds) {
        log.info("Update and fetch provider name: {}, expiration seconds: {}",
                feedProviderName, expirationSeconds);
        Long now = timeUtils.getEpochMillis();
        Long expiredMillis = now - (expirationSeconds * 1000);
        return stockSettingsRepository.updateAndGet(
                feedProviderName,
                now,
                expiredMillis
        );
    }

    @Transactional
    @Override
    public boolean unlockAll(String feedProviderName) {
        log.info("Update to unlocked all stocks of the provider name: {}", feedProviderName);
        return stockSettingsRepository.unlockAll(feedProviderName, false) > 0;
    }

    @Override
    public boolean unlockOne(String id, String feedProviderName) {
        log.info("Update to unlocked a stock(id :{}) of the provider name: {}",
                id, feedProviderName);
        return stockSettingsRepository.unlockOne(id, feedProviderName, false) > 0;
    }

    @Transactional
    @Override
    public boolean updatePrice(String id, String feedProviderName, BigDecimal lastPrice) {
        log.info("Update price of the stock(id :{}) of the provider name: {} to {}",
                id, feedProviderName, lastPrice);
        Long now = timeUtils.getEpochMillis();
        return stockSettingsRepository.updatePrice(
                lastPrice, now, id, feedProviderName
        ) > 0;
    }

    @Transactional
    @Override
    public boolean updateAllSubscribersLastTmp(String feedProviderName) {
        log.info("Update all of the stocks of the provider name: {}", feedProviderName);
        Long now = timeUtils.getEpochMillis();
        return stockSettingsRepository.updateAllSubscribersLastTmp(
                now, feedProviderName
        ) > 0;
    }

    @Transactional
    @Override
    public StockSettings save(StockSettings stockSettings) {
        log.info("Save stock {}", stockSettings.getId());
        return stockSettingsRepository.findById(stockSettings.getId())
                .orElse(stockSettingsRepository.save(stockSettings));
    }

    @Override
    public StockSettings create(String symbol) {
        log.info("Stock create request (id: {})", symbol);
        StockSettings stockSettings = new StockSettings();
        stockSettings.setId(symbol);
        return save(stockSettings);
    }

    @Transactional
    @Override
    public String delete(String id) {
        log.info("Delete stock {}", id);
        if (exist(id)) {
            stockSettingsRepository.deleteById(id);
        }
        return id;
    }

    @Override
    public boolean exist(String id) {
        log.info("Check stock exist for {}", id);
        return stockSettingsRepository.existsById(id);
    }
}
