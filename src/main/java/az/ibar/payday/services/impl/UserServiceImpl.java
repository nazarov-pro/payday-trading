package az.ibar.payday.services.impl;

import az.ibar.payday.container.dtos.UserActivationRequestDTO;
import az.ibar.payday.container.dtos.UserRequestDTO;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.UserStatusesEnum;
import az.ibar.payday.container.exceptions.NotFoundException;
import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.repos.UserRepository;
import az.ibar.payday.services.UserService;
import az.ibar.payday.services.MailSenderService;
import az.ibar.payday.utils.converters.UserRequestConverter;
import az.ibar.payday.utils.converters.ActivationMailMessageConverter;
import az.ibar.payday.utils.converters.UserDetailsConverter;
import az.ibar.payday.utils.generators.UUIDGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserRequestConverter userRequestConverter;
    private final UserDetailsConverter userDetailsConverter;
    private final UUIDGenerator uuidGenerator;
    private final MailSenderService mailSenderService;
    private final ActivationMailMessageConverter mailMessageConverter;


    @Override
    public boolean checkUsernameOrEmailIsTaken(String username, String email) {
        log.info("Checking if any user registered by email: {} or username: {}", username, email);
        return Optional.ofNullable(
                userRepository.existsByUsernameOrEmail(
                        username,
                        email,
                        UserStatusesEnum.DELETED //excluded
                )).orElse(false);
    }

    @Override
    @Transactional
    public void registerUser(UserRequestDTO userRequestDto) {
        log.info("Register user request received for email: {} and username: {}",
                userRequestDto.getEmail(), userRequestDto.getUsername());
        boolean taken = checkUsernameOrEmailIsTaken(userRequestDto.getUsername(), userRequestDto.getEmail());
        if (taken) {
            throw new IllegalArgumentException("Email or username has been taken.");
        }

        User user = userRequestConverter.convertSafely(userRequestDto)
                .orElseThrow(() -> new IllegalArgumentException("Validation of request failed."));
        user.setActivationToken(uuidGenerator.generate());
        user.setStatus(UserStatusesEnum.DEACTIVATED);
        user = userRepository.save(user);
        MailMessage mailMessage = mailMessageConverter.convertSafely(user)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));
        mailSenderService.send(mailMessage);
    }

    @Override
    @Transactional
    public void activateUser(UserActivationRequestDTO userActivationRequestDto) {
        log.info("Activate user request received by id: {} and token: {}",
                userActivationRequestDto.getId(), userActivationRequestDto.getActivationCode()
        );
        User user = userRepository.findById(userActivationRequestDto.getId())
                .orElseThrow(() -> new NotFoundException("User not found"));
        if (!user.getStatus().equals(UserStatusesEnum.DEACTIVATED)) {
            throw new IllegalArgumentException("Invalid request ");
        } else if (!user.getEmail().equals(userActivationRequestDto.getEmail())) {
            throw new NotFoundException("User not found.");
        } else if (!user.getActivationToken().equals(userActivationRequestDto.getActivationCode())) {
            throw new IllegalArgumentException("Invalid request.");
        }
        Integer updatedRows = userRepository.updateStatus(user.getId(), UserStatusesEnum.ACTIVE);
        if (updatedRows == 0) {
            throw new NotFoundException("User not found.");
        }
    }

    @Override
    public AuthUserDetails findByUsernameOrEmail(String usernameOrEmail) {
        log.info("Find user by email or username: {} request received", usernameOrEmail);
        return userDetailsConverter.convertSafely(
                userRepository.findOneByUsernameOrEmail(usernameOrEmail, UserStatusesEnum.ACTIVE)
        ).orElseThrow(() -> new NotFoundException("Username or email is not found."));
    }

    @Override
    public User getById(Long id) {
        log.info("Get user by id: {} request received", id);
        return userRepository.getOne(id);
    }

    @Override
    public Optional<User> findById(Long id) {
        log.info("Find user by id: {} request received", id);
        return userRepository.findById(id);
    }

    @Transactional
    @Override
    public User save(User user) {
        log.info("Save user email: {} request received", user.getEmail());
        return userRepository.save(user);
    }
}