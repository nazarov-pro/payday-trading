package az.ibar.payday.services.impl;

import az.ibar.payday.container.dtos.PurchasedStockResponseDTO;
import az.ibar.payday.container.entities.PurchasedStock;
import az.ibar.payday.container.exceptions.NotFoundException;
import az.ibar.payday.repos.PurchasedStockRepository;
import az.ibar.payday.services.PurchasedStockService;
import az.ibar.payday.utils.TimeUtils;
import az.ibar.payday.utils.converters.PurchasedStockDtoConverter;
import az.ibar.payday.utils.generators.UUIDGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PurchasedStockServiceImpl implements PurchasedStockService {
    private final PurchasedStockRepository purchasedStockRepository;
    private final PurchasedStockDtoConverter dtoConverter;
    private final TimeUtils timeUtils;
    private final UUIDGenerator uuidGenerator;

    @Transactional
    @Override
    public PurchasedStockResponseDTO save(PurchasedStock purchasedStock) {
        log.info("Save purchased stock received, symbol: {}, unit: {}", purchasedStock.getSymbol(),
                purchasedStock.getSymbolUnit());
        PurchasedStock existPurchasedStock = findOneRawByUserIdAndSymbol(purchasedStock.getUser().getId(),
                purchasedStock.getSymbol());
        if(existPurchasedStock != null) {
            existPurchasedStock.setSymbolUnit(existPurchasedStock.getSymbolUnit()
                    .add(purchasedStock.getSymbolUnit()));
            existPurchasedStock.setUpdatedTimeStamp(timeUtils.getEpochMillis());
            purchasedStock = existPurchasedStock;
        } else {
            purchasedStock.setCreatedTimestamp(timeUtils.getEpochMillis());
        }
        purchasedStock.setVersion(uuidGenerator.generate());
        return dtoConverter.convert(purchasedStockRepository.save(purchasedStock));
    }

    @Override
    public List<PurchasedStockResponseDTO> findAllByUserId(Long userId) {
        log.info("Find all purchased stocks request received (for user id: {})", userId);
        return purchasedStockRepository.findAllByUserId(userId).stream().map(
                dtoConverter::convert
        ).collect(Collectors.toList());
    }

    @Override
    public PurchasedStockResponseDTO findOneByUserIdAndSymbol(Long userId, String symbol) {
        log.info("Find a purchased stocks request received (for user id: {} and symbol: {})", userId, symbol);
        return dtoConverter.convertSafely(purchasedStockRepository.findOneByUserAndSymbol(userId, symbol))
                .orElseThrow(() -> new NotFoundException(""));
    }

    @Override
    public PurchasedStock findOneRawByUserIdAndSymbol(Long userId, String symbol) {
        log.info("Find a raw purchased stocks request received (for user id: {} and symbol: {})", userId, symbol);
        return purchasedStockRepository.findOneByUserAndSymbol(userId, symbol);
    }
}
