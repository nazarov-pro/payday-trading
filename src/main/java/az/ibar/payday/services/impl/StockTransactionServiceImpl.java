package az.ibar.payday.services.impl;

import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.dtos.StockOrderRequestDTO;
import az.ibar.payday.container.dtos.StockOrderResponseDTO;
import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.container.entities.PurchasedStock;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.StockTransactionModesEnum;
import az.ibar.payday.container.enums.StockTransactionStatusesEnum;
import az.ibar.payday.container.exceptions.NotFoundException;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.repos.StockTransactionRepository;
import az.ibar.payday.services.*;
import az.ibar.payday.services.trading.TradingService;
import az.ibar.payday.utils.Constants;
import az.ibar.payday.utils.TimeUtils;
import az.ibar.payday.utils.converters.StockOrderResponseDtoConverter;
import az.ibar.payday.utils.converters.StockTransactionConverter;
import az.ibar.payday.utils.generators.UUIDGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StockTransactionServiceImpl implements StockTransactionService {
    private final StockTransactionRepository stockTransactionRepository;
    private final StockOrderResponseDtoConverter responseDtoConverter;
    private final StockTransactionConverter stockTransactionConverter;
    private final BankAccountService bankAccountService;
    private final MailSenderService mailSenderService;
    private final PurchasedStockService purchasedStockService;
    private final StockSettingsService stockSettingsService;
    private final UserService userService;
    private final TimeUtils timeUtils;
    private final UUIDGenerator uuidGenerator;
    @Qualifier(Constants.BEAN_FINN_HUB_NAME)
    private final TradingService tradingService;

    @Override
    public List<StockOrderResponseDTO> findAllByUserId(Long userId) {
        log.info("Fetch all stock orders received by user id: {}", userId);
        return stockTransactionRepository.findAllByUserId(userId)
                .stream().map(responseDtoConverter::convert).collect(Collectors.toList());
    }

    @Override
    public List<StockResponseDTO> fetchAllStocksBySymbols(String symbols) {
        if (symbols == null || symbols.isEmpty()) {
            return stockSettingsService.findAll().stream().map(
                    stockSettings -> tradingService.get(stockSettings.getId())
            ).collect(Collectors.toList());
        } else {
            return Arrays.stream(symbols.split(","))
                    .map(String::trim)
                    .filter(symbol -> !symbol.isEmpty())
                    .map(tradingService::get)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public StockOrderResponseDTO findOneByUserId(Long id, Long userId) {
        log.info("Fetch s stock order(id: {}) received by user id: {}", id, userId);
        return responseDtoConverter.convertSafely(stockTransactionRepository.findOneByUserId(
                id, userId)).orElseThrow(() -> new NotFoundException("Stock Order not found."));
    }

    @Transactional
    @Override
    public StockOrderResponseDTO sell(StockOrderRequestDTO stockOrderRequestDto) {
        if (!stockSettingsService.exist(stockOrderRequestDto.getSymbol())) {
            throw new NotFoundException("Stock is not found, please subscribe for stock first.");
        }

        BankAccount bankAccount = bankAccountService.getById(stockOrderRequestDto.getBankAccountId())
                .orElseThrow(() -> new NotFoundException("Bank account not found."));

        if (!bankAccount.getUser().getId().equals(stockOrderRequestDto.getUserId())) {
            throw new NotFoundException("Bank account not found.");
        }
        PurchasedStock purchasedStock = purchasedStockService.findOneRawByUserIdAndSymbol(
                stockOrderRequestDto.getUserId(),
                stockOrderRequestDto.getSymbol()
        );
        if (purchasedStock == null) {
            throw new IllegalArgumentException("You dont have this stock to sell");
        } else if (purchasedStock.getSymbolUnit().compareTo(stockOrderRequestDto.getSymbolUnit()) < 0) {
            throw new IllegalArgumentException("You dont have enough quantity of this  stock to sell");
        }

        StockTransaction stockTransaction = stockTransactionConverter.convert(stockOrderRequestDto);
        stockTransaction.setCreatedTimestamp(timeUtils.getEpochMillis());
        StockResponseDTO stockResponse = tradingService.get(stockOrderRequestDto.getSymbol());
        stockTransaction.setStartAmount(stockResponse.getPrice());
        stockTransaction.setStatus(StockTransactionStatusesEnum.ACTIVE);
        stockTransaction.setVersion(uuidGenerator.generate());
        stockTransaction = stockTransactionRepository.save(stockTransaction);
        return responseDtoConverter.convertSafely(stockTransaction)
                .orElseThrow(() -> new IllegalArgumentException("Operation failed."));

    }

    @Transactional
    @Override
    public StockOrderResponseDTO buy(StockOrderRequestDTO stockOrderRequestDto) {
        if (!stockSettingsService.exist(stockOrderRequestDto.getSymbol())) {
            throw new NotFoundException("Stock is not found, please subscribe for stock first.");
        }

        BankAccount bankAccount = bankAccountService.getById(stockOrderRequestDto.getBankAccountId())
                .orElseThrow(() -> new NotFoundException("Bank account not found."));
        if (!bankAccount.getUser().getId().equals(stockOrderRequestDto.getUserId())) {
            throw new NotFoundException("Bank account not found.");
        } else if (
                bankAccount.getBalance().compareTo(
                        stockOrderRequestDto.getTargetAmount()
                                .multiply(stockOrderRequestDto.getSymbolUnit())) < 0
        ) {
            throw new IllegalArgumentException("Insufficient funds.");
        }
        StockTransaction stockTransaction = stockTransactionConverter.convert(stockOrderRequestDto);
        stockTransaction.setCreatedTimestamp(timeUtils.getEpochMillis());
        StockResponseDTO stockResponse = tradingService.get(stockOrderRequestDto.getSymbol());
        stockTransaction.setStartAmount(stockResponse.getPrice());
        stockTransaction.setStatus(StockTransactionStatusesEnum.ACTIVE);
        stockTransaction.setVersion(uuidGenerator.generate());
        stockTransaction = stockTransactionRepository.save(stockTransaction);
        return responseDtoConverter.convertSafely(stockTransaction)
                .orElseThrow(() -> new IllegalArgumentException("Operation failed."));
    }

    @Override
    public void handleStockUpdate(StockResponseDTO stockResponseDto) {
        log.info("Handle stock update id: {}, price: {}", stockResponseDto.getSymbol(), stockResponseDto.getPrice());
        List<StockTransaction> qualifiedTransactions = stockTransactionRepository.findAllQualifiedTransactions(
                stockResponseDto.getSymbol(),
                StockTransactionStatusesEnum.ACTIVE,
                StockTransactionModesEnum.SELL.getValue(),
                StockTransactionModesEnum.BUY.getValue(),
                stockResponseDto.getPrice()
        );

        qualifiedTransactions.forEach(
                transaction -> {
                    try {
                        if (transaction.getMode().equals(StockTransactionModesEnum.BUY.getValue())) {
                            buyOperation(transaction, stockResponseDto);
                        } else if (transaction.getMode().equals(StockTransactionModesEnum.SELL.getValue())) {
                            sellOperation(transaction, stockResponseDto);
                        } else {
                            log.error("Could not identify transaction's mode {}", transaction.getMode());
                        }
                    } catch (RuntimeException ex) {
                        log.error("Runtime exception occurred, ", ex);
                    }
                }
        );
    }

    @Transactional
    @Override
    public void buyOperation(StockTransaction stockTransaction, StockResponseDTO stockResponseDto) {
        log.info("Buy operation for stransaction id: {}", stockTransaction.getId());
        BigDecimal requiredFunds = stockTransaction.getSymbolUnit().multiply(stockResponseDto.getPrice());
        BankAccount bankAccount = bankAccountService.getById(stockTransaction.getBankAccount().getId())
                .orElseThrow(() -> new NotFoundException("Bank account not found"));
        MailMessage mailMessage = new MailMessage();
        if (bankAccount.getBalance().compareTo(requiredFunds) >= 0) {
            BankAccountResponseDTO bankAccountResponseDto = bankAccountService.withdraw(
                    bankAccount.getId(), requiredFunds, bankAccount.getVersion()
            );

            if (bankAccountResponseDto != null &&
                    bankAccountResponseDto.getBalance().compareTo(BigDecimal.ZERO) >= 0) {
                boolean isUpdated = updateStatus(
                        stockTransaction.getId(),
                        stockTransaction.getVersion(),
                        StockTransactionStatusesEnum.COMPLETED,
                        stockResponseDto.getPrice()
                );

                if (!isUpdated) {
                    throw new RuntimeException("Failed to update transaction status.");
                }

                PurchasedStock purchasedStock = new PurchasedStock();
                purchasedStock.setSymbol(stockTransaction.getSymbol());
                purchasedStock.setSymbolUnit(stockTransaction.getSymbolUnit());
                purchasedStock.setUser(bankAccount.getUser());
                purchasedStockService.save(purchasedStock);
                mailMessage.setContent(
                        String.format("Your order %d, successfully completed.", stockTransaction.getId())
                );
            }

        } else {
            mailMessage.setContent(
                    String.format("Your order %d was failed. Insufficient funds.", stockTransaction.getId())
            );

            log.error("Insufficient funds.");
            boolean isUpdated = updateStatus(
                    stockTransaction.getId(),
                    stockTransaction.getVersion(),
                    StockTransactionStatusesEnum.DEACTIVATED,
                    BigDecimal.ZERO
            );
            if (isUpdated) {
                log.info("Transaction settled.");
            }
        }
        User user = userService.findById(bankAccount.getUser().getId())
                .orElseThrow(() -> new NotFoundException("User not found."));
        mailMessage.setSubject("Transaction status");
        mailMessage.setRecipient(user.getEmail());
        mailSenderService.send(mailMessage);
    }

    @Transactional
    @Override
    public void sellOperation(StockTransaction stockTransaction, StockResponseDTO stockResponseDto) {
        log.info("Sell operation for stransaction id: {}", stockTransaction.getId());
        BigDecimal requiredSymbolUnits = stockTransaction.getSymbolUnit();
        BankAccount bankAccount = bankAccountService.getById(stockTransaction.getBankAccount().getId())
                .orElseThrow(() -> new NotFoundException("Bank account not found"));
        PurchasedStock purchasedStock = purchasedStockService.findOneRawByUserIdAndSymbol(
                bankAccount.getUser().getId(),
                stockResponseDto.getSymbol()
        );
        MailMessage mailMessage = new MailMessage();
        if (purchasedStock.getSymbolUnit().compareTo(requiredSymbolUnits) >= 0) {
            purchasedStock.setSymbol(stockTransaction.getSymbol());
            purchasedStock.setSymbolUnit(stockTransaction.getSymbolUnit().negate());
            purchasedStock.setUser(bankAccount.getUser());
            purchasedStockService.save(purchasedStock);

            boolean isUpdated = updateStatus(
                    stockTransaction.getId(),
                    stockTransaction.getVersion(),
                    StockTransactionStatusesEnum.COMPLETED,
                    stockResponseDto.getPrice()
            );

            if (!isUpdated) {
                throw new RuntimeException("Failed to update transaction status.");
            }

            BankAccountResponseDTO bankAccountResponseDto = bankAccountService.topUp(
                    bankAccount.getId(),
                    stockResponseDto.getPrice().multiply(stockTransaction.getSymbolUnit()),
                    bankAccount.getVersion()
            );
            if (bankAccountResponseDto == null ||
                    bankAccountResponseDto.getBalance().compareTo(BigDecimal.ZERO) < 0) {
                throw new RuntimeException("Failed to top up user's balance.");
            }
            mailMessage.setContent(
                    String.format("Your order %d, successfully completed.", stockTransaction.getId())
            );
        } else {
            log.error("Insufficient funds.");
            boolean isUpdated = updateStatus(
                    stockTransaction.getId(),
                    stockTransaction.getVersion(),
                    StockTransactionStatusesEnum.DEACTIVATED,
                    BigDecimal.ZERO
            );
            if (isUpdated) {
                log.info("Transaction settled.");
            }
            mailMessage.setContent(
                    String.format("Your order %d was failed. Insufficient stock units.", stockTransaction.getId())
            );
        }
        User user = userService.findById(bankAccount.getUser().getId())
                .orElseThrow(() -> new NotFoundException("User not found."));
        mailMessage.setSubject("Transaction status");
        mailMessage.setRecipient(user.getEmail());
        mailSenderService.send(mailMessage);
    }


    @Override
    public boolean updateStatus(Long id, String version, StockTransactionStatusesEnum status, BigDecimal finalAmount) {
        log.info("Updating stock transaction id: {}, final amount: {}, status: {}, version: {}",
                id, finalAmount, status, version);
        String newVersion = uuidGenerator.generate();
        return stockTransactionRepository.updateStatus(
                id, status, version, newVersion, finalAmount
        ) > 0;
    }

    @Override
    public StockTransaction save(StockTransaction stockTransaction) {
        log.info(
                "Stock transaction save request received. (id: {}, symbol: {}, symbol unit: {})",
                stockTransaction.getId(), stockTransaction.getSymbol(), stockTransaction.getSymbolUnit()
        );
        stockTransaction.setVersion(uuidGenerator.generate());
        stockTransaction.setCreatedTimestamp(timeUtils.getEpochMillis());
        stockTransaction.setUpdatedTimestamp(timeUtils.getEpochMillis());
        return stockTransactionRepository.save(stockTransaction);
    }
}
