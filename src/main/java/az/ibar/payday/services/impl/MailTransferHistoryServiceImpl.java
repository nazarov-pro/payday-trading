package az.ibar.payday.services.impl;

import az.ibar.payday.container.entities.MailTransferHistory;
import az.ibar.payday.repos.MailTransferHistoryRepository;
import az.ibar.payday.services.MailTransferHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class MailTransferHistoryServiceImpl implements MailTransferHistoryService {
    private final MailTransferHistoryRepository mailTransferHistoryRepository;

    @Transactional
    @Override
    public void save(MailTransferHistory mailTransferHistory) {
        log.info("Mail transfer history save request received.");
        mailTransferHistoryRepository.save(mailTransferHistory);
    }
}
