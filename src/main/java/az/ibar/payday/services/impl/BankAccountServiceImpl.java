package az.ibar.payday.services.impl;

import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.repos.BankAccountRepository;
import az.ibar.payday.services.BankAccountService;
import az.ibar.payday.utils.converters.BankAccountDtoConverter;
import az.ibar.payday.utils.generators.UUIDGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class BankAccountServiceImpl implements BankAccountService {
    private final BankAccountRepository bankAccountRepository;
    private final BankAccountDtoConverter bankAccountDtoConverter;
    private final UUIDGenerator uuidGenerator;

    @Override
    public List<BankAccountResponseDTO> list(Long userId) {
        log.info("Fetch all bank accounts of user(id: {}) request received.", userId);
        return bankAccountRepository.findAllByUserId(userId).stream()
                .map(bankAccountDtoConverter::convert).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public BankAccountResponseDTO create(Long userId) {
        log.info("Bank account create request received by user(id: {})", userId);
        User user = new User();
        user.setId(userId);
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(BigDecimal.ZERO);
        bankAccount.setUser(user);
        bankAccount.setVersion(uuidGenerator.generate());
        bankAccount = bankAccountRepository.save(bankAccount);
        return bankAccountDtoConverter.convertSafely(bankAccount)
                .orElseThrow(() -> new IllegalArgumentException("Operation failed."));
    }

    @Transactional
    @Override
    public BankAccountResponseDTO topUp(Long bankAccountId, BigDecimal amount, String version) {
        log.info("Top up bank account request received for bank account id: {} and amount: {}",
                bankAccountId, amount);
        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Negative amount is not supported.");
        }
        String newVersion = uuidGenerator.generate();
        BankAccount bankAccount = bankAccountRepository.updateAndGet(bankAccountId, amount, newVersion, version);
        return bankAccountDtoConverter.convertSafely(bankAccount)
                .orElseThrow(() -> new IllegalArgumentException("Operation failed."));
    }

    @Transactional
    @Override
    public BankAccountResponseDTO withdraw(Long bankAccountId, BigDecimal amount, String version) {
        log.info("Withdraw bank account request received for bank account id: {} and amount: {}",
                bankAccountId, amount
        );
        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            amount = amount.negate();
        }
        String newVersion = uuidGenerator.generate();
        BankAccount bankAccount = bankAccountRepository.updateAndGet(bankAccountId, amount, newVersion, version);
        return bankAccountDtoConverter.convertSafely(bankAccount)
                .orElseThrow(() -> new IllegalArgumentException("Operation failed."));

    }

    @Override
    public Boolean checkUserIdAndBankAccountId(Long userId, Long bankAccountId) {
        log.info("Check bank account(id: {}) belongs to user(id: {}) request received.",
                bankAccountId, userId);
        return bankAccountRepository.existsByIdAndUserId(bankAccountId, userId);
    }

    @Override
    public Optional<BankAccount> getById(Long id) {
        log.info("Get a bank account by id :{} request received", id);
        return bankAccountRepository.findById(id);
    }
}
