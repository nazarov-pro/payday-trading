package az.ibar.payday.services.impl;

import az.ibar.payday.container.entities.MailTransferHistory;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.services.MailSenderService;
import az.ibar.payday.services.MailTransferHistoryService;
import az.ibar.payday.utils.TimeUtils;
import az.ibar.payday.utils.converters.MailTransferHistoryConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Service
@RequiredArgsConstructor
@Slf4j
public class MailSenderServiceImpl implements MailSenderService {
    private final JavaMailSender javaMailSender;
    private final MailTransferHistoryService mailTransferHistoryService;
    private final MailTransferHistoryConverter mailTransferHistoryConverter;
    private final TimeUtils timeUtils;

    @Override
    @Async
    public Future<Boolean> send(MailMessage mailMessage) {
        boolean success = true;
        MailTransferHistory mailTransferHistory = mailTransferHistoryConverter
                .convertSafely(mailMessage)
                .orElseThrow(() -> new IllegalArgumentException("Mail transfer history converting failed"));
        mailTransferHistory.setLastUpdateTimestamp(timeUtils.getEpochMillis());
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(mailMessage.getRecipient());
        simpleMailMessage.setSubject(mailMessage.getSubject());
        simpleMailMessage.setText(mailMessage.getContent());
        simpleMailMessage.setFrom("payday@shahinnazarov.com");
        try {
            javaMailSender.send(simpleMailMessage);
            log.debug("Sending mail to {} successfully completed.", mailMessage.getRecipient());
        } catch (MailException mailException) {
            log.error("Sending mail to {} was failed.", mailMessage.getRecipient(), mailException);
            success = false;
        }
        mailTransferHistory.setSent(success);
        mailTransferHistoryService.save(mailTransferHistory);
        return CompletableFuture.completedFuture(success);
    }
}
