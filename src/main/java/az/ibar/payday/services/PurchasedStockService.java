package az.ibar.payday.services;

import az.ibar.payday.container.dtos.PurchasedStockResponseDTO;
import az.ibar.payday.container.entities.PurchasedStock;

import java.util.List;

public interface PurchasedStockService {
    PurchasedStockResponseDTO save(PurchasedStock purchasedStock);
    List<PurchasedStockResponseDTO> findAllByUserId(Long userId);
    PurchasedStockResponseDTO findOneByUserIdAndSymbol(Long userId, String symbol);
    //INTERNAL
    PurchasedStock findOneRawByUserIdAndSymbol(Long userId, String symbol);

}
