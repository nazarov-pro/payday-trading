package az.ibar.payday.services;

import az.ibar.payday.container.BaseService;
import az.ibar.payday.container.entities.StockSettings;

import java.math.BigDecimal;
import java.util.List;

public interface StockSettingsService extends BaseService {
    List<StockSettings> findAll();

    List<StockSettings> findAllByFeedProviderId(
            String feedProviderId
    );

    List<StockSettings> updateAndGet(
            String feedProviderName,
            Integer expirationSeconds
    );

    boolean unlockAll(String feedProviderName);

    boolean unlockOne(String id, String feedProviderName);

    boolean updatePrice(
            String id,
            String feedProviderName,
            BigDecimal lastPrice
    );

    boolean updateAllSubscribersLastTmp(String feedProviderName);

    String delete(String id);

    StockSettings save(StockSettings stockSettings);

    StockSettings create(String symbol);

    boolean exist(String id);
}
