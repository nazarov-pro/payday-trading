package az.ibar.payday.services;

import az.ibar.payday.container.BaseService;
import az.ibar.payday.container.dtos.UserActivationRequestDTO;
import az.ibar.payday.container.dtos.UserRequestDTO;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.models.AuthUserDetails;

import java.util.Optional;

public interface UserService extends BaseService {
    boolean checkUsernameOrEmailIsTaken(String username, String email);

    void registerUser(UserRequestDTO userRequestDto);

    void activateUser(UserActivationRequestDTO userActivationRequestDto);

    AuthUserDetails findByUsernameOrEmail(String usernameOrEmail);

    //INTERNAL
    User getById(Long id);

    Optional<User> findById(Long id);

    User save(User user);
}
