package az.ibar.payday.services;

import az.ibar.payday.container.BaseService;
import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.entities.BankAccount;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface BankAccountService extends BaseService {
    List<BankAccountResponseDTO> list(Long userId);

    BankAccountResponseDTO create(Long userId);

    BankAccountResponseDTO topUp(Long bankAccountId, BigDecimal amount, String version);

    BankAccountResponseDTO withdraw(Long bankAccountId, BigDecimal amount, String version);

    Boolean checkUserIdAndBankAccountId(Long userId, Long bankAccountId);

    //INTERNAL
    Optional<BankAccount> getById(Long id);
}
