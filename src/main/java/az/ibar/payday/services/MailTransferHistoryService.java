package az.ibar.payday.services;

import az.ibar.payday.container.BaseService;
import az.ibar.payday.container.entities.MailTransferHistory;

public interface MailTransferHistoryService extends BaseService {
    void save(MailTransferHistory mailTransferHistory);
}
