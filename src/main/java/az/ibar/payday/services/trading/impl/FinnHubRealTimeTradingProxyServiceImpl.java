package az.ibar.payday.services.trading.impl;

import az.ibar.payday.services.trading.FinnHubRealTimeTradingService;
import az.ibar.payday.utils.Constants;
import az.ibar.payday.utils.generators.UUIDGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service(Constants.BEAN_REALTIME_FINN_HUB_PROXY_NAME)
@RequiredArgsConstructor
@Slf4j
public class FinnHubRealTimeTradingProxyServiceImpl implements FinnHubRealTimeTradingService {
    @Value("#{environment.POD_NAME}")
    private final String podName;
    @Qualifier(Constants.BEAN_REALTIME_FINN_HUB_NAME_HTTP_LONG_POLLING)
    private final FinnHubRealTimeTradingService finnHubRealTimeHttpTradingService;
    @Qualifier(Constants.BEAN_REALTIME_FINN_HUB_NAME_WEB_SOCKET)
    private final FinnHubRealTimeTradingService finnHubRealTimeWebSocketTradingService;

    private final UUIDGenerator uuidGenerator;

    private FinnHubRealTimeTradingService finnHubRealTimeTradingService;
    private String uniqueAppIdentifier;

    @PostConstruct
    public void init() {
        try {
            finnHubRealTimeWebSocketTradingService.start(); //websocket has a priority
            this.finnHubRealTimeTradingService = finnHubRealTimeWebSocketTradingService;
        } catch (RuntimeException e) {
            log.error(
                    "Failed to start {} switching to {}.",
                    finnHubRealTimeWebSocketTradingService.name(),
                    finnHubRealTimeHttpTradingService.name()
            );
            finnHubRealTimeHttpTradingService.start();
            this.finnHubRealTimeTradingService = finnHubRealTimeHttpTradingService;
        }
        if (podName == null || podName.isEmpty()) {
            uniqueAppIdentifier = this.finnHubRealTimeTradingService.name().concat(":")
                    .concat(uuidGenerator.generate());
        } else {
            uniqueAppIdentifier = this.finnHubRealTimeTradingService.name().concat(":")
                    .concat(podName);
        }
    }

    @Override
    public void subscribe(String symbol) {
        finnHubRealTimeTradingService.subscribe(symbol);
    }

    @Override
    public void unsubscribe(String symbol) {
        finnHubRealTimeTradingService.unsubscribe(symbol);
    }

    @Override
    public String name() {
        return finnHubRealTimeTradingService.name()
                .concat(uniqueAppIdentifier);
    }

    @Override
    public void start() {
        throw new UnsupportedOperationException("Start only supported in trading services(ws, http).");
    }
}
