package az.ibar.payday.services.trading.impl;

import az.ibar.payday.container.dtos.FinnhubStockQuoteDTO;
import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.services.trading.FinnHubTradingService;
import az.ibar.payday.services.trading.FinnHubTradingV1Client;
import az.ibar.payday.utils.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service(Constants.BEAN_FINN_HUB_NAME)
@RequiredArgsConstructor
@Slf4j
@CacheConfig(cacheNames = "finnhub_trading")
public class FinnHubTradingServiceImpl implements FinnHubTradingService {
    @Value("${app.integration.finnhub.token}")
    private final String token;
    private final FinnHubTradingV1Client client;

    @Override
    @Cacheable(key = "'finnhub_stock_' + #symbol", unless = "#result == null")
    public StockResponseDTO get(String symbol) {
        log.info("sending request to finnhub for {} stock.", symbol);
        FinnhubStockQuoteDTO finnhubStockQuoteDto = client.fetchQuota(token, symbol);
        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setPrice(finnhubStockQuoteDto.getCurrentPrice());
        stockResponseDto.setSymbol(symbol);
        return stockResponseDto;
    }
}
