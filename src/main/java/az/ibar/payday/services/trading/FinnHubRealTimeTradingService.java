package az.ibar.payday.services.trading;

public interface FinnHubRealTimeTradingService extends RealTimeTradingService {
    void start();
}
