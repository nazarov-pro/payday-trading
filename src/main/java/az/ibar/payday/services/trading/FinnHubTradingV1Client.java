package az.ibar.payday.services.trading;

import az.ibar.payday.container.dtos.FinnhubStockQuoteDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "finnhub-v1", url = "${feign.client.config.finnhub-v1.url}"
)
public interface FinnHubTradingV1Client {

    @GetMapping(value = "/quote", consumes = MediaType.APPLICATION_JSON_VALUE)
    FinnhubStockQuoteDTO fetchQuota(
            @RequestParam("token") String token,
            @RequestParam("symbol") String symbol
    );
}
