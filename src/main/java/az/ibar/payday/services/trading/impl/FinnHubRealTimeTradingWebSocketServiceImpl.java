package az.ibar.payday.services.trading.impl;

import az.ibar.payday.container.dtos.FinnHubRealtimeTradingDTO;
import az.ibar.payday.container.dtos.FinnHubRealtimeTradingDataDTO;
import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.services.StockTransactionService;
import az.ibar.payday.services.trading.FinnHubRealTimeTradingService;
import az.ibar.payday.utils.Constants;
import az.ibar.payday.utils.WebSocketUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service(Constants.BEAN_REALTIME_FINN_HUB_NAME_WEB_SOCKET)
@ClientEndpoint
@RequiredArgsConstructor
@Slf4j
public class FinnHubRealTimeTradingWebSocketServiceImpl implements FinnHubRealTimeTradingService {
    private Session userSession = null;
    private final Set<String> symbols = new ConcurrentSkipListSet<>();
    private final Lock bootingLock = new ReentrantLock(true);

    @Value("${app.integration.finnhub.token}")
    private final String token;
    @Qualifier("json")
    private final ObjectMapper objectMapper;
    private final StockTransactionService stockTransactionService;
    private final StockSettingsService stockSettingsService;
    private final WebSocketUtils webSocketUtils;

    @Override
    public void start() {
        try {
            bootingLock.lock();
            URI uri = URI.create(String.format("wss://ws.finnhub.io?token=%s", token));
            WebSocketContainer container = webSocketUtils.getContainer();
            container.connectToServer(this, uri);
        } catch (DeploymentException | IOException e) {
            log.error("Web socket connection failed.");
            throw new RuntimeException(e);
        } finally {
            bootingLock.unlock();
        }
    }

    /**
     * Callback hook for Connection open events.
     *
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {
        log.info("{} opening.", name());
        this.userSession = userSession;
    }

    /**
     * Callback hook for Connection close events.
     *
     * @param userSession the userSession which is getting closed.
     * @param reason      the reason for connection close
     */
    @OnClose
    public void onClose(Session userSession, CloseReason reason) {
        log.info("{} closing.", name());
        this.userSession = null;
        if (stockSettingsService.unlockAll(name())) {
            log.info("Stocks unlocked.");
        } else {
            log.warn("Any locked symbols are not found.");
        }
        symbols.clear();
    }

    /**
     * Callback hook for Message Events. This method will be invoked when a client send a message.
     *
     * @param message The text message
     */
    @OnMessage
    public void onMessage(String message) {
        log.debug("Message received. message: {}", message);
        try {
            FinnHubRealtimeTradingDTO finnHubRealtimeTradingDto = objectMapper.readValue(
                    message, FinnHubRealtimeTradingDTO.class
            );
            if (finnHubRealtimeTradingDto.getType().equals("trade")) {
                finnHubRealtimeTradingDto.getData().forEach(this::handle);
            }
        } catch (IOException e) {
            log.error("IO Exception occurred.", e);
        }
    }

    @PreDestroy
    public void close() {
        log.info("CLosing...");
        if (userSession != null) {
            try {
                bootingLock.lock();
                userSession.close();
            } catch (IOException e) {
                log.error("Unable to close session.");
            } finally {
                bootingLock.unlock();
            }
        }
    }

    @Override
    public void subscribe(String symbol) {
        log.info("Subscribe for {}", symbol);
        symbols.add(symbol);
        if (userSession == null) {
            log.warn("User connection establishing(symbol: {})", symbol);
            start();
        } else {
            String message = String.format("{\"type\":\"subscribe\",\"symbol\":\"%s\"}", symbol);
            log.info("Message is sending to the broker. message: {}", message);
            userSession.getAsyncRemote().sendText(message);
        }
    }

    @Override
    public void unsubscribe(String symbol) {
        log.info("Unsubscribe for {}", symbol);
        if (symbols.contains(symbol)) {
            if (userSession != null) {
                close();
                symbols.remove(symbol);
                start();
            } else {
                symbols.remove(symbol);
            }
        } else {
            log.warn("{} symbol is not found.", symbol);
        }
    }

    public void handle(FinnHubRealtimeTradingDataDTO data) {
        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(data.getSymbol());
        stockResponseDto.setPrice(data.getLastPrice());
        stockSettingsService.updatePrice(data.getSymbol(), name(),
                stockResponseDto.getPrice());
        stockTransactionService.handleStockUpdate(stockResponseDto);
    }

    @Override
    public String name() {
        return Constants.REAL_TIME_FINN_HUB_NAME_WEB_SOCKET;
    }
}
