package az.ibar.payday.services.trading.impl;

import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.services.StockTransactionService;
import az.ibar.payday.services.trading.FinnHubRealTimeTradingService;
import az.ibar.payday.services.trading.TradingService;
import az.ibar.payday.utils.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service(Constants.BEAN_REALTIME_FINN_HUB_NAME_HTTP_LONG_POLLING)
@Slf4j
@RequiredArgsConstructor
public class FinnHubRealTimeTradingLongPollingServiceImpl implements FinnHubRealTimeTradingService {
    private final Set<String> symbols = new ConcurrentSkipListSet<>();
    private final Lock runningLock = new ReentrantLock();
    private volatile boolean running = false;
    private static final int LONG_POLLING_INTERVAL_MILLIS = 5000;

    private final StockTransactionService stockTransactionService;
    private final StockSettingsService stockSettingsService;
    @Qualifier(Constants.BEAN_FINN_HUB_NAME)
    private final TradingService tradingService;
    private final TaskExecutor taskExecutor;

    @Override
    public void start() {
        if (running) {
            log.warn("Thread already is running state.");
            return;
        }
        taskExecutor.execute(() -> {
            try {
                runningLock.lock();
                running = true;
                while (running) {
                    symbols.forEach(this::handle);
                    Thread.sleep(LONG_POLLING_INTERVAL_MILLIS);
                }
            } catch (InterruptedException interruptedException) {
                log.error("Interrupted exception occurred.", interruptedException);
            } finally {
                runningLock.unlock();
            }
        });
    }

    @PreDestroy
    public void close() throws InterruptedException {
        running = false;
        runningLock.tryLock(LONG_POLLING_INTERVAL_MILLIS + 5000,
                TimeUnit.MILLISECONDS);
        runningLock.unlock();
    }

    @Override
    public void subscribe(String symbol) {
        log.info("Subscribe for {}", symbol);
        symbols.add(symbol);
        if (!running) {
            log.warn("User connection establishing(symbol: {})", symbol);
            start();
        }
    }

    @Override
    public void unsubscribe(String symbol) {
        log.info("Unsubscribe for {}", symbol);
        if (symbols.contains(symbol)) {
            symbols.remove(symbol);
        } else {
            log.warn("{} symbol is not found.", symbol);
        }
    }

    public void handle(String symbol) {
        StockResponseDTO stockResponseDto = tradingService.get(symbol);
        stockSettingsService.updatePrice(symbol, name(), stockResponseDto.getPrice());
        stockTransactionService.handleStockUpdate(stockResponseDto);
    }

    @Override
    public String name() {
        return Constants.REAL_TIME_FINN_HUB_NAME_HTTP_LONG_POLLING;
    }
}
