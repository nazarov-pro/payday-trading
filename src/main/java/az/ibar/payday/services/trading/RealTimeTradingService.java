package az.ibar.payday.services.trading;

import az.ibar.payday.container.BaseService;

public interface RealTimeTradingService extends BaseService {
    void subscribe(String symbol);
    void unsubscribe(String symbol);

    String name();
}
