package az.ibar.payday.services.trading;

import az.ibar.payday.container.BaseService;
import az.ibar.payday.container.dtos.StockResponseDTO;

public interface TradingService extends BaseService {
    StockResponseDTO get(String symbol);
}
