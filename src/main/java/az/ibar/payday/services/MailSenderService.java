package az.ibar.payday.services;

import az.ibar.payday.container.BaseService;
import az.ibar.payday.container.models.MailMessage;

import java.util.concurrent.Future;

public interface MailSenderService extends BaseService {
    Future<Boolean> send(MailMessage mailMessage);
}
