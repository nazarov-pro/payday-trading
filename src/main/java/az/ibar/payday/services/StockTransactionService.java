package az.ibar.payday.services;

import az.ibar.payday.container.BaseService;
import az.ibar.payday.container.dtos.StockOrderRequestDTO;
import az.ibar.payday.container.dtos.StockOrderResponseDTO;
import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.container.enums.StockTransactionStatusesEnum;

import java.math.BigDecimal;
import java.util.List;

public interface StockTransactionService extends BaseService {
    List<StockOrderResponseDTO> findAllByUserId(Long userId);

    List<StockResponseDTO> fetchAllStocksBySymbols(String symbols);

    StockOrderResponseDTO findOneByUserId(Long id, Long userId);

    StockOrderResponseDTO sell(StockOrderRequestDTO stockOrderRequestDto);

    StockOrderResponseDTO buy(StockOrderRequestDTO stockOrderRequestDto);

    void handleStockUpdate(StockResponseDTO stockResponseDto);

    void buyOperation(StockTransaction stockTransaction, StockResponseDTO stockResponseDto);

    void sellOperation(StockTransaction stockTransaction, StockResponseDTO stockResponseDto);

    boolean updateStatus(Long id, String version, StockTransactionStatusesEnum status, BigDecimal finalAmount);

    //INTERNAL

    StockTransaction save(StockTransaction stockTransaction);
}
