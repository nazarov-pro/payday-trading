package az.ibar.payday.repos;

import az.ibar.payday.container.BaseRepository;
import az.ibar.payday.container.entities.MailTransferHistory;
import org.springframework.stereotype.Repository;

@Repository
public interface MailTransferHistoryRepository extends BaseRepository<MailTransferHistory, Long> {

}
