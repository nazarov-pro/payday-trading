package az.ibar.payday.repos;

import az.ibar.payday.container.BaseRepository;
import az.ibar.payday.container.entities.BankAccount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface BankAccountRepository extends BaseRepository<BankAccount, Long> {
    @Query("from BankAccount ba where ba.user.id=:userId")
    List<BankAccount> findAllByUserId(@Param("userId") Long userId);

    @Query(value = "update \"p_bank_accounts\" set balance=:balance+balance, version=:newVersion " +
            "where version=:version and id=:id and (balance + :balance >= 0) RETURNING *", nativeQuery = true)
    BankAccount updateAndGet(
            @Param("id") Long id,
            @Param("balance") BigDecimal balance,
            @Param("newVersion") String newVersion,
            @Param("version") String version
    );

    @Query("select (count(ba) > 0) from BankAccount ba where ba.id=:bankAccountId and ba.user.id=:userId")
    Boolean existsByIdAndUserId(@Param("bankAccountId") Long bankAccountId, @Param("userId") Long userId);
}
