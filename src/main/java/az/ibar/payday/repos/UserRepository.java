package az.ibar.payday.repos;

import az.ibar.payday.container.BaseRepository;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.UserStatusesEnum;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<User, Long> {
    @Query(
            "from User a where (a.email=:email or a.username=:username) and not a.status=:status"
    )
    Boolean existsByUsernameOrEmail(
            @Param("username") String username,
            @Param("email") String email,
            @Param("status") UserStatusesEnum accountStatusExcluded
    );

    @Modifying
    @Query(
            "update User a set a.status=:status where a.id=:id"
    )
    Integer updateStatus(
            @Param("id") Long id,
            @Param("status") UserStatusesEnum accountStatus
    );

    @Query(
            "from User a where (a.email=:emailOrUsername or a.username=:emailOrUsername) " +
                    "and a.password=:password and a.status=:status"
    )
    User findOneByUsernameOrEmailAndPassword(
            @Param("emailOrUsername") String emailOrUsername,
            @Param("password") String password,
            @Param("status") UserStatusesEnum accountStatus
    );

    @Query(
            "from User a where (a.email=:emailOrUsername or a.username=:emailOrUsername) " +
                    " and a.status=:status"
    )
    User findOneByUsernameOrEmail(
            @Param("emailOrUsername") String emailOrUsername,
            @Param("status") UserStatusesEnum accountStatus
    );
}
