package az.ibar.payday.repos;

import az.ibar.payday.container.BaseRepository;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.container.enums.StockTransactionStatusesEnum;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

public interface StockTransactionRepository extends BaseRepository<StockTransaction, Long> {
    @Query(
            "from StockTransaction st join st.bankAccount ba where ba.user.id=:userId"
    )
    List<StockTransaction> findAllByUserId(
            @Param("userId") Long userId
    );

    @Transactional
    @Modifying
    @Query(
            "update StockTransaction set status=:status, version=:newVersion, finalAmount=:finalAmount where id=:id and version=:version"
    )
    Integer updateStatus(
            @Param("id") Long id,
            @Param("status") StockTransactionStatusesEnum status,
            @Param("version") String version,
            @Param("newVersion") String newVersion,
            @Param("finalAmount") BigDecimal finalAmount
    );

    @Query(
            "from StockTransaction st join st.bankAccount ba where ba.user.id=:userId and st.id=:id"
    )
    StockTransaction findOneByUserId(
            @Param("id") Long id,
            @Param("userId") Long userId
    );

    @Query(
            "from StockTransaction st where st.symbol=:symbol and st.status=:status " +
                    "and ((st.mode=:sellMode and st.targetAmount <= :price) " +
                    "or (st.mode=:buyMode and st.targetAmount >= :price))"
    )
    List<StockTransaction> findAllQualifiedTransactions(
            @Param("symbol") String symbol,
            @Param("status") StockTransactionStatusesEnum status,
            @Param("sellMode") Integer sellMode,
            @Param("buyMode") Integer buyMode,
            @Param("price") BigDecimal price
    );
}
