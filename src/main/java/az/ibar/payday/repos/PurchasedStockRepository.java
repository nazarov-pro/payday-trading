package az.ibar.payday.repos;

import az.ibar.payday.container.BaseRepository;
import az.ibar.payday.container.entities.PurchasedStock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PurchasedStockRepository extends BaseRepository<PurchasedStock, Long> {
    @Query(
            "from PurchasedStock  ps where ps.user.id=:userId"
    )
    List<PurchasedStock> findAllByUserId(
            @Param("userId") Long userId
    );

    @Query(
            "from PurchasedStock ps where ps.user.id=:userId and ps.symbol=:symbol"
    )
    PurchasedStock findOneByUserAndSymbol(
            @Param("userId") Long userId,
            @Param("symbol") String symbol
    );
}
