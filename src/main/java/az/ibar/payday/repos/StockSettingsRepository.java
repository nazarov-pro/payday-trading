package az.ibar.payday.repos;

import az.ibar.payday.container.BaseRepository;
import az.ibar.payday.container.entities.StockSettings;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface StockSettingsRepository extends BaseRepository<StockSettings, String> {
    @Query(value = "update \"p_stock_settings\" set locked=true, last_update_timestamp=:now," +
            " feed_provider=:feedProvider where locked=false or last_update_timestamp <= :expiredTmp RETURNING *", nativeQuery = true)
    List<StockSettings> updateAndGet(
            @Param("feedProvider") String feedProviderName,
            @Param("now") Long now,
            @Param("expiredTmp") Long expiredTimeStamp
    );

    @Query("from StockSettings ss where ss.feedProvider=:feedProvider " +
            "and ss.locked=:locked")
    List<StockSettings> findALlByFeedProviderId(
            @Param("feedProvider") String feedProviderName,
            @Param("locked") Boolean locked
    );


            @Modifying
    @Query("update StockSettings set locked=:lock where feedProvider=:feedProvider")
    Integer unlockAll(
            @Param("feedProvider") String feedProvider,
            @Param("lock") Boolean lock
    );

    @Modifying
    @Query("update StockSettings set locked=:lock where id=:id and feedProvider=:feedProvider")
    Integer unlockOne(
            @Param("id") String id,
            @Param("feedProvider") String feedProvider,
            @Param("lock") Boolean lock
    );

    @Modifying
    @Query("update StockSettings set lastPrice=:lastPrice, lastUpdateTimestamp=:now" +
            " where feedProvider=:feedProvider and id=:id")
    Integer updatePrice(
            @Param("lastPrice") BigDecimal lastPrice,
            @Param("now") Long now,
            @Param("id") String id,
            @Param("feedProvider") String feedProviderName
    );

    @Modifying
    @Query("update StockSettings set lastUpdateTimestamp=:now" +
            " where feedProvider=:feedProvider")
    Integer updateAllSubscribersLastTmp(
            @Param("now") Long now,
            @Param("feedProvider") String feedProviderName
    );
}
