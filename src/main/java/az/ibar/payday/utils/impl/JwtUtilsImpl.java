package az.ibar.payday.utils.impl;

import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.utils.JwtUtils;
import az.ibar.payday.utils.TimeUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class JwtUtilsImpl implements JwtUtils {
    @Value("${app.security.jwt.secret}")
    private final String secret;
    @Value("${app.security.jwt.expiration-seconds}")
    private final Long expirationSeconds;
    private final TimeUtils timeUtils;

    private Key key;

    @PostConstruct
    @Override
    public void init() {
        this.key = Keys.hmacShaKeyFor(secret.getBytes());
    }

    @Override
    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parserBuilder().setSigningKey(key)
                .build().parseClaimsJws(token).getBody();
    }

    @Override
    public String getUsernameFromToken(String token) {
        return getAllClaimsFromToken(token).getSubject();
    }

    @Override
    public Long getUserIdFromToken(String token) {
        return getAllClaimsFromToken(token).get("userId", Long.class);
    }

    @Override
    public Date getExpirationDateFromToken(String token) {
        return getAllClaimsFromToken(token).getExpiration();
    }

    @Override
    public boolean isTokenExpired(String token) {
        final Long nowEpoch = timeUtils.getEpochMillis();
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date(nowEpoch));
    }

    @Override
    public String generateToken(AuthUserDetails authUserDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("role", Collections.singletonList(authUserDetails.getRole()));
        claims.put("userId", authUserDetails.getId());
        return doGenerateToken(claims, authUserDetails.getUsername());
    }

    @Override
    public String doGenerateToken(Map<String, Object> claims, String username) {
        final Long nowEpoch = timeUtils.getEpochMillis();
        final Date createdDate = new Date(nowEpoch);
        final Date expirationDate = new Date(nowEpoch + expirationSeconds * 1000);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(key)
                .compact();

    }

    @Override
    public boolean validateToken(String token) {
        return !isTokenExpired(token);
    }
}
