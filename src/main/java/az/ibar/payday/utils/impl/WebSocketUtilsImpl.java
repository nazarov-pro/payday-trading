package az.ibar.payday.utils.impl;

import az.ibar.payday.utils.WebSocketUtils;
import org.springframework.stereotype.Component;

import javax.websocket.ContainerProvider;
import javax.websocket.WebSocketContainer;

@Component
public class WebSocketUtilsImpl implements WebSocketUtils {

    @Override
    public WebSocketContainer getContainer() {
        return ContainerProvider.getWebSocketContainer();
    }

}
