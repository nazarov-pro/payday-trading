package az.ibar.payday.utils;

import javax.websocket.WebSocketContainer;

public interface WebSocketUtils {
    WebSocketContainer getContainer();
}
