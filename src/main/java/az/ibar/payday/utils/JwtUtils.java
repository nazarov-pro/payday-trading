package az.ibar.payday.utils;

import az.ibar.payday.container.models.AuthUserDetails;
import io.jsonwebtoken.Claims;

import java.util.Date;
import java.util.Map;

public interface JwtUtils {
    void init();

    Claims getAllClaimsFromToken(String token);

    String getUsernameFromToken(String token);

    Long getUserIdFromToken(String token);

    Date getExpirationDateFromToken(String token);

    boolean isTokenExpired(String token);

    String generateToken(AuthUserDetails authUserDetails);

    String doGenerateToken(Map<String, Object> claims, String username);

    boolean validateToken(String token);
}
