package az.ibar.payday.utils.generators;

import az.ibar.payday.container.models.ApiCollectionResponse;
import az.ibar.payday.container.models.ApiMessage;
import az.ibar.payday.container.models.ApiMetadata;
import az.ibar.payday.container.models.ApiResponse;

import java.util.Collection;

public interface ApiResponseGenerator {
    <T> ApiResponse<T> generate(T item);
    <T> ApiCollectionResponse<T> generateCollection(Collection<T> items);
    ApiMetadata generateMetadata();
    ApiMessage generateMessage(ApiMetadata metadata);
}
