package az.ibar.payday.utils.generators.impl;

import az.ibar.payday.utils.generators.UUIDGenerator;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UUIDGeneratorImpl implements UUIDGenerator {
    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
