package az.ibar.payday.utils.generators.impl;

import az.ibar.payday.container.models.ApiCollectionResponse;
import az.ibar.payday.container.models.ApiMessage;
import az.ibar.payday.container.models.ApiMetadata;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.utils.TimeUtils;
import az.ibar.payday.utils.generators.ApiResponseGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@RequiredArgsConstructor
public class ApiResponseGeneratorImpl implements ApiResponseGenerator {
    private final TimeUtils timeUtils;

    @Override
    public <T> ApiResponse<T> generate(T item) {
        ApiResponse<T> response = new ApiResponse<>();
        response.setItem(item);
        response.setMetadata(generateMetadata());
        response.setSuccess(true);
        return response;
    }

    @Override
    public <T> ApiCollectionResponse<T> generateCollection(Collection<T> items) {
        ApiCollectionResponse<T> collectionResponse = new ApiCollectionResponse<T>();
        collectionResponse.setItems(items);
        collectionResponse.setMetadata(generateMetadata());
        collectionResponse.setSuccess(true);
        return collectionResponse;
    }

    @Override
    public ApiMetadata generateMetadata() {
        ApiMetadata metadata = new ApiMetadata();
        metadata.setTimestamp(timeUtils.getEpochMillis());
        return metadata;
    }

    @Override
    public ApiMessage generateMessage(ApiMetadata metadata) {
        ApiMessage apiMessage = new ApiMessage();
        apiMessage.setMetadata(metadata);
        return apiMessage;
    }
}
