package az.ibar.payday.utils.generators;

public interface UUIDGenerator {
    String generate();
}
