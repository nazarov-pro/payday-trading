package az.ibar.payday.utils;

public interface TimeUtils {
    Long getEpochMillis();
    Long getEpochSeconds();
}
