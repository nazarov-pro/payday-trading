package az.ibar.payday.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

@Primary
@Component
@RequiredArgsConstructor
public class PBKDF2Encoder implements PasswordEncoder {

    @Value("${app.security.pass-encoder.secret}")
    private final String secret;
    @Value("${app.security.pass-encoder.iteration}")
    private final Integer iteration;
    @Value("${app.security.pass-encoder.key-length}")
    private final Integer keyLength;

    /**
     * More info (https://www.owasp.org/index.php/Hashing_Java)
     * @param rawPassword password
     * @return encoded password
     */
    @Override
    public String encode(CharSequence rawPassword) {
        try {
            byte[] result = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
                    .generateSecret(new PBEKeySpec(rawPassword.toString().toCharArray(), secret.getBytes(),
                            iteration, keyLength)).getEncoded();
            return Base64.getEncoder().encodeToString(result);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encode(rawPassword).equals(encodedPassword);
    }
}
