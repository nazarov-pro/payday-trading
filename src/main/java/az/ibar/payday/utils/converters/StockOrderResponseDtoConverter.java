package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;
import az.ibar.payday.container.dtos.StockOrderResponseDTO;
import az.ibar.payday.container.entities.StockTransaction;

public interface StockOrderResponseDtoConverter extends BaseConverter<StockTransaction, StockOrderResponseDTO> {
}
