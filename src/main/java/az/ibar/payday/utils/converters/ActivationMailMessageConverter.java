package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.models.MailMessage;

public interface ActivationMailMessageConverter extends BaseConverter<User, MailMessage> {
}
