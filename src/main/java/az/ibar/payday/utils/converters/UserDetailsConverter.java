package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.models.AuthUserDetails;

public interface UserDetailsConverter extends BaseConverter<User, AuthUserDetails> {

}
