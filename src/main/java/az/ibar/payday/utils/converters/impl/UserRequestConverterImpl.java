package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.container.dtos.UserRequestDTO;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.utils.converters.UserRequestConverter;
import org.springframework.stereotype.Component;

@Component
public class UserRequestConverterImpl implements UserRequestConverter {
    @Override
    public User convert(UserRequestDTO item) {
        User user = new User();
        user.setId(item.getId());
        user.setUsername(item.getUsername());
        user.setPassword(item.getPassword());
        user.setEmail(item.getEmail());
        return user;
    }

    @Override
    public boolean validate(UserRequestDTO item) {
        return item != null;
    }
}
