package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.utils.converters.QueryParamExtractor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class QueryParamExtractorImpl implements QueryParamExtractor {
    @Override
    public Map<String, String> convert(String text) {
        Map<String, String> queryParams = new HashMap<>();
        int point = 0;
        String lastKey = "";
        for (int i = 1; i < text.length(); i++) {
            if (text.charAt(i) == '=') {
                lastKey = text.substring(point, i);
                queryParams.put(lastKey, "");
                point = i + 1;
            } else if (text.charAt(i) == '&') {
                queryParams.put(lastKey, text.substring(point, i));
                point = i + 1;
                lastKey = "";
            }
        }
        int lastElementEndIndex = text.indexOf(" ", point);
        String lastElementVal = text.substring(
                point,
                lastElementEndIndex == -1 ? text.length() : lastElementEndIndex
        );
        if (lastKey.isEmpty()) {
            queryParams.put(lastElementVal, "");
        } else {
            queryParams.put(lastKey, lastElementVal);
        }
        return queryParams;
    }

    @Override
    public boolean validate(String item) {
        return item != null && !item.isEmpty();
    }

    @Override
    public Map<String, String> getZeroVal() {
        return Collections.emptyMap();
    }
}
