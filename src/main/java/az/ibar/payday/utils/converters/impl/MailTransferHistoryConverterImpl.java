package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.container.entities.MailTransferHistory;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.utils.converters.MailTransferHistoryConverter;
import org.springframework.stereotype.Component;

@Component
public class MailTransferHistoryConverterImpl implements MailTransferHistoryConverter {
    @Override
    public MailTransferHistory convert(MailMessage item) {
        MailTransferHistory mailTransferHistory = new MailTransferHistory();
        mailTransferHistory.setContent(item.getContent());
        mailTransferHistory.setRecipient(item.getRecipient());
        return mailTransferHistory;
    }

    @Override
    public boolean validate(MailMessage item) {
        return item != null;
    }
}
