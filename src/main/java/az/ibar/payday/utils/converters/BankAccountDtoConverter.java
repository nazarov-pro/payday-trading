package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;
import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.entities.BankAccount;

public interface BankAccountDtoConverter extends BaseConverter<BankAccount, BankAccountResponseDTO> {
}
