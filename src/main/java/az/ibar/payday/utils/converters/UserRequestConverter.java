package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;
import az.ibar.payday.container.dtos.UserRequestDTO;
import az.ibar.payday.container.entities.User;

public interface UserRequestConverter extends BaseConverter<UserRequestDTO, User> {

}
