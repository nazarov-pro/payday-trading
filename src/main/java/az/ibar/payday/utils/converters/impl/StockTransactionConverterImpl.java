package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.container.dtos.StockOrderRequestDTO;
import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.utils.converters.StockTransactionConverter;
import org.springframework.stereotype.Component;

@Component
public class StockTransactionConverterImpl implements StockTransactionConverter {
    @Override
    public StockTransaction convert(StockOrderRequestDTO item) {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(item.getBankAccountId());
        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setId(item.getId());
        stockTransaction.setMode(item.getMode());
        stockTransaction.setSymbol(item.getSymbol());
        stockTransaction.setSymbolUnit(item.getSymbolUnit());
        stockTransaction.setTargetAmount(item.getTargetAmount());
        return stockTransaction;
    }

    @Override
    public boolean validate(StockOrderRequestDTO item) {
        return item != null;
    }
}
