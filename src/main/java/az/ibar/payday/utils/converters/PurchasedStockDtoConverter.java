package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;
import az.ibar.payday.container.dtos.PurchasedStockResponseDTO;
import az.ibar.payday.container.entities.PurchasedStock;

public interface PurchasedStockDtoConverter extends BaseConverter<PurchasedStock, PurchasedStockResponseDTO> {
}
