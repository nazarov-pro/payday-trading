package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.container.dtos.PurchasedStockResponseDTO;
import az.ibar.payday.container.entities.PurchasedStock;
import az.ibar.payday.utils.converters.PurchasedStockDtoConverter;
import org.springframework.stereotype.Component;

@Component
public class PurchasedStockDtoConverterImpl implements PurchasedStockDtoConverter {

    @Override
    public PurchasedStockResponseDTO convert(PurchasedStock item) {
        PurchasedStockResponseDTO purchasedStockResponseDto = new PurchasedStockResponseDTO();
        purchasedStockResponseDto.setId(item.getId());
        purchasedStockResponseDto.setCreatedTimestamp(item.getCreatedTimestamp());
        purchasedStockResponseDto.setSymbol(item.getSymbol());
        purchasedStockResponseDto.setSymbolUnit(item.getSymbolUnit());
        purchasedStockResponseDto.setUpdatedTimestamp(item.getUpdatedTimeStamp());
        purchasedStockResponseDto.setVersion(item.getVersion());
        return purchasedStockResponseDto;
    }

    @Override
    public boolean validate(PurchasedStock item) {
        return item != null;
    }
}
