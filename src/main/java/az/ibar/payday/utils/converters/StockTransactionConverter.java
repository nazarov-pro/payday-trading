package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;
import az.ibar.payday.container.dtos.StockOrderRequestDTO;
import az.ibar.payday.container.entities.StockTransaction;

public interface StockTransactionConverter extends BaseConverter<StockOrderRequestDTO, StockTransaction> {
}
