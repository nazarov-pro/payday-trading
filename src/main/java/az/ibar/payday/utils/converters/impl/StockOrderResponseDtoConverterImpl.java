package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.container.dtos.StockOrderResponseDTO;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.container.enums.StockTransactionModesEnum;
import az.ibar.payday.utils.converters.StockOrderResponseDtoConverter;
import org.springframework.stereotype.Component;

@Component
public class StockOrderResponseDtoConverterImpl implements StockOrderResponseDtoConverter {
    @Override
    public StockOrderResponseDTO convert(StockTransaction item) {
        StockOrderResponseDTO stockOrderResponseDto = new StockOrderResponseDTO();
        if(item.getBankAccount() != null) {
            stockOrderResponseDto.setBankAccountId(item.getBankAccount().getId());
        }
        stockOrderResponseDto.setId(item.getId());
        stockOrderResponseDto.setMode(StockTransactionModesEnum.from(item.getMode()));
        stockOrderResponseDto.setSymbol(item.getSymbol());
        stockOrderResponseDto.setSymbolUnit(item.getSymbolUnit());
        stockOrderResponseDto.setTargetAmount(item.getTargetAmount());
        stockOrderResponseDto.setFinalAmount(item.getFinalAmount());
        stockOrderResponseDto.setCreatedTimestamp(item.getCreatedTimestamp());
        stockOrderResponseDto.setStartAmount(item.getStartAmount());
        stockOrderResponseDto.setStatus(item.getStatus());
        stockOrderResponseDto.setUpdatedTimestamp(item.getUpdatedTimestamp());
        stockOrderResponseDto.setVersion(item.getVersion());
        return stockOrderResponseDto;
    }

    @Override
    public boolean validate(StockTransaction item) {
        return item != null;
    }
}
