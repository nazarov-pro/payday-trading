package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.utils.converters.BankAccountDtoConverter;
import org.springframework.stereotype.Component;

@Component
public class BankAccountDtoConverterImpl implements BankAccountDtoConverter {

    @Override
    public BankAccountResponseDTO convert(BankAccount item) {
        BankAccountResponseDTO bankAccountResponseDto = new BankAccountResponseDTO();
        bankAccountResponseDto.setBalance(item.getBalance());
        bankAccountResponseDto.setId(item.getId());
        bankAccountResponseDto.setVersion(item.getVersion());
        return bankAccountResponseDto;
    }

    @Override
    public boolean validate(BankAccount item) {
        return item != null;
    }
}
