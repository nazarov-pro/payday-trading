package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;

import java.util.Map;

public interface QueryParamExtractor extends BaseConverter<String, Map<String, String>> {
}
