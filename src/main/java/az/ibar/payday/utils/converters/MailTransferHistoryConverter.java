package az.ibar.payday.utils.converters;

import az.ibar.payday.container.BaseConverter;
import az.ibar.payday.container.entities.MailTransferHistory;
import az.ibar.payday.container.models.MailMessage;

public interface MailTransferHistoryConverter extends BaseConverter<MailMessage, MailTransferHistory> {
}
