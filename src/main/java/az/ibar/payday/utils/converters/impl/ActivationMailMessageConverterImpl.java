package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.utils.converters.ActivationMailMessageConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ActivationMailMessageConverterImpl implements ActivationMailMessageConverter {
    @Value("${app.publicEndPoint:'http://localhost:8080'}")
    private final String publicEndPoint;

    @Override
    public MailMessage convert(User item) {
        MailMessage mailMessage = new MailMessage();
        mailMessage.setRecipient(item.getEmail());
        mailMessage.setSubject("Confirmation");
        mailMessage.setContent(
                String.format(
                        "Dear %s, for activation your account please click this url( " +
                                "%s/user/activate?id=%d&email=%s&activationToken=%s )",
                        item.getUsername(),
                        publicEndPoint,
                        item.getId(),
                        item.getEmail(),
                        item.getActivationToken()
                )
        );
        return mailMessage;
    }

    @Override
    public boolean validate(User item) {
        return item != null;
    }
}
