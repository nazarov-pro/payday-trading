package az.ibar.payday.utils.converters.impl;

import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.UserStatusesEnum;
import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.utils.converters.UserDetailsConverter;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsConverterImpl implements UserDetailsConverter {

    @Override
    public AuthUserDetails convert(User item) {
        AuthUserDetails authUserDetails = new AuthUserDetails();
        authUserDetails.setEnabled(item.getStatus().equals(UserStatusesEnum.ACTIVE));
        authUserDetails.setUsername(item.getUsername());
        authUserDetails.setPassword(item.getPassword());
        authUserDetails.setId(item.getId());
        authUserDetails.setRole("USER");
        return authUserDetails;
    }

    @Override
    public boolean validate(User item) {
        return item != null;
    }
}
