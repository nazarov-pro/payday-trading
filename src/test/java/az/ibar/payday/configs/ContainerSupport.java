package az.ibar.payday.configs;

import az.ibar.payday.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public class ContainerSupport extends BaseIntegrationTest {
    public static PostgreSQLContainer postgresqlContainer = new PostgreSQLContainer("postgres:12")
            .withDatabaseName("pg_test_db").withUsername("username").withPassword("secret");

    @DynamicPropertySource
    static void postgresqlProperties(DynamicPropertyRegistry registry) {
        postgresqlContainer.start();
        registry.add("spring.datasource.url", postgresqlContainer::getJdbcUrl);
        registry.add("spring.datasource.password", postgresqlContainer::getPassword);
        registry.add("spring.datasource.username", postgresqlContainer::getUsername);
    }

    @Test
    public void contextLoads() {
        Assert.assertTrue(postgresqlContainer.isRunning());
    }
}
