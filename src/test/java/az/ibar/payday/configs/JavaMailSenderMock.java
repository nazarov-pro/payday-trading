package az.ibar.payday.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.InputStream;

@Primary
@Component
public class JavaMailSenderMock implements JavaMailSender {
    private final Logger log = LoggerFactory.getLogger(JavaMailSenderMock.class);

    @Override
    public MimeMessage createMimeMessage() {
        return null;
    }

    @Override
    public MimeMessage createMimeMessage(InputStream contentStream) throws MailException {
        return null;
    }

    @Override
    public void send(MimeMessage mimeMessage) throws MailException {

    }

    @Override
    public void send(MimeMessage... mimeMessages) throws MailException {

    }

    @Override
    public void send(MimeMessagePreparator mimeMessagePreparator) throws MailException {

    }

    @Override
    public void send(MimeMessagePreparator... mimeMessagePreparators) throws MailException {

    }

    @Override
    public void send(SimpleMailMessage simpleMessage) throws MailException {
        log.info(
                "MOCK sending message to {} from {} with subject {} and content {}",
                simpleMessage.getTo(),
                simpleMessage.getFrom(),
                simpleMessage.getSubject(),
                simpleMessage.getText()
        );
    }

    @Override
    public void send(SimpleMailMessage... simpleMessages) throws MailException {

    }
}
