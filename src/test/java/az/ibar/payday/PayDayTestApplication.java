package az.ibar.payday;

import az.ibar.payday.configs.AsyncSupportConfiguration;
import az.ibar.payday.configs.InitialDataConfiguration;
import az.ibar.payday.configs.SchedulingConfiguration;
import az.ibar.payday.schedulers.StockManagementScheduler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * Exclude schedulers, exclude JPA
 */

@SpringBootApplication
@ComponentScan(
        excludeFilters = {
                @ComponentScan.Filter(
                        type = FilterType.ASSIGNABLE_TYPE,
                        classes = {
                                PaydayApplication.class,
                                SchedulingConfiguration.class,
                                StockManagementScheduler.class,
                                AsyncSupportConfiguration.class,
                                InitialDataConfiguration.class
                        }
                )
        }
)
public class PayDayTestApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(PayDayTestApplication.class);
        springApplication.run(args);
    }


}
