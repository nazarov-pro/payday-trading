package az.ibar.payday.services;

import az.ibar.payday.container.entities.MailTransferHistory;
import az.ibar.payday.repos.MailTransferHistoryRepository;
import az.ibar.payday.services.impl.MailTransferHistoryServiceImpl;
import az.ibar.payday.services.impl.UserServiceImpl;
import az.ibar.payday.utils.converters.impl.ActivationMailMessageConverterImpl;
import az.ibar.payday.utils.converters.impl.UserDetailsConverterImpl;
import az.ibar.payday.utils.converters.impl.UserRequestConverterImpl;
import az.ibar.payday.utils.generators.impl.UUIDGeneratorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MailTransferHistoryServiceTest {
    private MailTransferHistoryService mailTransferHistoryService;
    private MailTransferHistoryRepository mailTransferHistoryRepository;

    @Before
    public void setUp() {
        mailTransferHistoryRepository = Mockito.mock(MailTransferHistoryRepository.class);
        mailTransferHistoryService = new MailTransferHistoryServiceImpl(mailTransferHistoryRepository);
    }

    @Test
    public void testSaveHistory() {
        MailTransferHistory mailTransferHistory = new MailTransferHistory();
        mailTransferHistory.setId(1L);
        mailTransferHistoryService.save(mailTransferHistory);
        Mockito.verify(mailTransferHistoryRepository, Mockito.times(1))
                .save(Mockito.eq(mailTransferHistory));
    }

}
