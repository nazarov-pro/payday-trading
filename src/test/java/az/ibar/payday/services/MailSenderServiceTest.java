package az.ibar.payday.services;

import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.services.impl.MailSenderServiceImpl;
import az.ibar.payday.utils.TimeUtils;
import az.ibar.payday.utils.converters.impl.MailTransferHistoryConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class MailSenderServiceTest {
    private MailSenderService mailSenderService;
    private JavaMailSender javaMailSender;
    private MailTransferHistoryService mailTransferHistoryService;
    private TimeUtils timeUtils;

    @Before
    public void setUp() {
        javaMailSender = Mockito.mock(JavaMailSender.class);
        mailTransferHistoryService = Mockito.mock(MailTransferHistoryService.class);
        timeUtils = Mockito.mock(TimeUtils.class);
        mailSenderService = new MailSenderServiceImpl(
                javaMailSender,
                mailTransferHistoryService,
                new MailTransferHistoryConverterImpl(),
                timeUtils
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void sendSendNullObject() {
        mailSenderService.send(null);
    }

    @Test
    public void sendSendToWrongEmail() throws ExecutionException, InterruptedException {
        MailMessage mailMessage = new MailMessage();
        mailMessage.setRecipient("123123@dassd.com");
        Mockito.doAnswer(
                invocation -> {
                    throw new MailException("Sending failed") {
                        @Override
                        public boolean contains(Class<?> exType) {
                            return super.contains(exType);
                        }
                    };
                }
        ).when(javaMailSender).send(Mockito.any(SimpleMailMessage.class));
        Boolean sendingResult = mailSenderService.send(mailMessage).get();
        Assert.assertFalse(sendingResult);
    }

    @Test
    public void sendSend() throws ExecutionException, InterruptedException {
        MailMessage mailMessage = new MailMessage();
        mailMessage.setRecipient("email@email.com");
        boolean sendingResult = Optional.ofNullable(mailSenderService.send(mailMessage).get())
                .orElse(false);
        Assert.assertTrue(sendingResult);
        Mockito.verify(javaMailSender, Mockito.times(1))
                .send(Mockito.any(SimpleMailMessage.class));
    }


}
