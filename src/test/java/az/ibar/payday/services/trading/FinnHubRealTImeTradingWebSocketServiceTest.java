package az.ibar.payday.services.trading;

import az.ibar.payday.configs.ApplicationConfiguration;
import az.ibar.payday.container.dtos.FinnHubRealtimeTradingDataDTO;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.services.StockTransactionService;
import az.ibar.payday.services.trading.impl.FinnHubRealTimeTradingWebSocketServiceImpl;
import az.ibar.payday.utils.WebSocketUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;

// TODO: Add tests for rest of the methods (and integration test)
public class FinnHubRealTImeTradingWebSocketServiceTest {
    private FinnHubRealTimeTradingWebSocketServiceImpl finnHubRealTimeTradingWebSocketService;
    private StockTransactionService stockTransactionService;
    private StockSettingsService stockSettingsService;
    private WebSocketUtils webSocketUtils;
    private Session session;
    private WebSocketContainer webSocketContainer;


    private ObjectMapper objectMapper = new ApplicationConfiguration().defaultObjectMapper();
    private final String symbol = "AAPL";
    private final String token = "1234";

    @Before
    public void setUp() {
        stockTransactionService = Mockito.mock(StockTransactionService.class);
        stockSettingsService = Mockito.mock(StockSettingsService.class);
        webSocketUtils = Mockito.mock(WebSocketUtils.class);
        session = Mockito.mock(Session.class);
        webSocketContainer = Mockito.mock(WebSocketContainer.class);
        Mockito.when(webSocketUtils.getContainer()).thenReturn(webSocketContainer);
        finnHubRealTimeTradingWebSocketService = new FinnHubRealTimeTradingWebSocketServiceImpl(
                token, objectMapper, stockTransactionService, stockSettingsService, webSocketUtils
        );
    }

    @Test
    public void testStarting() throws IOException, DeploymentException {
        Mockito.when(
                webSocketContainer.connectToServer(
                        Mockito.eq(finnHubRealTimeTradingWebSocketService),
                        Mockito.any(URI.class)
                )
        ).thenReturn(session);
        finnHubRealTimeTradingWebSocketService.start();
        Mockito.verify(webSocketUtils, Mockito.times(1))
                .getContainer();
    }

    @Test(expected = RuntimeException.class)
    public void testStartingWithError() throws IOException, DeploymentException {
        Mockito.when(
                webSocketContainer.connectToServer(
                        Mockito.eq(finnHubRealTimeTradingWebSocketService),
                        Mockito.any(URI.class)
                )
        ).thenThrow(new IOException("Broken connection"));
        finnHubRealTimeTradingWebSocketService.start();
    }

    @Test
    public void testHandlingData() {
        FinnHubRealtimeTradingDataDTO finnHubRealtimeTradingDataDTO = new FinnHubRealtimeTradingDataDTO();
        finnHubRealtimeTradingDataDTO.setLastPrice(BigDecimal.ONE);
        finnHubRealtimeTradingDataDTO.setSymbol(symbol);

        finnHubRealTimeTradingWebSocketService.handle(finnHubRealtimeTradingDataDTO);

        Mockito.verify(stockSettingsService, Mockito.times(1)).updatePrice(
                Mockito.eq(finnHubRealtimeTradingDataDTO.getSymbol()),
                Mockito.eq(finnHubRealTimeTradingWebSocketService.name()),
                Mockito.eq(finnHubRealtimeTradingDataDTO.getLastPrice())
        );

        Mockito.verify(stockTransactionService, Mockito.times(1))
                .handleStockUpdate(Mockito.any());
    }


}
