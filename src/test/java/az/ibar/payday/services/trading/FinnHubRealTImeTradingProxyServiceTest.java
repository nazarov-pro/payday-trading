package az.ibar.payday.services.trading;

import az.ibar.payday.services.trading.impl.FinnHubRealTimeTradingProxyServiceImpl;
import az.ibar.payday.utils.generators.impl.UUIDGeneratorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class FinnHubRealTImeTradingProxyServiceTest {
    private FinnHubRealTimeTradingProxyServiceImpl finnHubTradingService;
    private FinnHubRealTimeTradingService finnHubRealTimeHttpTradingService;
    private FinnHubRealTimeTradingService finnHubRealTimeWebSocketTradingService;

    private final String podName = "POD-01";
    private final String symbol = "AAPL";

    @Before
    public void setUp() {
        finnHubRealTimeHttpTradingService = Mockito.mock(FinnHubRealTimeTradingService.class);
        finnHubRealTimeWebSocketTradingService = Mockito.mock(FinnHubRealTimeTradingService.class);
        finnHubTradingService = new FinnHubRealTimeTradingProxyServiceImpl(
                podName, finnHubRealTimeHttpTradingService,
                finnHubRealTimeWebSocketTradingService, new UUIDGeneratorImpl()
        );
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testStartingProxy() {
        finnHubTradingService.start();
    }

    @Test
    public void testWithWebSocket() {
        Mockito.doNothing().when(finnHubRealTimeWebSocketTradingService).start();
        Mockito.when(finnHubRealTimeWebSocketTradingService.name()).thenReturn("");

        finnHubTradingService.init();
        Mockito.verify(finnHubRealTimeWebSocketTradingService,
                Mockito.times(1)).start();

        finnHubTradingService.name();
        Mockito.verify(finnHubRealTimeWebSocketTradingService,
                Mockito.times(2)).name();
        Assert.assertEquals(finnHubTradingService.name(), ":".concat(podName));

        finnHubTradingService.subscribe(symbol);
        Mockito.verify(finnHubRealTimeWebSocketTradingService,
                Mockito.times(1)).subscribe(Mockito.eq(symbol));


        finnHubTradingService.unsubscribe(symbol);
        Mockito.verify(finnHubRealTimeWebSocketTradingService,
                Mockito.times(1)).unsubscribe(Mockito.eq(symbol));
    }

    @Test
    public void testWithHttpPolling() {
        Mockito.doThrow(new RuntimeException("Socket closed."))
                .when(finnHubRealTimeWebSocketTradingService).start();
        Mockito.when(finnHubRealTimeHttpTradingService.name()).thenReturn("");

        finnHubTradingService.init();

        Mockito.verify(finnHubRealTimeWebSocketTradingService,
                Mockito.times(1)).start();
        Mockito.verify(finnHubRealTimeHttpTradingService,
                Mockito.times(1)).start();

        finnHubTradingService.name();
        Mockito.verify(finnHubRealTimeHttpTradingService,
                Mockito.times(3)).name();
        Assert.assertEquals(finnHubTradingService.name(), ":".concat(podName));

        finnHubTradingService.subscribe(symbol);
        Mockito.verify(finnHubRealTimeHttpTradingService,
                Mockito.times(1)).subscribe(Mockito.eq(symbol));


        finnHubTradingService.unsubscribe(symbol);
        Mockito.verify(finnHubRealTimeHttpTradingService,
                Mockito.times(1)).unsubscribe(Mockito.eq(symbol));
    }
}
