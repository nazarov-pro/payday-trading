package az.ibar.payday.services.trading;

import az.ibar.payday.BaseIntegrationTest;
import az.ibar.payday.configs.ContainerSupport;
import az.ibar.payday.container.dtos.FinnhubStockQuoteDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FinnHubTradingServiceIntegrationTest extends ContainerSupport {
    @Value("${app.integration.finnhub.token}")
    private String token;
    @Autowired
    private FinnHubTradingV1Client finnHubTradingV1Client;

    @Test
    void givenSymbolWhenFetchQuotaThenRemoteCall() {
        String symbol = "AAPL";
        FinnhubStockQuoteDTO finnhubStockQuoteDTO = finnHubTradingV1Client.fetchQuota(token, symbol);
        Assert.assertNotNull(finnhubStockQuoteDTO);
        Assert.assertTrue(finnhubStockQuoteDTO.getTimestamp() > 0);
    }

}
