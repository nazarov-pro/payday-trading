package az.ibar.payday.services.trading;

import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.services.StockTransactionService;
import az.ibar.payday.services.trading.impl.FinnHubRealTimeTradingLongPollingServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.core.task.TaskExecutor;

import java.math.BigDecimal;

public class FinnHubRealTImeTradingHttpPollingServiceTest {
    private FinnHubRealTimeTradingLongPollingServiceImpl finnHubTradingRealTimeService;
    private TradingService tradingService;
    private StockTransactionService stockTransactionService;
    private StockSettingsService stockSettingsService;
    private TaskExecutor taskExecutor;

    private final String symbol = "AAPL";

    @Before
    public void setUp() {
        tradingService = Mockito.mock(TradingService.class);
        stockTransactionService = Mockito.mock(StockTransactionService.class);
        stockSettingsService = Mockito.mock(StockSettingsService.class);
        taskExecutor = Mockito.mock(TaskExecutor.class);
        finnHubTradingRealTimeService = new FinnHubRealTimeTradingLongPollingServiceImpl(
                stockTransactionService, stockSettingsService, tradingService, taskExecutor
        );
    }

    @Test
    public void testStartingMoreThanOne() {
        finnHubTradingRealTimeService.start();
        finnHubTradingRealTimeService.start();
        Mockito.verify(taskExecutor, Mockito.times(2))
                .execute(Mockito.any());
    }

    @Test
    public void testStartCloseThenAgainStart() throws InterruptedException {
        finnHubTradingRealTimeService.start();
        finnHubTradingRealTimeService.close();
        finnHubTradingRealTimeService.start();
        Mockito.verify(taskExecutor, Mockito.times(2))
                .execute(Mockito.any());
    }

    @Test
    public void testSubscribeWhenRunning() {
        finnHubTradingRealTimeService.start();
        finnHubTradingRealTimeService.subscribe(symbol);
        Mockito.verify(taskExecutor, Mockito.times(2))
                .execute(Mockito.any());
    }

    @Test
    public void testUnSubscribeWhenRunning() {
        finnHubTradingRealTimeService.start();
        finnHubTradingRealTimeService.unsubscribe(symbol);
        Mockito.verify(taskExecutor, Mockito.times(1))
                .execute(Mockito.any());
    }

    @Test
    public void testHandlingData() {
        StockResponseDTO stockResponseDTO = new StockResponseDTO();
        stockResponseDTO.setPrice(BigDecimal.ONE);
        stockResponseDTO.setSymbol(symbol);

        Mockito.when(tradingService.get(Mockito.eq(symbol)))
                .thenReturn(stockResponseDTO);

        finnHubTradingRealTimeService.handle(
                symbol
        );
        Mockito.verify(stockSettingsService, Mockito.times(1)).updatePrice(
                Mockito.eq(symbol),
                Mockito.eq(finnHubTradingRealTimeService.name()),
                Mockito.eq(stockResponseDTO.getPrice())
        );

        Mockito.verify(stockTransactionService, Mockito.times(1))
                .handleStockUpdate(Mockito.eq(stockResponseDTO));
    }

}
