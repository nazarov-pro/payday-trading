package az.ibar.payday.services.trading;

import az.ibar.payday.container.dtos.FinnhubStockQuoteDTO;
import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.services.trading.impl.FinnHubTradingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class FinnHubTradingServiceTest {
    private FinnHubTradingService finnHubTradingService;
    private FinnHubTradingV1Client finnHubTradingV1Client;

    private final String symbol = "AAPL";
    private final BigDecimal price = BigDecimal.TEN;

    @Before
    public void setUp() {
        finnHubTradingV1Client = Mockito.mock(FinnHubTradingV1Client.class);
        finnHubTradingService = new FinnHubTradingServiceImpl(
                "securedToken", finnHubTradingV1Client
        );
        Mockito.when(finnHubTradingV1Client.fetchQuota(Mockito.any(), Mockito.any())).thenAnswer(
                invocation -> {
                    String symbol = invocation.getArgument(1);
                    FinnhubStockQuoteDTO finnhubStockQuoteDto = new FinnhubStockQuoteDTO();
                    finnhubStockQuoteDto.setSymbol(symbol);
                    finnhubStockQuoteDto.setCurrentPrice(price);
                    return finnhubStockQuoteDto;
                }
        );
    }

    @Test
    public void testGet() {
        StockResponseDTO stockResponseDto = finnHubTradingService.get(symbol);
        Assert.assertEquals(stockResponseDto.getSymbol(), symbol);
        Assert.assertEquals(stockResponseDto.getPrice(), price);
    }
}
