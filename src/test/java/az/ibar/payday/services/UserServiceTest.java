package az.ibar.payday.services;

import az.ibar.payday.container.dtos.UserActivationRequestDTO;
import az.ibar.payday.container.dtos.UserRequestDTO;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.UserStatusesEnum;
import az.ibar.payday.container.exceptions.NotFoundException;
import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.repos.UserRepository;
import az.ibar.payday.services.impl.UserServiceImpl;
import az.ibar.payday.utils.converters.impl.ActivationMailMessageConverterImpl;
import az.ibar.payday.utils.converters.impl.UserDetailsConverterImpl;
import az.ibar.payday.utils.converters.impl.UserRequestConverterImpl;
import az.ibar.payday.utils.generators.impl.UUIDGeneratorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.any;
import static reactor.core.publisher.Mono.when;

public class UserServiceTest {
    private UserService userService;
    private UserRepository userRepository;
    private MailSenderService mailSenderService;
    private final String takenUsername = "takenUsername";
    private final String freeUsername = "freeUsername";
    private final String realEmail = "email@email.com";
    private final String fakeEmail = "fdafsdffds";
    private final String activationCode = "12345";
    private final Long deactivatedUserId = 1L;
    private final Long deletedUserId = 2L;

    @Before
    public void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        mailSenderService = Mockito.mock(MailSenderService.class);
        userService = new UserServiceImpl(
                userRepository,
                new UserRequestConverterImpl(),
                new UserDetailsConverterImpl(),
                new UUIDGeneratorImpl(),
                mailSenderService,
                new ActivationMailMessageConverterImpl("https://localhost:8080")
        );
        Mockito.when(userRepository.existsByUsernameOrEmail(
                Mockito.anyString(), Mockito.anyString(), any()
        )).thenAnswer(
                invocation -> {
                    String usernameArg = invocation.getArgument(0);
                    UserStatusesEnum userStatusExcludedArd = invocation.getArgument(2);
                    Assert.assertEquals(userStatusExcludedArd, UserStatusesEnum.DELETED);

                    if (usernameArg.equals(takenUsername)) {
                        return true;
                    } else if (usernameArg.equals(freeUsername)) {
                        return false;
                    }
                    return null;
                }
        );
        Mockito.when(mailSenderService.send(any())).thenAnswer(
                invocation -> {
                    MailMessage mailMessage = invocation.getArgument(0);
                    if (mailMessage.getRecipient().equals(realEmail)) {
                        return CompletableFuture.completedFuture(true);
                    } else if (mailMessage.getRecipient().equals(fakeEmail)) {
                        return CompletableFuture.completedFuture(false);
                    }
                    return CompletableFuture.completedFuture(false);
                }
        );
        Mockito.when(userRepository.findById(any())).thenAnswer(
                invocation -> {
                    Long userId = invocation.getArgument(0);
                    User user = new User();
                    user.setId(userId);
                    user.setActivationToken(activationCode);
                    user.setStatus(UserStatusesEnum.DEACTIVATED);
                    user.setEmail(realEmail);
                    if (userId.equals(deactivatedUserId)) {
                        user.setStatus(UserStatusesEnum.DEACTIVATED);
                    } else if (userId.equals(deletedUserId)) {
                        user.setStatus(UserStatusesEnum.DELETED);
                    }
                    return Optional.of(user);
                }
        );
        Mockito.when(userRepository.updateStatus(any(), any())).thenAnswer(
                invocation -> {
                    Long userId = invocation.getArgument(0);
                    UserStatusesEnum userStatus = invocation.getArgument(1);
                    Assert.assertEquals(userStatus, UserStatusesEnum.ACTIVE);
                    if (userId.equals(deactivatedUserId)) {
                        return 1;
                    }
                    return 0;
                }
        );
        Mockito.when(userRepository.findOneByUsernameOrEmail(
                any(), any()
        )).thenAnswer(
                invocation -> {
                    String emailOrUsername = invocation.getArgument(0);
                    UserStatusesEnum userStatus = invocation.getArgument(1);
                    Assert.assertEquals(userStatus, UserStatusesEnum.ACTIVE);
                    User user = new User();
                    user.setEmail(emailOrUsername);
                    user.setUsername(emailOrUsername);
                    user.setStatus(UserStatusesEnum.ACTIVE);
                    return user;
                }
        );
    }

    @Test
    public void testCheckUsername() {
        Assert.assertTrue(userService.checkUsernameOrEmailIsTaken(takenUsername, realEmail));
        Assert.assertFalse(userService.checkUsernameOrEmailIsTaken(freeUsername, realEmail));
        Assert.assertFalse(userService.checkUsernameOrEmailIsTaken("username", realEmail));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterWithTakenUsername() {
        UserRequestDTO userRequestDto = UserRequestDTO.builder()
                .username(takenUsername)
                .email(realEmail)
                .build();
        userService.registerUser(userRequestDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterWithFakeEmail() {
        UserRequestDTO userRequestDto = UserRequestDTO.builder()
                .username(freeUsername)
                .email(fakeEmail).build();
        userService.registerUser(userRequestDto);
    }

    @Test
    public void testRegister() {
        UserRequestDTO userRequestDto = UserRequestDTO.builder()
                .username(freeUsername)
                .email(realEmail)
                .build();
        Mockito.when(userRepository.save(any())).thenAnswer(
                invocation -> invocation.getArgument(0)
        );
        userService.registerUser(userRequestDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testActivateDeletedUser() {
        UserActivationRequestDTO userActivationRequestDto = new UserActivationRequestDTO();
        userActivationRequestDto.setActivationCode(activationCode);
        userActivationRequestDto.setEmail(realEmail);
        userActivationRequestDto.setId(deletedUserId);
        userService.activateUser(userActivationRequestDto);
    }

    @Test(expected = NotFoundException.class)
    public void testActivateWithWrongEmail() {
        UserActivationRequestDTO userActivationRequestDto = new UserActivationRequestDTO();
        userActivationRequestDto.setActivationCode(activationCode);
        userActivationRequestDto.setEmail(fakeEmail);
        userActivationRequestDto.setId(deactivatedUserId);
        userService.activateUser(userActivationRequestDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testActivateWithWrongActivationCode() {
        UserActivationRequestDTO userActivationRequestDto = new UserActivationRequestDTO();
        userActivationRequestDto.setActivationCode("54321");
        userActivationRequestDto.setEmail(realEmail);
        userActivationRequestDto.setId(deactivatedUserId);
        userService.activateUser(userActivationRequestDto);
    }

    @Test(expected = NotFoundException.class)
    public void testActivateWithNotUpdatable() {
        UserActivationRequestDTO userActivationRequestDto = new UserActivationRequestDTO();
        userActivationRequestDto.setActivationCode(activationCode);
        userActivationRequestDto.setEmail(realEmail);
        userActivationRequestDto.setId(100L);
        userService.activateUser(userActivationRequestDto);
    }

    @Test
    public void testActivate() {
        UserActivationRequestDTO userActivationRequestDto = new UserActivationRequestDTO();
        userActivationRequestDto.setActivationCode(activationCode);
        userActivationRequestDto.setEmail(realEmail);
        userActivationRequestDto.setId(deactivatedUserId);
        userService.activateUser(userActivationRequestDto);
    }

    @Test
    public void testFindByUsernameOrEmail() {
        AuthUserDetails authUserDetails = userService.findByUsernameOrEmail(freeUsername);
        Assert.assertEquals(authUserDetails.getUsername(), freeUsername);
    }
}
