package az.ibar.payday.services;

import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.dtos.StockOrderRequestDTO;
import az.ibar.payday.container.dtos.StockOrderResponseDTO;
import az.ibar.payday.container.dtos.StockResponseDTO;
import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.container.entities.PurchasedStock;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.StockTransactionModesEnum;
import az.ibar.payday.container.enums.StockTransactionStatusesEnum;
import az.ibar.payday.container.exceptions.NotFoundException;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.repos.StockTransactionRepository;
import az.ibar.payday.services.impl.StockTransactionServiceImpl;
import az.ibar.payday.services.trading.TradingService;
import az.ibar.payday.utils.TimeUtils;
import az.ibar.payday.utils.converters.impl.StockOrderResponseDtoConverterImpl;
import az.ibar.payday.utils.converters.impl.StockTransactionConverterImpl;
import az.ibar.payday.utils.generators.impl.UUIDGeneratorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class StockTransactionServiceTest {
    private StockTransactionService stockTransactionService;
    private StockTransactionRepository stockTransactionRepository;
    private BankAccountService bankAccountService;
    private MailSenderService mailSenderService;
    private PurchasedStockService purchasedStockService;
    private TradingService tradingService;
    private StockSettingsService stockSettingsService;
    private UserService userService;
    private TimeUtils timeUtils;
    private final String realEmail = "email@email.com";
    private final String fakeEmail = "fdafsdffds";
    private static final Long EXPECTED_MILLIS = Instant.now().plusSeconds(100).toEpochMilli();
    private static final Long EXPECTED_SECONDS = Instant.now().plusSeconds(100).getEpochSecond();
    private final Long activeUserId = 1L;
    private final Long deletedUserId = 100L;
    private final Long existBankAccountId = 1000L;
    private final Long removedBankAccountId = 1005L;
    private final String appleSymbol = "AAPL";
    private final BigDecimal appleSymbolUnits = BigDecimal.valueOf(4);
    private final Long stockTransactionId = 20L;

    @Before
    public void setUp() {
        stockTransactionRepository = mock(StockTransactionRepository.class);
        bankAccountService = mock(BankAccountService.class);
        mailSenderService = mock(MailSenderService.class);
        purchasedStockService = mock(PurchasedStockService.class);
        tradingService = mock(TradingService.class);
        stockSettingsService = mock(StockSettingsService.class);
        stockSettingsService = mock(StockSettingsService.class);
        userService = mock(UserService.class);
        timeUtils = mock(TimeUtils.class);

        stockTransactionService = new StockTransactionServiceImpl(
                stockTransactionRepository,
                new StockOrderResponseDtoConverterImpl(),
                new StockTransactionConverterImpl(),
                bankAccountService,
                mailSenderService,
                purchasedStockService,
                stockSettingsService,
                userService,
                timeUtils,
                new UUIDGeneratorImpl(),
                tradingService
        );
        User mockUser = new User();
        mockUser.setEmail("email.com");
        when(userService.getById(any())).thenReturn(mockUser);
        when(mailSenderService.send(any())).thenAnswer(
                invocation -> {
                    MailMessage mailMessage = invocation.getArgument(0);
                    if (mailMessage.getRecipient().equals(realEmail)) {
                        return CompletableFuture.completedFuture(true);
                    } else if (mailMessage.getRecipient().equals(fakeEmail)) {
                        return CompletableFuture.completedFuture(false);
                    }
                    return CompletableFuture.completedFuture(false);
                }
        );
        when(timeUtils.getEpochMillis()).thenReturn(EXPECTED_MILLIS);
        when(timeUtils.getEpochSeconds()).thenReturn(EXPECTED_SECONDS);
        when(stockSettingsService.exist(any())).thenReturn(true);
        when(stockTransactionRepository.findAllByUserId(anyLong())).thenAnswer(
                invocation -> {
                    Long userId = invocation.getArgument(0);
                    StockTransaction stockTransaction = new StockTransaction();
                    BankAccount bankAccount = new BankAccount();
                    User user = new User();
                    user.setId(userId);
                    bankAccount.setUser(user);
                    stockTransaction.setBankAccount(bankAccount);
                    stockTransaction.setId(activeUserId);
                    return Collections.singletonList(stockTransaction);
                }
        );

        when(stockTransactionRepository.findOneByUserId(anyLong(), anyLong())).thenAnswer(
                invocation -> {
                    Long id = invocation.getArgument(0);
                    Long userId = invocation.getArgument(1);
                    if (userId.equals(activeUserId)) {
                        StockTransaction stockTransaction = new StockTransaction();
                        BankAccount bankAccount = new BankAccount();
                        User user = new User();
                        user.setId(userId);
                        bankAccount.setUser(user);
                        bankAccount.setBalance(BigDecimal.valueOf(100));
                        stockTransaction.setBankAccount(bankAccount);
                        stockTransaction.setId(id);
                        return stockTransaction;
                    } else {
                        return null;
                    }
                }
        );
        when(bankAccountService.getById(anyLong())).thenAnswer(
                invocation -> {
                    Long bankAccountId = invocation.getArgument(0);
                    if (bankAccountId.equals(removedBankAccountId)) {
                        return Optional.empty();
                    } else if (bankAccountId.equals(existBankAccountId)) {
                        User user = new User();
                        user.setId(activeUserId);
                        BankAccount bankAccount = new BankAccount();
                        bankAccount.setUser(user);
                        bankAccount.setBalance(BigDecimal.valueOf(5000));
                        return Optional.of(bankAccount);
                    }
                    return Optional.empty();
                }
        );
        when(purchasedStockService.findOneRawByUserIdAndSymbol(any(), any())).thenAnswer(
                invocation -> {
                    Long userId = invocation.getArgument(0);
                    String symbol = invocation.getArgument(1);
                    if (symbol.equals(appleSymbol)) {
                        PurchasedStock purchasedStock = new PurchasedStock();
                        purchasedStock.setSymbolUnit(appleSymbolUnits);
                        purchasedStock.setSymbol(appleSymbol);
                        return purchasedStock;
                    }
                    return null;
                }
        );
        when(tradingService.get(anyString())).thenAnswer(
                invocation -> {
                    String symbol = invocation.getArgument(0);
                    if (symbol.equals(appleSymbol)) {
                        StockResponseDTO stockResponseDto = new StockResponseDTO();
                        stockResponseDto.setSymbol(symbol);
                        stockResponseDto.setPrice(BigDecimal.valueOf(25));
                        return stockResponseDto;
                    }
                    return null;
                }
        );
        when(stockTransactionRepository.save(any())).thenAnswer(
                invocation -> {
                    StockTransaction stockTransaction = invocation.getArgument(0);
                    if (stockTransaction.getId().equals(stockTransactionId)) {
                        return stockTransaction;
                    }
                    return null;
                }
        );
        when(bankAccountService.withdraw(any(), any(), any())).thenAnswer(
                invocation -> {
                    Long bankAccountId = invocation.getArgument(0);
                    BigDecimal funds = invocation.getArgument(1);
                    if (bankAccountId.equals(existBankAccountId)) {
                        BankAccountResponseDTO bankAccountResponseDto = new BankAccountResponseDTO();
                        bankAccountResponseDto.setBalance(BigDecimal.valueOf(5000).subtract(funds));
                        return bankAccountResponseDto;
                    } else {
                        return null;
                    }
                }
        );
        when(bankAccountService.topUp(any(), any(), any())).thenAnswer(
                invocation -> {
                    Long bankAccountId = invocation.getArgument(0);
                    BigDecimal funds = invocation.getArgument(1);
                    if (bankAccountId.equals(existBankAccountId)) {
                        BankAccountResponseDTO bankAccountResponseDto = new BankAccountResponseDTO();
                        bankAccountResponseDto.setBalance(BigDecimal.valueOf(5000).add(funds));
                        return bankAccountResponseDto;
                    } else {
                        return null;
                    }
                }
        );
        when(stockTransactionRepository.updateStatus(any(), any(), any(), any(), any())).thenAnswer(
                invocation -> {
                    Long id = invocation.getArgument(0);
                    if (id.equals(1L)) {
                        return 1;
                    }
                    return 0;
                }
        );
    }

    @Test
    public void testFindAllByUserId() {
        List<StockOrderResponseDTO> stockOrders = stockTransactionService.findAllByUserId(activeUserId);
        assertFalse(stockOrders.isEmpty());
        assertEquals(stockOrders.size(), 1);
    }

    @Test
    public void testFindOneByUserIdAndId() {
        StockOrderResponseDTO stockOrder = stockTransactionService.findOneByUserId(1L, activeUserId);
        assertNotNull(stockOrder);
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneByUserIdAndIdWithNullUser() {
        stockTransactionService.findOneByUserId(1L, deletedUserId);
    }

    @Test(expected = NotFoundException.class)
    public void testSellWithFakeBankAccount() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(removedBankAccountId);
        stockTransactionService.sell(stockOrderRequestDto);
    }


    @Test(expected = NotFoundException.class)
    public void testSellWithOtherUsersBankAccount() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(deletedUserId);
        stockTransactionService.sell(stockOrderRequestDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSellWithoutPurchasedStock() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(activeUserId);
        stockOrderRequestDto.setSymbol("GOOG");
        stockTransactionService.sell(stockOrderRequestDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSellWithMoreThanTheyHave() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(activeUserId);
        stockOrderRequestDto.setSymbol(appleSymbol);
        stockOrderRequestDto.setSymbolUnit(appleSymbolUnits.add(BigDecimal.TEN));
        stockTransactionService.sell(stockOrderRequestDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSellWithNotAcceptableSaving() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(activeUserId);
        stockOrderRequestDto.setSymbol(appleSymbol);
        stockOrderRequestDto.setSymbolUnit(appleSymbolUnits);
        stockOrderRequestDto.setId(stockTransactionId + 5);
        stockTransactionService.sell(stockOrderRequestDto);
    }

    @Test
    public void testSell() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(activeUserId);
        stockOrderRequestDto.setSymbol(appleSymbol);
        stockOrderRequestDto.setSymbolUnit(appleSymbolUnits);
        stockOrderRequestDto.setId(stockTransactionId);
        stockOrderRequestDto.setTargetAmount(BigDecimal.valueOf(155));
        stockOrderRequestDto.setMode(StockTransactionModesEnum.SELL.getValue());

        StockOrderResponseDTO stockOrderResponseDto = stockTransactionService.sell(stockOrderRequestDto);
        Assert.assertEquals(stockOrderResponseDto.getId(), stockOrderRequestDto.getId());
        Assert.assertEquals(stockOrderResponseDto.getSymbol(), stockOrderRequestDto.getSymbol());
        Assert.assertEquals(stockOrderResponseDto.getSymbolUnit(), stockOrderRequestDto.getSymbolUnit());
        Assert.assertEquals(stockOrderResponseDto.getTargetAmount(), stockOrderRequestDto.getTargetAmount());
        Assert.assertEquals(stockOrderResponseDto.getMode().getValue(), stockOrderRequestDto.getMode());
    }

    @Test(expected = NotFoundException.class)
    public void testBuyWithFakeBankAccount() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(removedBankAccountId);
        stockTransactionService.buy(stockOrderRequestDto);
    }


    @Test(expected = NotFoundException.class)
    public void testBuyWithOtherUsersBankAccount() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(deletedUserId);
        stockTransactionService.buy(stockOrderRequestDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuyWithMoreThanUserHas() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(activeUserId);
        stockOrderRequestDto.setSymbol(appleSymbol);
        stockOrderRequestDto.setSymbolUnit(BigDecimal.valueOf(5000));
        stockOrderRequestDto.setTargetAmount(BigDecimal.valueOf(155));
        stockTransactionService.buy(stockOrderRequestDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuyWithNotAcceptableSaving() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(activeUserId);
        stockOrderRequestDto.setSymbol(appleSymbol);
        stockOrderRequestDto.setSymbolUnit(appleSymbolUnits);
        stockOrderRequestDto.setId(stockTransactionId + 5);
        stockOrderRequestDto.setTargetAmount(BigDecimal.valueOf(155));
        stockTransactionService.buy(stockOrderRequestDto);
    }

    @Test
    public void testBuy() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(existBankAccountId);
        stockOrderRequestDto.setUserId(activeUserId);
        stockOrderRequestDto.setSymbol(appleSymbol);
        stockOrderRequestDto.setSymbolUnit(appleSymbolUnits);
        stockOrderRequestDto.setId(stockTransactionId);
        stockOrderRequestDto.setTargetAmount(BigDecimal.valueOf(155));
        stockOrderRequestDto.setMode(StockTransactionModesEnum.BUY.getValue());

        StockOrderResponseDTO stockOrderResponseDto = stockTransactionService.buy(stockOrderRequestDto);
        Assert.assertEquals(stockOrderResponseDto.getId(), stockOrderRequestDto.getId());
        Assert.assertEquals(stockOrderResponseDto.getSymbol(), stockOrderRequestDto.getSymbol());
        Assert.assertEquals(stockOrderResponseDto.getSymbolUnit(), stockOrderRequestDto.getSymbolUnit());
        Assert.assertEquals(stockOrderResponseDto.getTargetAmount(), stockOrderRequestDto.getTargetAmount());
        Assert.assertEquals(stockOrderResponseDto.getMode().getValue(), stockOrderRequestDto.getMode());
    }

    @Test
    public void testHandleStockForBuying() {
        when(stockTransactionRepository.findAllQualifiedTransactions(
                anyString(), any(), anyInt(), anyInt(), any()
        )).thenAnswer(invocation -> {
            StockTransaction stockTransactionToBuy = new StockTransaction();
            stockTransactionToBuy.setMode(StockTransactionModesEnum.BUY.getValue());
            return Collections.singletonList(stockTransactionToBuy);
        });
        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(appleSymbol);
        stockResponseDto.setPrice(BigDecimal.TEN);
        stockTransactionService.handleStockUpdate(stockResponseDto);
    }

    @Test
    public void testHandleStockForSelling() {
        when(stockTransactionRepository.findAllQualifiedTransactions(
                anyString(), any(), anyInt(), anyInt(), any()
        )).thenAnswer(invocation -> {
            StockTransaction stockTransactionToSell = new StockTransaction();
            stockTransactionToSell.setMode(StockTransactionModesEnum.SELL.getValue());
            return Collections.singletonList(stockTransactionToSell);
        });
        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(appleSymbol);
        stockResponseDto.setPrice(BigDecimal.TEN);
        stockTransactionService.handleStockUpdate(stockResponseDto);
    }

    @Test
    public void testHandleStockForUnknownOperation() {
        when(stockTransactionRepository.findAllQualifiedTransactions(
                anyString(), any(), anyInt(), anyInt(), any()
        )).thenAnswer(invocation -> {
            StockTransaction stockTransactionUnknown = new StockTransaction();
            stockTransactionUnknown.setMode(StockTransactionModesEnum.UNKNOWN.getValue());
            return Collections.singletonList(stockTransactionUnknown);
        });
        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(appleSymbol);
        stockResponseDto.setPrice(BigDecimal.TEN);
        stockTransactionService.handleStockUpdate(stockResponseDto);
    }

    @Test
    public void testBuyOperationWithNoBalance() {
        User user = new User();
        user.setId(1L);
        user.setEmail("email@email.com");
        when(userService.findById(any())).thenReturn(Optional.of(user));
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(BigDecimal.valueOf(0));
        bankAccount.setUser(user);
        bankAccount.setId(1L);
        when(bankAccountService.getById(bankAccount.getId()))
                .thenReturn(Optional.of(bankAccount));
        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setMode(StockTransactionModesEnum.BUY.getValue());
        stockTransaction.setSymbolUnit(BigDecimal.ONE);
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setId(5L);

        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(appleSymbol);
        stockResponseDto.setPrice(BigDecimal.valueOf(5000));
        stockTransactionService.buyOperation(stockTransaction, stockResponseDto);
        verify(mailSenderService, times(1)).send(any());

        stockTransaction.setId(1L);
        stockTransactionService.buyOperation(stockTransaction, stockResponseDto);
        bankAccount.setBalance(BigDecimal.valueOf(5000));
        stockTransactionService.buyOperation(stockTransaction, stockResponseDto);
        verify(bankAccountService, times(1)).withdraw(
                any(), any(), any()
        );
    }

    @Test(expected = RuntimeException.class)
    public void testSellOperationWithOtherTransaction() {
        User user = new User();
        user.setEmail("email@email.com");
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(BigDecimal.valueOf(0));
        bankAccount.setUser(user);
        bankAccount.setId(1L);

        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setMode(StockTransactionModesEnum.SELL.getValue());
        stockTransaction.setSymbolUnit(BigDecimal.ONE);
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setSymbol(appleSymbol);
        stockTransaction.setId(5L);

        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(appleSymbol);
        stockResponseDto.setPrice(BigDecimal.valueOf(5000));
        stockTransactionService.sellOperation(stockTransaction, stockResponseDto);
    }

    @Test(expected = RuntimeException.class)
    public void testSellOperationSameTransactionAtTheSametime() {
        User user = new User();
        user.setEmail("email@email.com");
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(BigDecimal.valueOf(0));
        bankAccount.setUser(user);
        bankAccount.setId(removedBankAccountId);

        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setMode(StockTransactionModesEnum.SELL.getValue());
        stockTransaction.setSymbolUnit(BigDecimal.ONE);
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setSymbol(appleSymbol);
        stockTransaction.setId(1L);

        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(appleSymbol);
        stockResponseDto.setPrice(BigDecimal.valueOf(5000));
        stockTransactionService.sellOperation(stockTransaction, stockResponseDto);
    }

    @Test
    public void testSellOperation() {
        User user = new User();
        user.setEmail("email@email.com");
        user.setId(1L);
        when(userService.findById(any())).thenReturn(Optional.of(user));

        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(BigDecimal.valueOf(0));
        bankAccount.setUser(user);
        bankAccount.setId(existBankAccountId);
        when(bankAccountService.getById(bankAccount.getId()))
                .thenReturn(Optional.of(bankAccount));

        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setMode(StockTransactionModesEnum.SELL.getValue());
        stockTransaction.setSymbolUnit(BigDecimal.ONE);
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setSymbol(appleSymbol);
        stockTransaction.setId(1L);

        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(appleSymbol);
        stockResponseDto.setPrice(BigDecimal.valueOf(5000));
        stockTransactionService.sellOperation(stockTransaction, stockResponseDto);
        verify(bankAccountService, times(1)).topUp(any(), any(), any());
        verify(stockTransactionRepository, times(1)).updateStatus(
                any(), eq(StockTransactionStatusesEnum.COMPLETED), any(), any(), any()
        );
    }

    @Test
    public void testSellOperationInsufficientStockUnits() {
        User user = new User();
        user.setId(1L);
        user.setEmail("email@email.com");
        when(userService.findById(any())).thenReturn(Optional.of(user));
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBalance(BigDecimal.valueOf(0));
        bankAccount.setUser(user);
        bankAccount.setId(existBankAccountId);
        when(bankAccountService.getById(bankAccount.getId()))
                .thenReturn(Optional.of(bankAccount));

        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setMode(StockTransactionModesEnum.SELL.getValue());
        stockTransaction.setSymbolUnit(BigDecimal.valueOf(50000));
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setSymbol(appleSymbol);
        stockTransaction.setId(1L);

        StockResponseDTO stockResponseDto = new StockResponseDTO();
        stockResponseDto.setSymbol(appleSymbol);
        stockResponseDto.setPrice(BigDecimal.valueOf(5000));
        stockTransactionService.sellOperation(stockTransaction, stockResponseDto);
        verify(stockTransactionRepository, times(1)).updateStatus(
                any(), eq(StockTransactionStatusesEnum.DEACTIVATED), any(), any(), any()
        );
    }

}
