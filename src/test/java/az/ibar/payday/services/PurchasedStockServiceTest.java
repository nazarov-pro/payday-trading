package az.ibar.payday.services;

import az.ibar.payday.container.entities.PurchasedStock;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.exceptions.NotFoundException;
import az.ibar.payday.repos.PurchasedStockRepository;
import az.ibar.payday.services.impl.PurchasedStockServiceImpl;
import az.ibar.payday.utils.TimeUtils;
import az.ibar.payday.utils.converters.impl.PurchasedStockDtoConverterImpl;
import az.ibar.payday.utils.generators.impl.UUIDGeneratorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PurchasedStockServiceTest {
    private PurchasedStockService purchasedStockService;
    private PurchasedStockRepository purchasedStockRepository;
    private TimeUtils timeUtils;

    private String existedSymbol = "AAPL";
    private BigDecimal existedSymbolUnit = BigDecimal.ONE;

    private String newSymbol = "GOOG";

    @Before
    public void setUp() {
        timeUtils = Mockito.mock(TimeUtils.class);
        purchasedStockRepository = Mockito.mock(PurchasedStockRepository.class);
        purchasedStockService = new PurchasedStockServiceImpl(
                purchasedStockRepository,
                new PurchasedStockDtoConverterImpl(),
                timeUtils,
                new UUIDGeneratorImpl()
        );

    }

    @Test
    public void testSaveExistedPurchasedStock() {
        BigDecimal symbolUnit = BigDecimal.TEN;
        User user = new User();
        user.setId(1L);
        PurchasedStock purchasedStock = new PurchasedStock();
        purchasedStock.setUser(user);
        purchasedStock.setSymbolUnit(symbolUnit);
        purchasedStock.setSymbol(existedSymbol);
        when(purchasedStockService.findOneRawByUserIdAndSymbol(any(), any())).thenAnswer(
                invocation -> {
                    Long userId = invocation.getArgument(0);
                    String symbol = invocation.getArgument(1);
                    if (symbol.equals(existedSymbol)) {
                        PurchasedStock purchasedStockArg = new PurchasedStock();
                        purchasedStockArg.setSymbolUnit(existedSymbolUnit);
                        purchasedStockArg.setSymbol(symbol);
                        return purchasedStockArg;
                    }
                    return null;
                }
        );

        when(purchasedStockRepository.save(any()))
                .thenAnswer(invocation ->  {
                    PurchasedStock purchasedStockArg = invocation.getArgument(0);
                    Assert.assertTrue(symbolUnit.add(existedSymbolUnit)
                            .equals(purchasedStockArg.getSymbolUnit()));
                    return purchasedStockArg;
                });
        purchasedStockService.save(purchasedStock);
    }

    @Test
    public void testSaveNewPurchasedStock() {
        User user = new User();
        user.setId(1L);
        PurchasedStock purchasedStock = new PurchasedStock();
        purchasedStock.setUser(user);
        purchasedStock.setSymbol(newSymbol);
        purchasedStock.setSymbolUnit(BigDecimal.TEN);
        when(purchasedStockRepository.save(any()))
                .thenAnswer(invocation ->  {
                    PurchasedStock purchasedStockArg = invocation.getArgument(0);
                    Assert.assertTrue(purchasedStock.getSymbolUnit()
                            .equals(purchasedStockArg.getSymbolUnit()));
                    return purchasedStockArg;
                });
        purchasedStockService.save(purchasedStock);
    }

    @Test
    public void testFindAll() {
        when(purchasedStockRepository.findAllByUserId(any()))
                .thenReturn(Collections.emptyList());
        purchasedStockService.findAllByUserId(1L);
        verify(purchasedStockRepository, times(1))
                .findAllByUserId(any());
    }

    @Test(expected = NotFoundException.class)
    public void testFindOneByUserIdAndSymbolWithInvalidSymbol() {
        when(purchasedStockRepository.findOneByUserAndSymbol(any(), any()))
                .thenReturn(null);
        purchasedStockService.findOneByUserIdAndSymbol(1L, newSymbol);
    }

    @Test
    public void testFindOneByUserIdAndSymbol() {
        User user = new User();
        user.setId(1L);
        PurchasedStock purchasedStock = new PurchasedStock();
        purchasedStock.setUser(user);
        purchasedStock.setSymbol(newSymbol);
        purchasedStock.setSymbolUnit(BigDecimal.TEN);

        when(purchasedStockRepository.findOneByUserAndSymbol(any(), any()))
                .thenReturn(purchasedStock);
        purchasedStockService.findOneByUserIdAndSymbol(1L, newSymbol);
        verify(purchasedStockRepository, times(1))
                .findOneByUserAndSymbol(any(), any());
    }

    @Test
    public void testFindOneRawByUserIdAndSymbol() {
        when(purchasedStockRepository.findOneByUserAndSymbol(any(), any()))
                .thenReturn(null);
        purchasedStockService.findOneRawByUserIdAndSymbol(1L, newSymbol);
        verify(purchasedStockRepository, times(1))
                .findOneByUserAndSymbol(any(), any());
    }



}
