package az.ibar.payday.services;

import az.ibar.payday.container.entities.StockSettings;
import az.ibar.payday.repos.StockSettingsRepository;
import az.ibar.payday.services.impl.StockSettingsServiceImpl;
import az.ibar.payday.utils.TimeUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class StockSettingsServiceTest {
    private StockSettingsService stockSettingsService;
    private StockSettingsRepository stockSettingsRepository;
    private TimeUtils timeUtils;

    private static final Long EXPECTED_MILLIS = Instant.now().plusSeconds(100).toEpochMilli();
    private static final Long EXPECTED_SECONDS = Instant.now().plusSeconds(100).getEpochSecond();


    @Before
    public void setUp() {
        stockSettingsRepository = Mockito.mock(StockSettingsRepository.class);
        timeUtils = Mockito.mock(TimeUtils.class);
        stockSettingsService = new StockSettingsServiceImpl(
                stockSettingsRepository, timeUtils
        );
        when(timeUtils.getEpochMillis()).thenReturn(EXPECTED_MILLIS);
        when(timeUtils.getEpochSeconds()).thenReturn(EXPECTED_SECONDS);
    }

    @Test
    public void testUpdateAndGet() {
        when(stockSettingsRepository.updateAndGet(any(), any(), any()))
                .thenReturn(Collections.emptyList());
        int expiredSeconds = 100;
        stockSettingsService.updateAndGet(
                "feed", expiredSeconds
        );
        Long expectedResult = timeUtils.getEpochMillis() - (expiredSeconds * 1000);
        verify(stockSettingsRepository, times(1))
                .updateAndGet(any(), any(), eq(expectedResult));
    }

    @Test
    public void testUnlockAllNothingUpdated() {
        when(stockSettingsRepository.unlockAll(any(), any()))
                .thenReturn(0);
        Assert.assertFalse(stockSettingsService.unlockAll("feed"));
    }

    @Test
    public void testUnlockAll() {
        when(stockSettingsRepository.unlockAll(any(), any()))
                .thenReturn(10);
        Assert.assertTrue(stockSettingsService.unlockAll("feed"));
    }

    @Test
    public void testUnlockOneNothingUpdated() {
        when(stockSettingsRepository.unlockOne(any(), any(), any()))
                .thenReturn(0);
        Assert.assertFalse(stockSettingsService.unlockOne("AAPL", "feed"));
    }

    @Test
    public void testUnlockOne() {
        when(stockSettingsRepository.unlockOne(any(), any(), any()))
                .thenReturn(1);
        Assert.assertTrue(stockSettingsService.unlockOne("AAPL", "feed"));
    }

    @Test
    public void testUpdatePriceNothingUpdated() {
        when(stockSettingsRepository.updatePrice(any(), any(), any(), any()))
                .thenReturn(0);
        Assert.assertFalse(stockSettingsService.updatePrice("AAPL", "feed", BigDecimal.ONE));
    }

    @Test
    public void testUpdatePrice() {
        when(stockSettingsRepository.updatePrice(any(), any(), any(), any()))
                .thenReturn(1);
        Assert.assertTrue(stockSettingsService.updatePrice("AAPL", "feed", BigDecimal.ONE));
    }

    @Test
    public void testUpdateAllSubsLastTimestampNothingUpdated() {
        when(stockSettingsRepository.updateAllSubscribersLastTmp(any(), any()))
                .thenReturn(0);
        Assert.assertFalse(stockSettingsService.updateAllSubscribersLastTmp("feed"));
    }

    @Test
    public void testUpdateAllSubsLastTimestamp() {
        when(stockSettingsRepository.updateAllSubscribersLastTmp(any(), any()))
                .thenReturn(2);
        Assert.assertTrue(stockSettingsService.updateAllSubscribersLastTmp("feed"));
    }

    @Test
    public void testSave() {
        StockSettings stockSettings = new StockSettings();
        when(stockSettingsRepository.save(any())).thenAnswer(
                invocation -> invocation.<StockSettings>getArgument(0)
        );
        Assert.assertEquals(stockSettings, stockSettingsService.save(stockSettings));
    }

    @Test
    public void testExistById() {
        StockSettings stockSettings = new StockSettings();
        when(stockSettingsRepository.existsById(any())).thenReturn(true);
        Assert.assertTrue(stockSettingsService.exist("AAPL"));
    }
}
