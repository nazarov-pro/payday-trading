package az.ibar.payday.services;

import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.container.entities.StockSettings;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.repos.BankAccountRepository;
import az.ibar.payday.services.impl.BankAccountServiceImpl;
import az.ibar.payday.utils.converters.impl.BankAccountDtoConverterImpl;
import az.ibar.payday.utils.generators.impl.UUIDGeneratorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class BankAccountServiceTest {
    private BankAccountService bankAccountService;
    private BankAccountRepository bankAccountRepository;
    private final Long removedBankAccountId = 0L;
    private final Long activeBankAccountId = 1L;

    @Before
    public void setUp() {
        bankAccountRepository = Mockito.mock(BankAccountRepository.class);
        bankAccountService = new BankAccountServiceImpl(
                bankAccountRepository, new BankAccountDtoConverterImpl(), new UUIDGeneratorImpl()
        );
        when(bankAccountRepository.updateAndGet(any(), any(), any(), any()))
                .thenAnswer(invocation -> {
                    Long id = invocation.getArgument(0);
                    if (id.equals(removedBankAccountId)) {
                        return null;
                    }
                    BankAccount bankAccount = new BankAccount();
                    bankAccount.setId(id);
                    bankAccount.setUser(new User());
                    return bankAccount;
                });
    }

    @Test
    public void testList() {
        when(bankAccountRepository.findAllByUserId(any()))
                .thenReturn(Collections.emptyList());
        bankAccountService.list(1L);
        verify(bankAccountRepository, times(1))
                .findAllByUserId(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateSavingProblem() {
        when(bankAccountRepository.save(any()))
                .thenReturn(null);
        bankAccountService.create(1L);
    }

    @Test
    public void testCreate() {
        when(bankAccountRepository.save(any()))
                .thenAnswer(invocation -> invocation.<BankAccount>getArgument(0));
        bankAccountService.create(activeBankAccountId);
        verify(bankAccountRepository, times(1))
                .save(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTopUpAddingNegativeAmount() {
        bankAccountService.topUp(activeBankAccountId, BigDecimal.ZERO.subtract(BigDecimal.TEN), "v1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTopUpBlockingAccountInDbLevel() {
        bankAccountService.topUp(removedBankAccountId, BigDecimal.TEN, "v1");
    }

    @Test
    public void testTopUp() {
        BigDecimal amount = BigDecimal.TEN;
        bankAccountService.topUp(activeBankAccountId, amount, "v1");
        verify(bankAccountRepository, times(1))
                .updateAndGet(eq(activeBankAccountId), eq(amount), any(), any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawingBlockingAccountInDbLevel() {
        BigDecimal amount = new BigDecimal("-100");
        bankAccountService.withdraw(removedBankAccountId, amount, "v1");
    }

    @Test
    public void testWitdrawWithPositiveAmount() {
        BigDecimal amount = BigDecimal.TEN;
        bankAccountService.withdraw(activeBankAccountId, amount, "v1");
        verify(bankAccountRepository, times(1))
                .updateAndGet(eq(activeBankAccountId), eq(amount.negate()), any(), any());
    }

    @Test
    public void testWithdraw() {
        BigDecimal amount = BigDecimal.valueOf(-100);
        bankAccountService.withdraw(activeBankAccountId, amount, "v1");
        verify(bankAccountRepository, times(1))
                .updateAndGet(eq(activeBankAccountId), eq(amount), any(), any());
    }

    @Test
    public void testCheckUserIdAndBankAccountId() {
        when(bankAccountRepository.existsByIdAndUserId(any(), any()))
                .thenReturn(true);
        Assert.assertTrue(bankAccountService.checkUserIdAndBankAccountId(1L, activeBankAccountId));
        verify(bankAccountRepository, times(1))
                .existsByIdAndUserId(any(), any());
    }

    @Test
    public void testGetById() {
        when(bankAccountRepository.findById(any()))
                .thenReturn(Optional.empty());
        Assert.assertFalse(bankAccountService.getById(1L).isPresent());
        verify(bankAccountRepository, times(1))
                .findById(any());
    }
}
