package az.ibar.payday.resources;

import az.ibar.payday.configs.ContainerSupport;
import az.ibar.payday.container.dtos.AuthRequestDTO;
import az.ibar.payday.container.dtos.AuthResponseDTO;
import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.dtos.BankAccountTransactionRequestDTO;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.UserStatusesEnum;
import az.ibar.payday.container.models.ApiCollectionResponse;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.services.BankAccountService;
import az.ibar.payday.services.UserService;
import az.ibar.payday.utils.Constants;
import az.ibar.payday.utils.TimeUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.math.BigDecimal;
import java.time.Instant;

import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureWebTestClient
class BankAccountResourceIntegrationTest extends ContainerSupport {
    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private UserService userService;

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @MockBean
    TimeUtils timeUtils;
    private static final Instant NOW = Instant.now();

    private User user;
    private String token;

    // User Authentication
    @BeforeAll
    public void setUp() {
        when(timeUtils.getEpochSeconds()).thenReturn(NOW.getEpochSecond());
        when(timeUtils.getEpochMillis()).thenReturn(NOW.toEpochMilli());
        String rawPassword = "testSecured1234";

        user = new User();
        user.setPassword(passwordEncoder.encode(rawPassword));
        user.setEmail("bank-account-test@domain.com");
        user.setUsername("bank-account-test");
        user.setStatus(UserStatusesEnum.ACTIVE);
        user.setActivationToken("just-an-activation-token");
        user = userService.save(user);

        AuthRequestDTO authRequestDTO = AuthRequestDTO.builder()
                .usernameOrEmail(user.getEmail()).password(rawPassword).build();

        ApiResponse<AuthResponseDTO> responseBody = webTestClient.post()
                .uri("/auth")
                .bodyValue(authRequestDTO)
                .accept(MediaType.APPLICATION_JSON)
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<AuthResponseDTO>>() {
                })
                .returnResult()
                .getResponseBody();
        Assert.assertNotNull(responseBody);
        Assert.assertNotNull(responseBody.getItem());
        token = responseBody.getItem().getToken();
    }


    @Test
    void givenUserWhenCreateThenCreateBankAccountWithZeroBalance() {
        webTestClient.post()
                .uri("/bank-accounts/create")
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<BankAccountResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItem());
                            BankAccountResponseDTO bankAccount = response.getResponseBody().getItem();
                            Assert.assertEquals(bankAccount.getBalance(), BigDecimal.ZERO);
                        }
                );
    }

    @Test
    void givenBankAccountWhenTopUpThenIncreaseAccountsBalance() {
        BankAccountResponseDTO bankAccountResponseDTO = bankAccountService.create(user.getId());
        BankAccountTransactionRequestDTO bankAccountTransactionRequestDTO =
                BankAccountTransactionRequestDTO.builder()
                        .amount(BigDecimal.valueOf(500.50))
                        .version(bankAccountResponseDTO.getVersion())
                        .build();

        webTestClient.post()
                .uri("/bank-accounts/{id}/top-up", bankAccountResponseDTO.getId())
                .bodyValue(bankAccountTransactionRequestDTO)
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<BankAccountResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItem());
                            BankAccountResponseDTO bankAccount = response.getResponseBody().getItem();
                            Assert.assertEquals(bankAccount.getBalance()
                                    .compareTo(bankAccountTransactionRequestDTO.getAmount()), 0);
                            Assert.assertNotEquals(bankAccount.getVersion(),
                                    bankAccountTransactionRequestDTO.getVersion());
                        }
                );
    }

    @Test
    void givenBankAccountWhenListThenReturnBankAccount() {
        BankAccountResponseDTO bankAccountResponseDTO = bankAccountService.create(user.getId());
        webTestClient.get()
                .uri("/bank-accounts")
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiCollectionResponse<BankAccountResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItems());
                            long countOfRequiredItems = response.getResponseBody().getItems().stream()
                                    .filter(item ->
                                            item.getId().equals(bankAccountResponseDTO.getId()) &&
                                                    item.getVersion().equals(bankAccountResponseDTO.getVersion()) &&
                                                    item.getBalance()
                                                            .compareTo(bankAccountResponseDTO.getBalance()) == 0
                                    ).count();
                            Assert.assertEquals(countOfRequiredItems, 1);
                        }
                );
    }

}
