package az.ibar.payday.resources;

import az.ibar.payday.configs.ContainerSupport;
import az.ibar.payday.container.dtos.*;
import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.container.entities.PurchasedStock;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.StockTransactionModesEnum;
import az.ibar.payday.container.enums.StockTransactionStatusesEnum;
import az.ibar.payday.container.enums.UserStatusesEnum;
import az.ibar.payday.container.models.ApiCollectionResponse;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.services.*;
import az.ibar.payday.services.trading.FinnHubTradingV1Client;
import az.ibar.payday.services.trading.TradingService;
import az.ibar.payday.utils.Constants;
import az.ibar.payday.utils.TimeUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collection;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureWebTestClient
class StockResourceIntegrationTest extends ContainerSupport {
    private static final Instant NOW = Instant.now();

    @Autowired
    private WebTestClient webTestClient;
    @Autowired
    private UserService userService;
    @Autowired
    private BankAccountService bankAccountService;
    @Autowired
    private PurchasedStockService purchasedStockService;
    @Autowired
    private StockTransactionService stockTransactionService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private StockSettingsService stockSettingsService;
    @MockBean
    private TimeUtils timeUtils;
    @MockBean(name = Constants.BEAN_FINN_HUB_NAME)
    private TradingService tradingService;

    private User user;
    private String token;
    private BankAccountResponseDTO bankAccount;

    @BeforeAll
    public void setUp() {
        when(timeUtils.getEpochSeconds()).thenReturn(NOW.getEpochSecond());
        when(timeUtils.getEpochMillis()).thenReturn(NOW.toEpochMilli());

        String rawPassword = "testSecured1234";

        user = new User();
        user.setPassword(passwordEncoder.encode(rawPassword));
        user.setEmail("stock-test@domain.com");
        user.setUsername("stock-test");
        user.setStatus(UserStatusesEnum.ACTIVE);
        user.setActivationToken("just-an-activation-token");
        user = userService.save(user);

        AuthRequestDTO authRequestDTO = AuthRequestDTO.builder()
                .usernameOrEmail(user.getEmail()).password(rawPassword).build();

        ApiResponse<AuthResponseDTO> responseBody = webTestClient.post()
                .uri("/auth")
                .bodyValue(authRequestDTO)
                .accept(MediaType.APPLICATION_JSON)
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<AuthResponseDTO>>() {
                })
                .returnResult()
                .getResponseBody();

        Assert.assertNotNull(responseBody);
        Assert.assertNotNull(responseBody.getItem());
        token = responseBody.getItem().getToken();

        BigDecimal initialFunds = BigDecimal.valueOf(5000);
        bankAccount = bankAccountService.create(user.getId());
        bankAccount = bankAccountService
                .topUp(bankAccount.getId(), initialFunds, bankAccount.getVersion());
        Assert.assertEquals(initialFunds.compareTo(bankAccount.getBalance()), 0);
    }


    @Test
    void givenPurchasedStockWhenListPurchasedStocksThenReturnPurchasedStocks() {
        //given
        PurchasedStock purchasedStock = new PurchasedStock();
        purchasedStock.setSymbol("AAPL");
        purchasedStock.setUser(user);
        purchasedStock.setSymbolUnit(BigDecimal.ONE);
        purchasedStockService.save(purchasedStock);

        webTestClient.get()
                .uri("/stocks")
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiCollectionResponse<PurchasedStockResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItems());
                            long requiredPurchasedStocksSize = response.getResponseBody().getItems()
                                    .stream().filter(
                                            purchasedStockResponse ->
                                                    purchasedStockResponse.getId().equals(purchasedStock.getId()) &&
                                                            purchasedStockResponse.getSymbol().equals(purchasedStock.getSymbol())
                                    ).count();
                            Assert.assertEquals(requiredPurchasedStocksSize, 1);
                        }
                );
    }

    @Test
    void givenStockTransactionWhenListStockOrdersThenReturnStockOrders() {
        //given
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(this.bankAccount.getId());

        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setSymbol("AAPL");
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setSymbolUnit(BigDecimal.ONE);
        stockTransaction.setMode(StockTransactionModesEnum.SELL.getValue());
        stockTransaction.setStatus(StockTransactionStatusesEnum.ACTIVE);
        stockTransaction.setStartAmount(BigDecimal.valueOf(100));
        stockTransaction.setTargetAmount(BigDecimal.valueOf(125));
        stockTransactionService.save(stockTransaction);

        webTestClient.get()
                .uri("/stocks/orders")
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiCollectionResponse<StockOrderResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItems());
                            long requiredPurchasedStocksSize = response.getResponseBody().getItems()
                                    .stream().filter(
                                            stockOrderResponseDTO ->
                                                    stockOrderResponseDTO.getId().equals(stockTransaction.getId()) &&
                                                            stockOrderResponseDTO.getSymbol()
                                                                    .equals(stockTransaction.getSymbol()) &&
                                                            stockOrderResponseDTO.getCreatedTimestamp()
                                                                    .equals(stockTransaction.getCreatedTimestamp())
                                    ).count();
                            Assert.assertEquals(requiredPurchasedStocksSize, 1);
                        }
                );
    }

    @Test
    void givenStockTransactionWhenGetStockOrdersThenReturnTheStockOrder() {
        //given
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(this.bankAccount.getId());

        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setSymbol("GOOG");
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setSymbolUnit(BigDecimal.ONE);
        stockTransaction.setMode(StockTransactionModesEnum.BUY.getValue());
        stockTransaction.setStatus(StockTransactionStatusesEnum.DEACTIVATED);
        stockTransaction.setStartAmount(BigDecimal.valueOf(100));
        stockTransaction.setTargetAmount(BigDecimal.valueOf(125));
        stockTransactionService.save(stockTransaction);

        webTestClient.get()
                .uri("/stocks/orders/{id}", stockTransaction.getId())
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<StockOrderResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItem());
                            StockOrderResponseDTO stockOrderResponseDTO = response.getResponseBody().getItem();
                            Assert.assertEquals(stockOrderResponseDTO.getId(),
                                    stockTransaction.getId());
                            Assert.assertEquals(stockOrderResponseDTO.getSymbol(),
                                    stockTransaction.getSymbol());
                            Assert.assertEquals(stockOrderResponseDTO.getCreatedTimestamp(),
                                    stockTransaction.getCreatedTimestamp());
                        }
                );
    }

    @Test
    void givenStockSymbolsWhenGetMarketsThenReturnTheStockValues() {
        //given
        when(tradingService.get(any())).then(invocation -> StockResponseDTO.builder()
                .symbol(invocation.getArgument(0)).price(BigDecimal.ONE).build());

        String symbols = "AAPL,GOOG,GOOGL";
        String url = String.format("/stocks/market?symbols=%s", symbols);

        webTestClient.get()
                .uri(url)
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiCollectionResponse<StockResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItems());
                            Collection<StockResponseDTO> items = response.getResponseBody().getItems();
                            Assert.assertEquals(items.size(), symbols.split(",").length);
                            items.forEach(
                                    stock -> Assert.assertEquals(
                                            stock.getPrice().compareTo(BigDecimal.ONE), 0)
                            );
                        }
                );
    }

    @Test
    void givenPurchasedStockWhenSellStockOrderThenCreateOrder() {
        //given
        stockSettingsService.create("AAPL");
        when(tradingService.get(any())).then(invocation -> StockResponseDTO.builder()
                .symbol(invocation.getArgument(0)).price(BigDecimal.ONE).build());
        PurchasedStock purchasedStock = new PurchasedStock();
        purchasedStock.setSymbol("AAPL");
        purchasedStock.setUser(user);
        purchasedStock.setSymbolUnit(BigDecimal.ONE);
        purchasedStockService.save(purchasedStock);
        StockOrderRequestDTO stockOrderRequestDTO = StockOrderRequestDTO.builder()
                .bankAccountId(bankAccount.getId())
                .symbol("AAPL")
                .symbolUnit(BigDecimal.ONE)
                .targetAmount(BigDecimal.TEN)
                .build();

        webTestClient.post()
                .uri("/stocks/sell")
                .bodyValue(stockOrderRequestDTO)
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<StockOrderResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItem());
                            StockOrderResponseDTO orderResponseDTO = response.getResponseBody().getItem();
                            Assert.assertEquals(orderResponseDTO.getStatus(),
                                    StockTransactionStatusesEnum.ACTIVE);
                            Assert.assertEquals(orderResponseDTO.getMode(),
                                    StockTransactionModesEnum.SELL);
                            Assert.assertEquals(orderResponseDTO.getSymbol(),
                                    stockOrderRequestDTO.getSymbol());
                            Assert.assertEquals(orderResponseDTO.getSymbolUnit()
                                    .compareTo(stockOrderRequestDTO.getSymbolUnit()), 0);
                            Assert.assertEquals(orderResponseDTO.getTargetAmount()
                                    .compareTo(stockOrderRequestDTO.getTargetAmount()), 0);
                        }
                );
    }

    @Test
    void givenBankAccountWithFundsWhenBuyStockOrderThenCreateOrder() {
        //given bank account with required funds
        stockSettingsService.create("AAPL");
        when(tradingService.get(any())).then(invocation -> StockResponseDTO.builder()
                .symbol(invocation.getArgument(0)).price(BigDecimal.ONE).build());

        BigDecimal requiredAmount = BigDecimal.valueOf(100);
        BigDecimal bankAccountFunds = bankAccountService.getById(bankAccount.getId())
                .map(
                        account -> {
                            bankAccount.setVersion(account.getVersion());
                            return account.getBalance();
                        }
                ).orElse(BigDecimal.ZERO);
        if (bankAccountFunds.compareTo(requiredAmount) < 0) {
            bankAccountService.topUp(
                    bankAccount.getId(),
                    requiredAmount.subtract(bankAccountFunds),
                    bankAccount.getVersion()
            );
        }

        StockOrderRequestDTO stockOrderRequestDTO = StockOrderRequestDTO.builder()
                .bankAccountId(bankAccount.getId())
                .symbol("AAPL")
                .symbolUnit(BigDecimal.ONE)
                .targetAmount(requiredAmount)
                .build();

        webTestClient.post()
                .uri("/stocks/buy")
                .bodyValue(stockOrderRequestDTO)
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<StockOrderResponseDTO>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItem());
                            StockOrderResponseDTO orderResponseDTO = response.getResponseBody().getItem();
                            Assert.assertEquals(orderResponseDTO.getStatus(),
                                    StockTransactionStatusesEnum.ACTIVE);
                            Assert.assertEquals(orderResponseDTO.getMode(),
                                    StockTransactionModesEnum.BUY);
                            Assert.assertEquals(orderResponseDTO.getSymbol(),
                                    stockOrderRequestDTO.getSymbol());
                            Assert.assertEquals(orderResponseDTO.getSymbolUnit()
                                    .compareTo(stockOrderRequestDTO.getSymbolUnit()), 0);
                            Assert.assertEquals(orderResponseDTO.getTargetAmount()
                                    .compareTo(stockOrderRequestDTO.getTargetAmount()), 0);
                        }
                );
    }

    @Test
    void givenStockSymbolWhenSubscribeThenSaveStockSettings() {
        //given stock symbol
        String symbol = "AAPL";

        webTestClient.post()
                .uri("/stocks/subscribe/{id}", symbol)
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<String>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItem());
                            String result = response.getResponseBody().getItem();
                            Assert.assertEquals(result, "OK");
                            Assert.assertTrue(stockSettingsService.exist(symbol));
                        }
                );
    }

    @Test
    void givenStockSymbolWhenUnSubscribeThenDeleteIfExistsStockSettings() {
        //given stock symbol
        String symbol = "AAPL";
        stockSettingsService.create(symbol);

        webTestClient.post()
                .uri("/stocks/unsubscribe/{id}", symbol)
                .accept(MediaType.APPLICATION_JSON)
                .header(Constants.HEADER_KEY_AUTHORIZATION,
                        Constants.HEADER_VALUE_AUTHORIZATION_PREFIX.concat(token))
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<String>>() {
                })
                .consumeWith(
                        response -> {
                            Assert.assertNotNull(response.getResponseBody());
                            Assert.assertNotNull(response.getResponseBody().getItem());
                            String result = response.getResponseBody().getItem();
                            Assert.assertEquals(result, "OK");
                            Assert.assertFalse(stockSettingsService.exist(symbol));
                        }
                );
    }


}
