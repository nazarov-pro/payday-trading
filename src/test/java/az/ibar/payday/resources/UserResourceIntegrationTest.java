package az.ibar.payday.resources;

import az.ibar.payday.configs.ContainerSupport;
import az.ibar.payday.container.dtos.UserRequestDTO;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.UserStatusesEnum;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.services.UserService;
import az.ibar.payday.utils.converters.QueryParamExtractor;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;

@AutoConfigureWebTestClient
class UserResourceIntegrationTest extends ContainerSupport {
    @Autowired
    private WebTestClient webTestClient;
    @Autowired
    private QueryParamExtractor queryParamExtractor;
    @Autowired
    private UserService userService;

    @MockBean
    private JavaMailSender javaMailSender;

    @Test
    void givenUserRegistrationRequestWhenUserValidThenRegister() {
        UserRequestDTO userRequestDTO = UserRequestDTO.builder()
                .email("test-user-registration@testemail.com")
                .username("test-user-registration")
                .password("testTest123")
                .build();

        doAnswer(invocation -> {
            SimpleMailMessage simpleMailMessage = invocation.getArgument(0);
            String text = simpleMailMessage.getText();
            Assert.assertNotNull(text);
            //http://someurl.com/user/activate?id=1&
            // email=test@testemail.com&activationToken=eaf8a3fe-3efa-407d-b4d7-0085fb77c507
            int paramsStarts = text.indexOf("/user/activate?") + "/user/activate?".length();
            Map<String, String> params = queryParamExtractor.convertSafelyDirect(text.substring(paramsStarts));
            User user = userService.getById(Long.parseLong(params.get("id")));
            Assert.assertNotNull(user);
            Assert.assertEquals(user.getEmail(), params.get("email"));
            Assert.assertEquals(user.getActivationToken(), params.get("activationToken"));
            Assert.assertEquals(user.getStatus(), UserStatusesEnum.DEACTIVATED);
            return null;
        }).when(javaMailSender).send(any(SimpleMailMessage.class));


        ApiResponse<String> responseBody = webTestClient.post()
                .uri("/user/register")
                .bodyValue(userRequestDTO)
                .accept(MediaType.APPLICATION_JSON)
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<String>>() {
                })
                .returnResult().getResponseBody();
        Assert.assertNotNull(responseBody.getItem());
        Assert.assertEquals(responseBody.getItem(), "OK");
    }

    @Test
    void givenUserActivationRequestWhenUserDeactivatedThenActivateUser() {
        //initial
        User user = new User();
        user.setEmail("test-user-activation@domain.com");
        user.setUsername("test-user-activation");
        user.setStatus(UserStatusesEnum.DEACTIVATED);
        user.setActivationToken("just-an-activation-token");
        user = userService.save(user);
        Assert.assertNotNull(user);

        //given
        String url = String.format(
                "/user/activate?id=%d&email=%s&activationToken=%s",
                user.getId(),
                user.getEmail(),
                user.getActivationToken()
        );
        //when
        ApiResponse<String> responseBody = webTestClient.get()
                .uri(url)
                .accept(MediaType.APPLICATION_JSON)
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<String>>() {
                }).returnResult().getResponseBody();

        //then
        Assert.assertNotNull(responseBody.getItem());
        Assert.assertEquals(responseBody.getItem(), "OK");

        User activatedUser = userService.getById(user.getId());
        Assert.assertEquals(activatedUser.getStatus(), UserStatusesEnum.ACTIVE);
    }
}
