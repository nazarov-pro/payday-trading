package az.ibar.payday.resources;

import az.ibar.payday.configs.ContainerSupport;
import az.ibar.payday.container.dtos.AuthRequestDTO;
import az.ibar.payday.container.dtos.AuthResponseDTO;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.UserStatusesEnum;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.services.UserService;
import az.ibar.payday.utils.JwtUtils;
import az.ibar.payday.utils.TimeUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Instant;

import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureWebTestClient
class AuthenticationResourceIntegrationTest extends ContainerSupport {
    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtils jwtUtils;

    @MockBean
    private TimeUtils timeUtils;

    private static final Instant NOW = Instant.now();
    private User user;
    private final String rawPassword = "testSecured1234";

    @BeforeAll
    public void setUp() {
        when(timeUtils.getEpochSeconds()).thenReturn(NOW.getEpochSecond());
        when(timeUtils.getEpochMillis()).thenReturn(NOW.toEpochMilli());

        user = new User();
        user.setPassword(passwordEncoder.encode(rawPassword));
        user.setEmail("authentication-test@domain.com");
        user.setUsername("authentication-test");
        user.setStatus(UserStatusesEnum.ACTIVE);
        user.setActivationToken("just-an-activation-token");
        user = userService.save(user);
    }

    @Test
    void givenUserWhenAuthenticationThenGetJwtToken() {
        //given
        AuthRequestDTO authRequestDTO = AuthRequestDTO.builder()
                .usernameOrEmail(user.getEmail()).password(rawPassword).build();

        //when
        ApiResponse<AuthResponseDTO> responseBody = webTestClient.post()
                .uri("/auth")
                .bodyValue(authRequestDTO)
                .accept(MediaType.APPLICATION_JSON)
                .exchange().expectStatus().is2xxSuccessful()
                .expectBody(new ParameterizedTypeReference<ApiResponse<AuthResponseDTO>>() {
                })
                .returnResult()
                .getResponseBody();

        //then
        Assert.assertNotNull(responseBody.getItem());
        Assert.assertNotNull(responseBody.getItem().getToken());
        Assert.assertTrue(jwtUtils.validateToken(responseBody.getItem().getToken()));
    }
}
