package az.ibar.payday.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ConstantsTest {

    @Test
    public void testStockListIsNotEmpty() {
        List<String> sampleStocks = Constants.SAMPLE_STOCKS;
        Assert.assertNotNull("Sample stocks is null", sampleStocks);
        Assert.assertFalse("Sample stocks is empty", sampleStocks.isEmpty());
    }
}
