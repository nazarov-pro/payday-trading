package az.ibar.payday.utils.converters;

import az.ibar.payday.container.dtos.UserRequestDTO;
import az.ibar.payday.container.entities.User;
import az.ibar.payday.utils.converters.impl.UserRequestConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserRequestConverterTest {
    private UserRequestConverter userRequestConverter;

    @Before
    public void setUp() {
        userRequestConverter = new UserRequestConverterImpl();
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                userRequestConverter.validate(null)
        );
    }

    @Test
    public void testConvertingWithBankAccount() {
        UserRequestDTO userRequestDto = UserRequestDTO.builder()
                .username("username")
                .email("email@email.com")
                .id(1L)
                .password("password")
                .build();
        
        User convertedDto = userRequestConverter
                .convertSafely(userRequestDto)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedDto.getId(), userRequestDto.getId());
        Assert.assertEquals(convertedDto.getUsername(), userRequestDto.getUsername());
        Assert.assertEquals(convertedDto.getEmail(), userRequestDto.getEmail());
        Assert.assertEquals(convertedDto.getPassword(), userRequestDto.getPassword());
    }

}
