package az.ibar.payday.utils.converters;

import az.ibar.payday.container.entities.MailTransferHistory;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.utils.converters.impl.MailTransferHistoryConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MailTransferHistoryConverterTest {
    private MailTransferHistoryConverter mailTransferHistoryConverter;

    @Before
    public void setUp() {
        mailTransferHistoryConverter = new MailTransferHistoryConverterImpl();
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                mailTransferHistoryConverter.validate(null)
        );
    }

    @Test
    public void testConvertingWithBankAccount() {
        MailMessage mailMessage = new MailMessage();
        mailMessage.setContent("content");
        mailMessage.setRecipient("email@email.com");
        MailTransferHistory convertedDto = mailTransferHistoryConverter
                .convertSafely(mailMessage)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedDto.getContent(), mailMessage.getContent());
        Assert.assertEquals(convertedDto.getRecipient(), mailMessage.getRecipient());
    }

}
