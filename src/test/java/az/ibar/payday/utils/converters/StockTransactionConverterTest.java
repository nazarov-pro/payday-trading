package az.ibar.payday.utils.converters;

import az.ibar.payday.container.dtos.StockOrderRequestDTO;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.container.enums.StockTransactionModesEnum;
import az.ibar.payday.utils.converters.impl.StockTransactionConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class StockTransactionConverterTest {
    private StockTransactionConverter stockTransactionConverter;

    @Before
    public void setUp() {
        stockTransactionConverter = new StockTransactionConverterImpl();
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                stockTransactionConverter.validate(null)
        );
    }

    @Test
    public void testConvertingWithBankAccount() {
        StockOrderRequestDTO stockOrderRequestDto = new StockOrderRequestDTO();
        stockOrderRequestDto.setBankAccountId(1L);
        stockOrderRequestDto.setId(2L);
        stockOrderRequestDto.setMode(StockTransactionModesEnum.SELL.getValue());
        stockOrderRequestDto.setSymbol("AAPL");
        stockOrderRequestDto.setSymbolUnit(BigDecimal.valueOf(2.5));
        stockOrderRequestDto.setTargetAmount(BigDecimal.TEN);
        StockTransaction convertedEntity = stockTransactionConverter
                .convertSafely(stockOrderRequestDto)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedEntity.getBankAccount().getId(), stockOrderRequestDto.getBankAccountId());
        Assert.assertEquals(convertedEntity.getId(), stockOrderRequestDto.getId());
        Assert.assertEquals(convertedEntity.getMode(), stockOrderRequestDto.getMode());
        Assert.assertEquals(convertedEntity.getSymbol(), stockOrderRequestDto.getSymbol());
        Assert.assertEquals(convertedEntity.getSymbolUnit(), stockOrderRequestDto.getSymbolUnit());
        Assert.assertEquals(convertedEntity.getTargetAmount(), stockOrderRequestDto.getTargetAmount());
    }
}
