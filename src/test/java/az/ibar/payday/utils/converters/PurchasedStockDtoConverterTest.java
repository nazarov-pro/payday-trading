package az.ibar.payday.utils.converters;

import az.ibar.payday.container.dtos.PurchasedStockResponseDTO;
import az.ibar.payday.container.entities.PurchasedStock;
import az.ibar.payday.utils.converters.impl.PurchasedStockDtoConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class PurchasedStockDtoConverterTest {
    private PurchasedStockDtoConverter purchasedStockDtoConverter;

    @Before
    public void setUp() {
        purchasedStockDtoConverter = new PurchasedStockDtoConverterImpl();
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                purchasedStockDtoConverter.validate(null)
        );
    }

    @Test
    public void testConvertingWithBankAccount() {
        PurchasedStock purchasedStock = new PurchasedStock();
        purchasedStock.setId(1L);
        purchasedStock.setCreatedTimestamp(2L);
        purchasedStock.setSymbol("AAPL");
        purchasedStock.setSymbolUnit(BigDecimal.ONE);
        purchasedStock.setUpdatedTimeStamp(3L);
        purchasedStock.setVersion("version");
        PurchasedStockResponseDTO convertedDto = purchasedStockDtoConverter
                .convertSafely(purchasedStock)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedDto.getId(), purchasedStock.getId());
        Assert.assertEquals(convertedDto.getCreatedTimestamp(), purchasedStock.getCreatedTimestamp());
        Assert.assertEquals(convertedDto.getSymbol(), purchasedStock.getSymbol());
        Assert.assertEquals(convertedDto.getSymbolUnit(), purchasedStock.getSymbolUnit());
        Assert.assertEquals(convertedDto.getUpdatedTimestamp(), purchasedStock.getUpdatedTimeStamp());
        Assert.assertEquals(convertedDto.getVersion(), purchasedStock.getVersion());
    }

}
