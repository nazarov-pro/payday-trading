package az.ibar.payday.utils.converters;

import az.ibar.payday.container.dtos.StockOrderResponseDTO;
import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.container.entities.StockTransaction;
import az.ibar.payday.container.enums.StockTransactionModesEnum;
import az.ibar.payday.container.enums.StockTransactionStatusesEnum;
import az.ibar.payday.utils.converters.impl.StockOrderResponseDtoConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class StockOrderResponseDTOConverterTest {
    private StockOrderResponseDtoConverter stockOrderResponseDtoConverter;

    @Before
    public void setUp() {
        stockOrderResponseDtoConverter = new StockOrderResponseDtoConverterImpl();
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                stockOrderResponseDtoConverter.validate(null)
        );
    }

    @Test
    public void testConvertingWithBankAccount() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(1L);

        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setBankAccount(bankAccount);
        stockTransaction.setId(2L);
        stockTransaction.setMode(StockTransactionModesEnum.SELL.getValue());
        stockTransaction.setSymbol("AAPL");
        stockTransaction.setSymbolUnit(BigDecimal.valueOf(2.5));
        stockTransaction.setTargetAmount(BigDecimal.TEN);
        stockTransaction.setCreatedTimestamp(3L);
        stockTransaction.setStatus(StockTransactionStatusesEnum.ACTIVE);
        stockTransaction.setStartAmount(BigDecimal.ONE);
        stockTransaction.setUpdatedTimestamp(4L);
        stockTransaction.setVersion("version");
        StockOrderResponseDTO convertedDto = stockOrderResponseDtoConverter
                .convertSafely(stockTransaction)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedDto.getBankAccountId(), stockTransaction.getBankAccount().getId());
        Assert.assertEquals(convertedDto.getId(), stockTransaction.getId());
        Assert.assertEquals(convertedDto.getMode().getValue(), stockTransaction.getMode());
        Assert.assertEquals(convertedDto.getSymbol(), stockTransaction.getSymbol());
        Assert.assertEquals(convertedDto.getSymbolUnit(), stockTransaction.getSymbolUnit());
        Assert.assertEquals(convertedDto.getTargetAmount(), stockTransaction.getTargetAmount());
        Assert.assertEquals(convertedDto.getCreatedTimestamp(), stockTransaction.getCreatedTimestamp());
        Assert.assertEquals(convertedDto.getStartAmount(), stockTransaction.getStartAmount());
        Assert.assertEquals(convertedDto.getStatus(), stockTransaction.getStatus());
        Assert.assertEquals(convertedDto.getUpdatedTimestamp(), stockTransaction.getUpdatedTimestamp());
        Assert.assertEquals(convertedDto.getVersion(), stockTransaction.getVersion());
    }

    @Test
    public void testConvertingWithoutBankAccount() {
        StockTransaction stockTransaction = new StockTransaction();
        stockTransaction.setId(2L);
        stockTransaction.setMode(StockTransactionModesEnum.SELL.getValue());
        stockTransaction.setSymbol("AAPL");
        stockTransaction.setSymbolUnit(BigDecimal.valueOf(2.5));
        stockTransaction.setTargetAmount(BigDecimal.TEN);
        stockTransaction.setCreatedTimestamp(3L);
        stockTransaction.setStatus(StockTransactionStatusesEnum.ACTIVE);
        stockTransaction.setStartAmount(BigDecimal.ONE);
        stockTransaction.setUpdatedTimestamp(4L);
        stockTransaction.setVersion("version");
        StockOrderResponseDTO convertedDto = stockOrderResponseDtoConverter
                .convertSafely(stockTransaction)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedDto.getId(), stockTransaction.getId());
        Assert.assertEquals(convertedDto.getMode().getValue(), stockTransaction.getMode());
        Assert.assertEquals(convertedDto.getSymbol(), stockTransaction.getSymbol());
        Assert.assertEquals(convertedDto.getSymbolUnit(), stockTransaction.getSymbolUnit());
        Assert.assertEquals(convertedDto.getTargetAmount(), stockTransaction.getTargetAmount());
        Assert.assertEquals(convertedDto.getCreatedTimestamp(), stockTransaction.getCreatedTimestamp());
        Assert.assertEquals(convertedDto.getStartAmount(), stockTransaction.getStartAmount());
        Assert.assertEquals(convertedDto.getStatus(), stockTransaction.getStatus());
        Assert.assertEquals(convertedDto.getUpdatedTimestamp(), stockTransaction.getUpdatedTimestamp());
        Assert.assertEquals(convertedDto.getVersion(), stockTransaction.getVersion());
    }

}
