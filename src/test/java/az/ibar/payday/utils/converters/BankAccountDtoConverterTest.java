package az.ibar.payday.utils.converters;

import az.ibar.payday.container.dtos.BankAccountResponseDTO;
import az.ibar.payday.container.entities.BankAccount;
import az.ibar.payday.utils.converters.impl.BankAccountDtoConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class BankAccountDtoConverterTest {
    private BankAccountDtoConverter bankAccountDtoConverter;

    @Before
    public void setUp() {
        bankAccountDtoConverter = new BankAccountDtoConverterImpl();
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                bankAccountDtoConverter.validate(null)
        );
    }

    @Test
    public void testConvertingWithBankAccount() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(1L);
        bankAccount.setBalance(BigDecimal.TEN);
        bankAccount.setVersion("version");
        BankAccountResponseDTO convertedDto = bankAccountDtoConverter
                .convertSafely(bankAccount)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedDto.getId(), bankAccount.getId());
        Assert.assertEquals(convertedDto.getBalance(), bankAccount.getBalance());
        Assert.assertEquals(convertedDto.getVersion(), bankAccount.getVersion());
    }

}
