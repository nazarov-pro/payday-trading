package az.ibar.payday.utils.converters;

import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.models.MailMessage;
import az.ibar.payday.utils.converters.impl.ActivationMailMessageConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ActivationMailMessageConverterTest {
    private ActivationMailMessageConverter mailMessageConverter;

    @Before
    public void setUp() {
        mailMessageConverter = new ActivationMailMessageConverterImpl("http://localhost:8080");
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                mailMessageConverter.validate(null)
        );
    }

    @Test
    public void testConvertingWithBankAccount() {
        User user = new User();
        user.setId(1L);
        user.setUsername("username");
        user.setEmail("email@email.com");
        user.setActivationToken("token");
        MailMessage convertedDto = mailMessageConverter
                .convertSafely(user)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedDto.getRecipient(), user.getEmail());
    }

}
