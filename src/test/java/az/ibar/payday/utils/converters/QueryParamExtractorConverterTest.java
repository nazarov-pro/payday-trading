package az.ibar.payday.utils.converters;

import az.ibar.payday.utils.converters.impl.QueryParamExtractorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class QueryParamExtractorConverterTest {
    private QueryParamExtractor queryParamExtractor;

    @Before
    public void setUp() {
        queryParamExtractor = new QueryParamExtractorImpl();
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                queryParamExtractor.validate(null)
        );
    }

    @Test
    public void testEmptyValue() {
        Assert.assertFalse("Check empty value handler",
                queryParamExtractor.validate("")
        );
    }

    @Test
    public void testHandlingEmptyValueSafely() {
        Assert.assertEquals(
                queryParamExtractor.getZeroVal(),
                queryParamExtractor.convertSafelyDirect("")
        );
    }

    @Test
    public void testConvertingWithVal() {
        String path = "id=4&email=my_email@domain.com&token=123";
        Map<String, String> data = queryParamExtractor.convert(path);
        Assert.assertEquals(data.get("id"), "4");
        Assert.assertEquals(data.get("email"), "my_email@domain.com");
        Assert.assertEquals(data.get("token"), "123");
    }

    @Test
    public void testConvertingWithNoVal() {
        String path = "id=4&email=my_email@domain.com&test";
        Map<String, String> data = queryParamExtractor.convert(path);
        Assert.assertEquals(data.get("id"), "4");
        Assert.assertEquals(data.get("email"), "my_email@domain.com");
        Assert.assertEquals(data.get("test"), "");
    }

}
