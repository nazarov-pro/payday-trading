package az.ibar.payday.utils.converters;

import az.ibar.payday.container.entities.User;
import az.ibar.payday.container.enums.UserStatusesEnum;
import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.utils.converters.impl.UserDetailsConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserDetailsConverterTest {
    private UserDetailsConverter userDetailsConverter;

    @Before
    public void setUp() {
        userDetailsConverter = new UserDetailsConverterImpl();
    }

    @Test
    public void testNullValue() {
        Assert.assertFalse("Check null value handler",
                userDetailsConverter.validate(null)
        );
    }

    @Test
    public void testConvertingWithBankAccount() {
        User user = new User();
        user.setId(1L);
        user.setUsername("username");
        user.setPassword("password");
        user.setStatus(UserStatusesEnum.DEACTIVATED);
        AuthUserDetails convertedDto = userDetailsConverter
                .convertSafely(user)
                .orElseThrow(() -> new RuntimeException("Converting failed."));
        Assert.assertEquals(convertedDto.getId(), user.getId());
        Assert.assertEquals(convertedDto.getUsername(), user.getUsername());
        Assert.assertFalse(convertedDto.isEnabled());
        Assert.assertEquals(convertedDto.getPassword(), user.getPassword());
    }

}
