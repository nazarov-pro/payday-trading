package az.ibar.payday.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PBKDF2EncoderTest {
    private PasswordEncoder passwordEncoder;
    private final String secret = "secret";
    private final Integer iterationLevel = 21;
    private final Integer keyLength = 256;

    @Before
    public void setUp() {
        passwordEncoder = new PBKDF2Encoder(
                secret, iterationLevel, keyLength
        );
    }

    @Test
    public void testHashIsNotTheSame() {
        String sensitiveData = "secret-word";
        String encode = passwordEncoder.encode(sensitiveData);
        Assert.assertFalse("Hash and raw password is same.", sensitiveData.equals(encode));
    }

    @Test
    public void testHashMatchesRawPwd() {
        String sensitiveData = "secret-word";
        String encode = passwordEncoder.encode(sensitiveData);
        Assert.assertTrue("Hashing of the same pwd is not matches.",
                passwordEncoder.matches(sensitiveData, encode));
    }
}
