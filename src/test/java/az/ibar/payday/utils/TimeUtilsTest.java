package az.ibar.payday.utils;

import az.ibar.payday.utils.impl.TimeUtilsImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TimeUtilsTest {
    private TimeUtils timeUtils;

    @Before
    public void setUp() {
        timeUtils = new TimeUtilsImpl();
    }

    @Test
    public void testEpochMillis() {
        Long epochMillis = timeUtils.getEpochMillis();
        Assert.assertTrue(epochMillis <= timeUtils.getEpochMillis());
    }

    @Test
    public void testEpochSeconds() {
        Long epochSeconds = timeUtils.getEpochSeconds();
        Assert.assertTrue(epochSeconds <= timeUtils.getEpochSeconds());
    }

    @Test
    public void testNotNull() {
        Long epochMillis = timeUtils.getEpochMillis();
        Long epochSeconds = timeUtils.getEpochSeconds();
        Assert.assertNotNull(epochMillis);
        Assert.assertNotNull(epochSeconds);
    }
}
