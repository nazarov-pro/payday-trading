package az.ibar.payday.utils.generators;

import az.ibar.payday.container.models.ApiCollectionResponse;
import az.ibar.payday.container.models.ApiMessage;
import az.ibar.payday.container.models.ApiMetadata;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.utils.TimeUtils;
import az.ibar.payday.utils.generators.impl.ApiResponseGeneratorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

public class ApiResponseGeneratorTest {
    private ApiResponseGenerator apiResponseGenerator;
    private TimeUtils timeUtils;

    @Before
    public void setUp() {
        timeUtils = Mockito.mock(TimeUtils.class);
        Mockito.when(timeUtils.getEpochMillis()).thenReturn(1000L);
        Mockito.when(timeUtils.getEpochSeconds()).thenReturn(1L);
        apiResponseGenerator = new ApiResponseGeneratorImpl(timeUtils);
    }

    @Test
    public void testGenerationMetadata() {
        ApiMetadata metadata = apiResponseGenerator.generateMetadata();
        Assert.assertEquals(metadata.getTimestamp(), timeUtils.getEpochMillis());
    }

    @Test
    public void testGenerationMessage() {
        ApiMetadata metadata = apiResponseGenerator.generateMetadata();
        ApiMessage message = apiResponseGenerator.generateMessage(metadata);
        Assert.assertEquals(message.getMetadata(), metadata);
    }

    @Test
    public void testGenerationSingleMessage() {
        String message = "OK";
        ApiResponse<String> apiResponse = apiResponseGenerator.generate(message);
        Assert.assertEquals(apiResponse.getItem(), message);
        Assert.assertEquals(apiResponse.isSuccess(), true);
        Assert.assertEquals(apiResponse.getMetadata().getTimestamp(), timeUtils.getEpochMillis());
    }

    @Test
    public void testGenerationCollectionMessage() {
        List<String> messages = Arrays.asList("a", "b", "c");
        ApiCollectionResponse<String> apiResponse = apiResponseGenerator.generateCollection(messages);
        Assert.assertEquals(apiResponse.getItems(), messages);
        Assert.assertEquals(apiResponse.isSuccess(), true);
        Assert.assertEquals(apiResponse.getMetadata().getTimestamp(), timeUtils.getEpochMillis());
    }

}
