package az.ibar.payday.utils.generators;

import az.ibar.payday.container.models.ApiCollectionResponse;
import az.ibar.payday.container.models.ApiMessage;
import az.ibar.payday.container.models.ApiMetadata;
import az.ibar.payday.container.models.ApiResponse;
import az.ibar.payday.utils.generators.impl.UUIDGeneratorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class UUIDGenerationTest {
    private UUIDGenerator uuidGenerator;

    @Before
    public void setUp() {
        uuidGenerator = new UUIDGeneratorImpl();
    }

    @Test
    public void testNotNull() {
        String uuid = uuidGenerator.generate();
        Assert.assertNotNull(uuid);
    }

    @Test
    public void testSizeIsStandard() {
        String uuid = uuidGenerator.generate();
        Assert.assertEquals(uuid.length(), 36);
    }
}
