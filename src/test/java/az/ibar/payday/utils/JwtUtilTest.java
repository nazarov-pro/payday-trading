package az.ibar.payday.utils;

import az.ibar.payday.container.models.AuthUserDetails;
import az.ibar.payday.utils.impl.JwtUtilsImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class JwtUtilTest {
    private JwtUtils jwtUtils;
    private TimeUtils timeUtils;
    private final String JWT_SECRET = "12345trtshrsjrsu5y6wsrytjujhsrw5y6ursjusr5jujyntgrgynjx";
    private final Long EXPIRATION_SECONDS = 10L;
    private static final Long EXPECTED_MILLIS = Instant.now().plusSeconds(100).toEpochMilli();
    private static final Long EXPECTED_SECONDS = Instant.now().plusSeconds(100).getEpochSecond();

    @Before
    public void setUp() {
        timeUtils = Mockito.mock(TimeUtils.class);
        Mockito.when(timeUtils.getEpochMillis()).thenReturn(EXPECTED_MILLIS);
        Mockito.when(timeUtils.getEpochSeconds()).thenReturn(EXPECTED_SECONDS);
        jwtUtils = new JwtUtilsImpl(
                JWT_SECRET,
                EXPIRATION_SECONDS,
                timeUtils
        );
        jwtUtils.init();
    }

    @Test
    public void testUsername() {
        String username = "username";
        String token = generateToken(1L, username, "USER");
        Assert.assertEquals(jwtUtils.getUsernameFromToken(token), username);
    }

    @Test
    public void testUserId() {
        Long userId = 1L;
        String token = generateToken(userId, "username", "USER");
        Assert.assertEquals(jwtUtils.getUserIdFromToken(token), userId);
    }

    @Test
    public void testExpirationDate() {
        String token = generateToken(1L, "username", "USER");
        Assert.assertEquals(
                jwtUtils.getExpirationDateFromToken(token).getTime(),
                (timeUtils.getEpochSeconds() + EXPIRATION_SECONDS) * 1000
        );
    }

    private String generateToken(
            Long userId,
            String username,
            String role
    ) {
        AuthUserDetails authUserDetails = new AuthUserDetails();
        authUserDetails.setId(userId);
        authUserDetails.setUsername(username);
        authUserDetails.setRole(role);
        return jwtUtils.generateToken(authUserDetails);
    }

    @Test
    public void testWithNotExpiredToken() {
        String token = generateToken(1L, "username", "email");
        Assert.assertTrue(jwtUtils.validateToken(token));
    }

    @Test
    public void testWithExpiredToken() {
        String token = generateToken(1L, "username", "email");
        Mockito.when(timeUtils.getEpochMillis()).thenReturn(
                Instant.now().plus(EXPECTED_SECONDS, ChronoUnit.HOURS).toEpochMilli());
        Assert.assertFalse(jwtUtils.validateToken(token));
    }
}
