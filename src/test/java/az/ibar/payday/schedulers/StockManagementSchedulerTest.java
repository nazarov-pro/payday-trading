package az.ibar.payday.schedulers;

import az.ibar.payday.container.entities.StockSettings;
import az.ibar.payday.schedulers.impl.StockManagementSchedulerImpl;
import az.ibar.payday.services.StockSettingsService;
import az.ibar.payday.services.trading.RealTimeTradingService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StockManagementSchedulerTest {
    private StockManagementScheduler stockManagementScheduler;
    private RealTimeTradingService realTimeTradingService;
    private StockSettingsService stockSettingsService;
    private static final String REAL_TIME_TRADING_SERVICE_NAME = "mock_trading_service";

    @Before
    public void setUp() {
        realTimeTradingService = Mockito.mock(RealTimeTradingService.class);
        stockSettingsService = Mockito.mock(StockSettingsService.class);
        stockManagementScheduler = new StockManagementSchedulerImpl(
                realTimeTradingService, stockSettingsService
        );
        Mockito.when(realTimeTradingService.name()).thenReturn(REAL_TIME_TRADING_SERVICE_NAME);
    }

    @Test
    public void testInitialize() {
        Mockito.when(stockSettingsService.unlockAll(Mockito.anyString()))
                .thenAnswer(invocation -> {
                            String name = invocation.getArgument(0);
                            Assert.assertEquals(name, REAL_TIME_TRADING_SERVICE_NAME);
                            return true;
                        }
                );
        Mockito.when(stockSettingsService.updateAndGet(Mockito.anyString(), Mockito.anyInt()))
                .thenAnswer(invocation -> {
                            String name = invocation.getArgument(0);
                            Integer expirationSeconds = invocation.getArgument(1);
                            Assert.assertEquals(name, REAL_TIME_TRADING_SERVICE_NAME);
                            Assert.assertTrue(expirationSeconds > 0);
                            return getStockList();
                        }
                );
        List<String> stockList = getStockList().stream().map(StockSettings::getId).collect(Collectors.toList());
        Mockito.doAnswer(
                invocation -> {
                    String stockId = invocation.getArgument(0);
                    Assert.assertTrue(stockList.contains(stockId));
                    return null;
                }
        ).when(realTimeTradingService).subscribe(Mockito.anyString());
        stockManagementScheduler.init();
    }

    @Test
    public void testRefreshStocks() {
        Mockito.when(stockSettingsService.updateAllSubscribersLastTmp(Mockito.anyString()))
                .thenAnswer(invocation -> {
                            String name = invocation.getArgument(0);
                            Assert.assertEquals(name, REAL_TIME_TRADING_SERVICE_NAME);
                            return true;
                        }
                );
        Mockito.when(stockSettingsService.updateAndGet(Mockito.anyString(), Mockito.anyInt()))
                .thenAnswer(invocation -> {
                            String name = invocation.getArgument(0);
                            Integer expirationSeconds = invocation.getArgument(1);
                            Assert.assertEquals(name, REAL_TIME_TRADING_SERVICE_NAME);
                            Assert.assertTrue(expirationSeconds > 0);
                            return getStockList();
                        }
                );
        List<String> stockList = getStockList().stream().map(StockSettings::getId).collect(Collectors.toList());
        Mockito.doAnswer(
                invocation -> {
                    String stockId = invocation.getArgument(0);
                    Assert.assertTrue(stockList.contains(stockId));
                    return null;
                }
        ).when(realTimeTradingService).subscribe(Mockito.anyString());
        stockManagementScheduler.refreshStocks();
    }

    private List<StockSettings> getStockList() {
        StockSettings stockSettings = new StockSettings();
        stockSettings.setId("AAPL");
        StockSettings stockSettings1 = new StockSettings();
        stockSettings1.setId("GOOG");
        return Arrays.asList(stockSettings, stockSettings1);
    }
}
